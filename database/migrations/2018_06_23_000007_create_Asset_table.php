<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'Asset';

    /**
     * Run the migrations.
     * @table Asset
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->integer('min_capacity')->nullable();
            $table->integer('max_capacity')->nullable();
            $table->integer('per_agent_booking_limit')->nullable();
            $table->string('description')->nullable();
            $table->integer('created_by')->unsigned();
            $table->string('asset_ref', 45)->nullable();
            $table->timestamps();
            $table->index(["created_by"], 'fk_Asset_created_by_idx');

            $table->unique(["asset_ref"], 'asset_ref_UNIQUE');


            $table->foreign('created_by', 'fk_Asset_created_by_idx')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
