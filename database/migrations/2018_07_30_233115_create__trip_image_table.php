<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripImageTable extends Migration
{

    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'TripImage';


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {

            $table->engine = 'InnoDB';
            
            $table->string('path');
            
            $table->integer('trip_id')->unsigned();
            $table->increments('id');
            $table->index(["trip_id"], 'fk_TripImage_Trip1_idx');

            $table->foreign('trip_id', 'fk_TripImage_Trip1_idx')
                ->references('id')->on('Trip')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->set_schema_table);
    }
}
