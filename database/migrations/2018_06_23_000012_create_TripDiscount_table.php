<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripdiscountTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'TripDiscount';

    /**
     * Run the migrations.
     * @table TripDiscount
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->double('adult')->nullable();
            $table->double('child')->nullable();
            $table->double('infant')->nullable();
            $table->integer('trip_id')->unsigned();
            $table->integer('user_group_id')->unsigned();
            $table->timestamps();
            $table->index(["user_group_id"], 'fk_TripDiscount_UserGroup1_idx');

            $table->index(["trip_id"], 'fk_TripDiscount_Trip1_idx');


            $table->foreign('trip_id', 'fk_TripDiscount_Trip1_idx')
                ->references('id')->on('Trip')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('user_group_id', 'fk_TripDiscount_UserGroup1_idx')
                ->references('id')->on('UserGroup')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
