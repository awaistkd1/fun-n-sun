<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreatedByToCoupon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Coupon', function (Blueprint $table) {
            $table->integer('created_by')->unsigned();
            $table->index(["created_by"], 'fk_Coupon_created_by_idx');

            $table->foreign('created_by', 'fk_Coupon_created_by_idx')
            ->references('id')->on('users')
            ->references('id')->on('users')
            ->onDelete('cascade')
            ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Coupon', function (Blueprint $table) {
            //
        });
    }
}
