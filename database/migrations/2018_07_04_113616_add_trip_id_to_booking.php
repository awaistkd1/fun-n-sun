<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTripIdToBooking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Booking', function (Blueprint $table) {
            $table->integer('trip_id')->unsigned();
            $table->index(["trip_id"], 'fk_Booking_trip_id_idx');

            $table->foreign('trip_id', 'fk_Booking_trip_id_idx')
            ->references('id')->on('Trip')
            ->onDelete('cascade')
            ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Booking', function (Blueprint $table) {
            //
        });
    }
}
