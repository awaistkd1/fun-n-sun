<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsergroupTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'UserGroup';

    /**
     * Run the migrations.
     * @table UserGroup
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->string('description')->nullable();
            $table->tinyInteger('assets_add')->nullable();
            $table->tinyInteger('assets_update')->nullable();
            $table->tinyInteger('assets_delete')->nullable();
            $table->tinyInteger('trips_add')->nullable();
            $table->tinyInteger('trips_update')->nullable();
            $table->tinyInteger('trips_delete')->nullable();
            $table->tinyInteger('bookings_add')->nullable();
            $table->tinyInteger('bookings_update')->nullable();
            $table->tinyInteger('bookings_delete')->nullable();
            $table->tinyInteger('customers_add')->nullable();
            $table->tinyInteger('customers_update')->nullable();
            $table->tinyInteger('customers_delete')->nullable();
            $table->tinyInteger('sales_agents_add')->nullable();
            $table->tinyInteger('sales_agents_update')->nullable();
            $table->tinyInteger('sales_agents_delete')->nullable();
            $table->tinyInteger('coupons_add')->nullable();
            $table->tinyInteger('coupons_update')->nullable();
            $table->tinyInteger('coupons_delete')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
