<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtraservicediscountTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'ExtraServiceDiscount';

    /**
     * Run the migrations.
     * @table ExtraServiceDiscount
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->double('discount')->nullable();
            $table->integer('extra_service_id')->unsigned();
            $table->integer('user_group_id')->unsigned();
            $table->timestamps();
            $table->index(["extra_service_id"], 'fk_ExtraServiceDiscount_ExtraService1_idx');

            $table->index(["user_group_id"], 'fk_ExtraServiceDiscount_UserGroup1_idx');


            $table->foreign('extra_service_id', 'fk_ExtraServiceDiscount_ExtraService1_idx')
                ->references('id')->on('ExtraService')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('user_group_id', 'fk_ExtraServiceDiscount_UserGroup1_idx')
                ->references('id')->on('UserGroup')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
