<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponvalidtripsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'CouponValidTrips';

    /**
     * Run the migrations.
     * @table CouponValidGroups
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('coupon_id')->unsigned();
            $table->integer('trip_id')->unsigned();
            
            
            $table->index(["coupon_id"], 'fk_CouponValidTrip_Coupon1_idx');

            $table->index(["trip_id"], 'fk_CouponValidTrip_Trip1_idx');


            $table->foreign('coupon_id', 'fk_CouponValidTrip_Coupon1_idx')
                ->references('id')->on('Coupon')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('trip_id', 'fk_CouponValidTrip_Trip1_idx')
                ->references('id')->on('Trip')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
