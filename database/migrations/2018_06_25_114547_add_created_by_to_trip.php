<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreatedByToTrip extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Trip', function (Blueprint $table) {
            $table->integer('created_by')->unsigned();
            $table->index(["created_by"], 'fk_Trip_created_by_idx');
            
            $table->foreign('created_by', 'fk_Trip_created_by_idx')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Trip', function (Blueprint $table) {
            //
        });
    }
}
