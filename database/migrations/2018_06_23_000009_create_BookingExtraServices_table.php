<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingextraservicesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'BookingExtraServices';

    /**
     * Run the migrations.
     * @table BookingExtraServices
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('booking_id')->unsigned();
            $table->integer('extra_service_id')->unsigned();
            $table->timestamps();
            $table->index(["booking_id"], 'fk_BookingExtraServices_Booking1_idx');

            $table->index(["extra_service_id"], 'fk_BookingExtraServices_ExtraService1_idx');


            $table->foreign('booking_id', 'fk_BookingExtraServices_Booking1_idx')
                ->references('id')->on('Booking')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('extra_service_id', 'fk_BookingExtraServices_ExtraService1_idx')
                ->references('id')->on('ExtraService')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
