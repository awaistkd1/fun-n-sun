<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFkAssetId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('AssetImage', function (Blueprint $table) {
            
            $table->dropForeign('fk_AssetImage_Asset1_idx');



            $table->foreign('asset_id', 'fk_AssetImage_Asset1_idx')
                ->references('id')->on('Asset')
                ->onDelete('cascade')
                ->onUpdate('no action');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('AssetImage', function (Blueprint $table) {
            //
        });
    }
}
