<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripextraservicesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'TripExtraServices';

    /**
     * Run the migrations.
     * @table TripExtraServices
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('trip_id')->unsigned();
            $table->integer('extra_service_id')->unsigned();
            $table->timestamps();
            $table->index(["trip_id"], 'fk_TripExtraServices_Trip1_idx');

            $table->index(["extra_service_id"], 'fk_TripExtraServices_ExtraService1_idx');


            $table->foreign('trip_id', 'fk_TripExtraServices_Trip1_idx')
                ->references('id')->on('Trip')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('extra_service_id', 'fk_TripExtraServices_ExtraService1_idx')
                ->references('id')->on('ExtraService')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
