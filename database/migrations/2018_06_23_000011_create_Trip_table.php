<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'Trip';

    /**
     * Run the migrations.
     * @table Trip
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->string('date', 45)->nullable();
            $table->string('start_date', 45)->nullable();
            $table->string('end_date', 45)->nullable();
            $table->string('repeat', 45)->nullable();
            $table->string('description', 45)->nullable();
            $table->integer('asset_id')->unsigned();
            $table->double('adult_price')->nullable();
            $table->double('child_price')->nullable();
            $table->double('infant_price')->nullable();
            $table->string('trip_ref', 45)->nullable();
            $table->timestamps();
            $table->index(["asset_id"], 'fk_Trip_Asset1_idx');

            $table->unique(["trip_ref"], 'trip_ref_UNIQUE');


            $table->foreign('asset_id', 'fk_Trip_Asset1_idx')
                ->references('id')->on('Asset')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
