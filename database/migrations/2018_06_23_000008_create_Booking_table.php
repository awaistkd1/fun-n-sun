<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'Booking';

    /**
     * Run the migrations.
     * @table Booking
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->integer('created_user_id')->unsigned();
            $table->integer('ticket_qty')->nullable();
            $table->string('request_info')->nullable();
            $table->tinyInteger('sales_agents_commission')->nullable();
            $table->integer('sales_agent_id')->unsigned();
            $table->integer('coupon_id')->unsigned();
            $table->integer('adult_qty')->nullable();
            $table->integer('child_qty')->nullable();
            $table->integer('infant_qty')->nullable();
            $table->integer('status')->nullable();
            $table->string('booking_ref', 45)->nullable();
            $table->timestamps();
            $table->index(["customer_id"], 'fk_Booking_Customer1_idx');

            $table->index(["created_user_id"], 'fk_Booking_user1_idx');

            $table->index(["sales_agent_id"], 'fk_Booking_user2_idx');

            $table->index(["coupon_id"], 'fk_Booking_Coupon1_idx');

            $table->unique(["booking_ref"], 'booking_ref_UNIQUE');


            $table->foreign('customer_id', 'fk_Booking_Customer1_idx')
                ->references('id')->on('Customer')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('created_user_id', 'fk_Booking_user1_idx')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('sales_agent_id', 'fk_Booking_user2_idx')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('coupon_id', 'fk_Booking_Coupon1_idx')
                ->references('id')->on('Coupon')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
