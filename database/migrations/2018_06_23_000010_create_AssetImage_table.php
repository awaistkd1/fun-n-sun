<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetimageTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'AssetImage';

    /**
     * Run the migrations.
     * @table AssetImage
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('path');
            $table->integer('asset_id')->unsigned();
            $table->increments('id');
            $table->timestamps();
            $table->index(["asset_id"], 'fk_AssetImage_Asset1_idx');


            $table->foreign('asset_id', 'fk_AssetImage_Asset1_idx')
                ->references('id')->on('Asset')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
