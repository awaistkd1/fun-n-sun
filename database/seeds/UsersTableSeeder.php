<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	// insert admin group
    	DB::table('UserGroup')->insert([
			'name' => 'Admin',
			'description' => 'This group is for admin only',
			'assets_add' => true,
			'assets_update' => true,
			'assets_delete' => true,
			'trips_add' => true,
			'trips_update' => true,
			'trips_delete' => true,
			'bookings_add' => true,
			'bookings_update' => true,
			'bookings_delete' => true,
			'customers_add' => true,
			'customers_update' => true,
			'customers_delete' => true,
			'sales_agents_add' => true,
			'sales_agents_update' => true,
			'sales_agents_delete' => true,
			'coupons_add' => true,
			'coupons_update' => true,
			'coupons_delete' => true
		]);

        // insert user
       	DB::table('users')->insert([
            'name' => 'Muhammad Awais',
            'email' => 'aawais.nu@gmail.com',
            'password' => bcrypt('admin'),
            'picture' => 'admin.jpg',
            'mobile' => '+923334363087',
            'user_group_id'=>DB::getPdo()->lastInsertId(),
            'created_by' => 0
        ]);
    }
}
