 <?php
return [
            'assets' => [
                'add' => ['add_asset', 'save_asset'],
                'view' => ['view_asset','view_asset_by_id','get_assets_ajax'],
                'update' => ['edit_asset','save_edit_asset'],
                'delete' => ['delete_asset']
            ],
            'coupons' => [
                'add' => ['add_coupon','save_coupon'],
                'view' => ['view_coupon_by_id','get_coupons_ajax','view_coupon'],
                'update' => ['edit_coupon','update_coupon'],
                'delete' => ['delete_coupon']
            ],
            
            'customers' => [
                'add' => ['add_customer','save_customer'],
                'view' => ['view_customer','get_customers_ajax','view_customer_by_id'],
                'update' => ['edit_customer','save_edit_customer'],
                'delete' => ['delete_customer']  
            ],
            'sales_agents' => [
                'add' => ['add_users','save_users'],
                'view' => ['view_agent','get_agents_ajax','view_agent_by_id'],
                'update' => ['edit_agent','update_agent'],
                'delete' => ['delete_agent']
            ],
            'trips' => [
                'add' => ['add_trip','save_trip'],
                'view' => ['view_trip', 'get_trips_ajax', 'view_trip_by_id'],
                'update' => ['change_status','edit_trip','update_trip'],
                'delete' => ['delete_trip'] 
            ],
            'group_rights' =>[
                'add' => ['add_user_group','save_user_group'],
                'view' => ['view_user_group','get_user_groups_ajax','view_user_group_by_id'],
                'update' => ['edit_user_group','update_user_group'],
                'delete' => ['delete_user_group']
            ],
            'users' =>[
                'add' => ['add_users','save_users'],
                'view' => ['view_users','get_users_ajax','view_user_by_id'],
                'update' => ['edit_user','update_user'],
                'delete' => ['delete_user']
            ],
            'bookings' => [
                'add' => ['make_booking','save_booking'],
                'view' => ['bookings','view_all_bookings_of_trip','get_bookings_table'],
                 'update' => [],
                 'delete' =>[]
            ],
            'events' => [
                'add' => ['add_event','save_event'],
                'view' => ['view_events','view_event_calander','get_events_ajax'],
                'update' => ['edit_event','update_event'],
                'delete' => ['delete_event']   
            ]
        ];