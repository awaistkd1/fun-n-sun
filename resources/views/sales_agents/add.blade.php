
@extends('layouts.dashboard')

@section('main-content')

<div class="row">
    <!--col-md-12 start-->
    <div class="col-md-12">
        <div class="page-heading">
            <h1>
                Add New Sales Agent
            </h1>
        </div>
    </div>
    <!--col-md-12 start-->
</div>


<div class="row">
    <div class="col-md-12">
        <div class="box-info">
            <h3>Information</h3>
            <hr>
            <form id="validate-form" data-validate="parsley" class="form-horizontal row-border" action="">

                <div class="col-md-6">
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Group</label>
                        <div class="col-sm-6">
                            <Select class="form-control parsley-validated" required>
                                <option value="">Select</option>
                                @for ($i = 1; $i <= 10; $i++)
                                    <option>Group - {{ $i }}</option>
                                @endfor
                            </Select>
                        </div>
                    </div>
                    <div class="form-group lable-padd">
                            <label class="col-sm-3">Name</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control parsley-validated" required placeholder="">
                        </div>
                    </div>
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Email</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control parsley-validated" required placeholder="" data-minlength="6">
                        </div>
                    </div>
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Mobile #</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control parsley-validated" required placeholder="" data-maxlength="6">
                        </div>
                    </div>
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Description</label>
                        <div class="col-sm-6">
                            <textarea class="form-control parsley-validated" placeholder="" data-rangelength="[5,10]"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-lg-12 action-button">
                            <button onclick="javascript:$('#validate-form').parsley( 'validate' );" class="btn-danger btn">Submit</button>
                            <a class="btn-default btn" href="#">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection