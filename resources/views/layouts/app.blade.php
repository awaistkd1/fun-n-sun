<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>{{ config('app.name', 'Laravel') }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Bootstrap -->
        <link href="{{ asset('bs3/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style-responsive.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('plugins/kalendar/kalendar.css') }}" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="{{ asset('plugins/bootstrap-datepicker/css/datepicker.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('plugins/bootstrap-timepicker/compiled/timepicker.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('plugins/bootstrap-colorpicker/css/colorpicker.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('plugins/jquery-multi-select/css/multi-select.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('plugins/tag-input/jquery.tagsinput.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('plugins/icheck/red.css') }}" />
        <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
        <link href="{{ asset('plugins/scroll/nanoscroller.css') }}" rel="stylesheet">

        <!--data-table css-->

        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">

        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">

        <link rel="stylesheet" type="text/css" href="{{ asset('plugins/dist/dropzone.css') }}">

        <!-- jquery timepicker -->
        <link rel="stylesheet" type="text/css" href="{{ asset('plugins/jquery-timepicker/jquery.timepicker.min.css') }}">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script>
            var upload_url = "{{ url('/upload') }}";
            var csrf_token = "{{ csrf_token() }}";
            window.Laravel = { csrfToken: '{{ csrf_token() }}' };
        </script>
        <style>
            .dropzone .dz-preview .dz-error-message {top: 150px!important;}
            .error{ color: red; }
        </style>
    </head>
    <body>
        

        @yield('content')


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
        <script src="{{ asset('js/jquery-1.10.2.js') }}"></script> 
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed --> 
        <script src="{{ asset('bs3/js/bootstrap.min.js') }}"></script> 
        <script src="{{ asset('js/smooth-sliding-menu.js') }}"></script> 
        <script src="{{ asset('js/console-numbering.js') }}"></script> 
        <script src="{{ asset('js/to-do-admin.js') }}"></script> 
        <script src="{{ asset('js/jquery.sparkline.js') }}"></script> 
        <script src="{{ asset('js/sparkline-chart.js') }}"></script> 
        <script src="{{ asset('js/select-checkbox.js') }}"></script> 
        
        <script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}"></script>

        <!-- Data Tables -->
        <script src="http://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
        
        <script src="{{ asset('js/jPushMenu.js') }}"></script> 
        <script src="{{ asset('js/side-chats.js') }}"></script> 
        <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script> 
        <script src="{{ asset('plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script> 
        <script src="{{ asset('plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script> 
        <script src="{{ asset('plugins/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script> 
        <script src="{{ asset('plugins/jquery-multi-select/js/jquery.multi-select.js') }}"></script> 
        <script src="{{ asset('plugins/jquery-multi-select/js/jquery.quicksearch.js') }}"></script>
        <script src="{{ asset('js/form-components.js') }}"></script> 
        
        <script src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script> 
        <script src="{{ asset('plugins/tag-input/jquery.tagsinput.js') }}"></script> 
        <script src="{{ asset('plugins/tag-input/taginput-edit.js') }}"></script> 
        <script src="{{ asset('plugins/icheck/icheck.js') }}"></script> 
        <script src="{{ asset('plugins/scroll/jquery.nanoscroller.js') }}"></script>
        <script src="{{ asset('plugins/kalendar/kalendar.js') }}"></script> 
        <script src="{{ asset('plugins/kalendar/edit-kalendar.js') }}"></script> 
        
        <script src="{{ asset('plugins/dist/dropzone.js') }}"></script>

        <!-- jquery validation -->
        <script src="{{ asset('plugins/jquery-validation/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('plugins/jquery-validation/additional-methods.min.js') }}"></script>

        <!-- jquery time picker -->
        <script src="{{ asset('plugins/jquery-timepicker/jquery.timepicker.min.js') }}"></script>

        <script>
            $(document).ready(function(){
            	$('.skin-section input').iCheck({
            		checkboxClass: 'icheckbox_flat-red',
            		radioClass: 'iradio_flat-red'
            	});
            });
        </script>

        <script>
            var sep = '<div class="row">'+
                '<div class="col-md-6">'+
                    '<hr>'+
                '</div>'+
            '</div>';
            
            var listCounter = 0;
            $("#add_more_trip_services_btn").on("click", function(e) {
            	listCounter++;
            	removeBtn = '<div class="col-md-12"><div class="form-group lable-padd" style="margin-bottom:0;"><label class="col-lg-12"><div onclick="removeTripServicesList('+listCounter+')" class="remove_trip_services_btn" style="cursor:pointer;float:left;"><i class="fa fa-trash-o"></i> <span>Remove</span></div></div></div>';

            	$("#trip_services_list").append("<div id='trip_services_list_append_"+listCounter+"'>"+$("#trip_services_list_append").html()+removeBtn+"</div>" + sep);
            });
            function removeTripServicesList(ID){
                $("#trip_services_list_append_"+ID).remove();
            }
        </script>
        @yield('js')
    </body>
</html>