@extends('layouts.app')

@section('content')

<!--layout-container start-->
<div id="layout-container"> 

	<!--Left navbar start-->
	<div id="nav"> 
	<!--logo start-->
	<div class="profile">
	    <div class="logo"><a href="{{ route('home') }}"><img src="{{ asset('images/logo.png') }}" alt=""></a></div>
	</div>
	<!--logo end--> 

	<!--navigation start-->
	<ul class="navigation">
	    <li>
	    	<a class="{{{ (Request::is('dashboard') ? 'active' : '') }}}"  href="{{ route('home') }}">
	        	<i class="fa fa-home"></i><span>Dashboard</span>
	        </a>
	    </li>
	    <li class="sub">
	    	<a class="{{{ (Request::is('assets/*') ? 'active' : '') }}}" href="{{ route('view_asset') }}">
	        	<i class="fa fa-book"></i><span>Assets</span>
	        </a>
	        <ul class="navigation-sub">
	            <li>
	            	<a href="{{ route('view_asset') }}">
	            		<i class="fa fa-th-list"></i><span>All Assets</span>
	                </a>
	            </li>
	            @if($user_group->assets_add)
	            <li>
	            	<a href="{{ route('add_asset') }}">
	            		<i class="fa fa-plus"></i><span>Add New</span>
	                </a>
	            </li>
	            @endif
	        </ul>
	    </li>
	    <li class="sub">
	    	<a class="{{{ (Request::is('trip/*') ? 'active' : '') }}}" href="{{ route('view_trip') }}">
	        	<i class="fa fa-map-marker"></i><span>Trips</span>
	        </a>
	        <ul class="navigation-sub">
	            <li>
	            	<a href="{{ route('view_trip') }}">
	            		<i class="fa fa-th-list"></i><span>All Trips</span>
	                </a>
	            </li>
	            @if($user_group->trips_add)
	            <li>
	            	<a href="{{ route('add_trip') }}">
	            		<i class="fa fa-plus"></i><span>Add New</span>
	                </a>
	            </li>
	            @endif
	        </ul>
	    </li>
	    <li>
	    	<a class="{{{ (Request::is('bookings') || Request::is('bookings/*') ? 'active' : '') }}}" href="{{ route('bookings') }}">
	        	<i class="fa fa-table"></i><span>Booking</span>
	        </a>
	    </li>
	    <li class="sub">
	    	<a class="{{{ (Request::is('sales-agent/*') || Request::is('customer/*') ? 'active' : '') }}}" href="#">
	        	<i class="fa fa-list-alt"></i><span>Accounts</span>
	        </a>
	        <ul class="navigation-sub">
	            <li>
	            	<a href="{{ route('view_customer') }}">
	                	<i class="fa fa-book"></i><span>All Customers</span>
	                </a>
	            </li>
	            @if($user_group->customers_add)
	            <li>
	            	<a href="{{ route('add_customer') }}">
	                	<i class="fa fa-plus"></i><span>Add Customers</span>
	                </a>
	            </li>
	            @endif
	            <li>
	            	<a href="{{ route('view_agent') }}">
	                	<i class="fa fa-compass"></i><span>All Sale Agents</span>
	                </a>
	            </li>
	            @if($user_group->sales_agents_add)
	            <li>
	            	<a href="{{ route('add_users') }}">
	                	<i class="fa fa-plus"></i><span>Add Sales Agent</span>
	                </a>
	            </li>	            
	            @endif
	        </ul>
	      </li>

	    <li class="sub">
	    	<a class="{{{ (Request::is('coupon/*') ? 'active' : '') }}}" href="{{ route('view_coupon') }}">
	        	<i class="fa fa-gift"></i><span>Coupons</span>
	        </a>
	        <ul class="navigation-sub">
	            <li>
	            	<a href="{{ route('view_coupon') }}">
	            		<i class="fa fa-th-list"></i><span>All Coupons</span>
	                </a>
	            </li>
	            @if($user_group->coupons_add)
	            <li>
	            	<a href="{{ route('add_coupon') }}">
	            		<i class="fa fa-plus"></i><span>Add New</span>
	                </a>
	            </li>
	            @endif
	        </ul>
	    </li>
	    @if($user_group->id == 1)
	    <li class="sub">
	    	<a class="{{{ (Request::is('user-group/*') ? 'active' : '') }}}" href="{{ route('view_user_group') }}">
	        	<i class="fa fa-group"></i><span>Group Rights</span>
	        </a>
	        <ul class="navigation-sub">
	            <li>
	            	<a href="{{ route('view_user_group') }}">
	            		<i class="fa fa-th-list"></i><span>All Groups</span>
	                </a>
	            </li>
	            <li>
	            	<a href="{{ route('add_user_group') }}">
	            		<i class="fa fa-plus"></i><span>Add New</span>
	                </a>
	            </li>
	        </ul>
	    </li>
	    <li class="sub">
	    	<a class="{{{ (Request::is('users/*') ? 'active' : '') }}}" href="{{ route('view_users') }}">
	        	<i class="fa fa-user"></i><span>Users</span>
	        </a>
	        <ul class="navigation-sub">
	            <li>
	            	<a href="{{ route('view_users') }}">
	            		<i class="fa fa-th-list"></i><span>All Users</span>
	                </a>
	            </li>
	            <li>
	            	<a href="{{ route('add_users') }}">
	            		<i class="fa fa-plus"></i><span>Add New</span>
	                </a>
	            </li>
	        </ul>
	    </li>
	    <li class="sub"><a href="{{ route('view_reports_page') }}"><i class="fa fa-file-text"></i><span>Reports</span></a></li>
	    <li class="sub">
	    	<a class="{{{ (Request::is('calender/*') || Request::is('calender') ? 'active' : '') }}}" href="{{ route('view_events') }}"><i class="fa fa-calendar"></i><span>Events</span></a>
	    	<ul class="navigation-sub">
	    		<li>
	    			<a href="{{ route('add_event') }}">
	    				<i class="fa fa-plus"></i><span>Add</span>
	    			</a>
	    		</li>
	    		<li>
	    			<a href="{{ route('view_events') }}">
	    				<i class="fa fa-th-list"></i><span>List</span>
	    			</a>
	    		</li>
	    		<li>
	    			<a href="{{ route('view_event_calander') }}">
	    				<i class="fa fa-calendar"></i><span>Calender</span>
	    			</a>
	    		</li>
	    	</ul>
	    </li>
	    @endif
	    
	</ul>
	<!--navigation end--> 
	</div>
	<!--Left navbar end-->   

	<!--main start-->
	<style>
	.box-info-action-icons i{font-size:22px;margin:10px 13px 0px 0px;}
	</style>
	<div id="main">
		
		<div class="head-title">
			<div class="menu-switch"><i class="fa fa-bars"></i></div>
			<!--row start-->
			<div class="row"> 
		    	<!--col-md-12 start-->
		        <div class="col-md-12"> 
		            <!--profile dropdown start-->
		            <ul class="user-info pull-right fadeInLeftBig animated">
		                <li class="profile-info dropdown"> 
		                	<a data-toggle="dropdown" class="dropdown-toggle" href="#"> 
		                		<img class="img-circle profile-image" alt="" src="{{ \Storage::disk('local')->url(Auth::user()->picture) }}">{{ Auth::user()->name }}
		                   	</a>
		                    <ul class="dropdown-menu">
		                        <li class="caret"></li>
		                        <li><a href="{{ route('view_profile') }}"> <i class="fa fa-user"></i> Edit Profile</a> </li>
		                        <li><a href="{{ route('change_password') }}"> <i class="fa fa-lock"></i> Change Password</a> </li>
		                        <li><a href="{{ route('logout') }}" 
		                        	onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();"
                                    >
                                        <i class="fa fa-clipboard"></i> Log Out
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                                </li>
		                    </ul>
		                </li>
		            </ul>
		            <!--profile dropdown end--> 
		            
		            <!--top nav start-->
		            <h1 class="system-name">{{ config('app.name') }}</h1>
		        </div>
		    	<!--col-md-12 end--> 
		  	</div>
		  	<!--row end--> 
		</div>


		<!--margin-container start-->
		<div class="margin-container"> 
			<div class="scrollable wrapper"> 
				@yield('main-content')
			</div>
		</div>
		
	</div>
	<!--main end-->
</div>
<!--layout-container end-->

@endsection