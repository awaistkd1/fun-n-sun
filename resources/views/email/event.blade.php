@extends('beautymail::templates.sunny')

@section('content')

    @include ('beautymail::templates.sunny.heading' , [
        'heading' => $title,
        'level' => 'h1',
    ])

    @include('beautymail::templates.sunny.contentStart')

        <p>{{$description}}</p>

    @include('beautymail::templates.sunny.contentEnd')

    @include('beautymail::templates.sunny.button', [
        	'title' => 'Add Trip Now',
        	'link' => route('add_trip')
    ])

@stop