@extends('layouts.dashboard')


@section('main-content')

<style>
	.gallery-wrap .img-big-wrap img {
	    height: 450px;
	    width: auto;
	    display: inline-block;
	    cursor: zoom-in;
	}


	.gallery-wrap .img-small-wrap .item-gallery {
	    width: 60px;
	    height: 60px;
	    border: 1px solid #ddd;
	    margin: 7px 2px;
	    display: inline-block;
	    overflow: hidden;
	}

	.gallery-wrap .img-small-wrap {
	    text-align: center;
	}
	.gallery-wrap .img-small-wrap img {
	    max-width: 100%;
	    max-height: 100%;
	    object-fit: cover;
	    border-radius: 4px;
	    cursor: zoom-in;
	}
</style>

    <div class="row">
        <!--col-md-12 start-->
        <div class="col-md-12">
            <div class="page-heading">
                <h1>Booking<small> Details</small></h1>
            </div>
        </div>
        <!--col-md-12 end-->
    </div>

	@if (session('status'))
	<div class="alert alert-success alert-dismissible fade in">
	    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	    <strong>Success!</strong> {{ session('status') }}
	</div>
	@endif    


    <div class="row">
        <div class="col-md-12">
            <div class="box-info">

				<div class="card">
				    <div class="row">
				        <aside class="col-sm-12">
				            <article class="card-body p-5">
				              
				                <!-- price-detail-wrap .// -->
				                <dl class="item-property">
				                    <dt>Ref.</dt>
				                    <dd>
				                        <p>{{ $booking->booking_ref }}</p>
				                    </dd>
				                </dl>


				                <dl class="item-property">
				                    <dt>Customer</dt>
				                    <dd>
				                        <p>{{ $customer->first_name.' '.$customer->middle_name.' '.$customer->last_name }}</p>
				                    </dd>
				                </dl>

				                <dl>
				                	<dt>
				                		Trip
				                	</dt>
				                	<dd>
				                		<p>{{$booking->trip()->first()->name}}</p>
				                	</dd>
				                </dl>

				                <dl>
				                	<dt>
				                		Sales Agent
				                	</dt>
				                	<dd>
				                		<p>{{$booking->sales_agent()->first()->name}}</p>
				                	</dd>
				                </dl>				                


				                <dl>
				                	<dt>
				                		Tickets (Qty)
				                	</dt>
				                	<dd>
				                		<p>{{$booking->ticket_qty}}</p>
				                	</dd>
				                </dl>	

				                <dl>
				                	<dt>
				                		Quntity
				                	</dt>
				                	<dd>
				                		<table border="1">
				                			<tr>
				                				<th>Adult</th>
				                				<th>Child</th>
				                				<th>Infant</th>
				                			</tr>
				                			<tr>
				                				<td>{{ $booking->adult_qty }}</td>
				                				<td>{{ $booking->child_qty }}</td>
				                				<td>{{ $booking->infant_qty }}</td>
				                			</tr>
				                		</table>
				                	</dd>
				                </dl>


				                <dl>
				                	<dt>
				                		Extra Services
				                	</dt>
				                	<dd>
				                		<table border="1">
				                			<tr>
				                				<th>Name</th>
				                				<th>Price</th>
				                				<th>Qty</th>
				                			</tr>
				                			@foreach($extra_services as $service)
				                			<tr>
				                				<td>{{ $service['name'] }}</td>
				                				<td>{{ $service['price'] }} {{$currency}}</td>
				                				<td>{{ $service['qty'] }}</td>
				                			</tr>
				                			@endforeach
				                		</table>
				                	</dd>
				                </dl>				                     

				                <dl>
				                	<dt>
				                		Total
				                	</dt>
				                	<dd>
				                		<p>{{ $total_cost }} {{$currency}}</p>
				                	</dd>
				                </dl>

				                <dl>
				                	<dt>
				                		Paid
				                	</dt>
				                	<dd>
				                		<p>{{ $booking->amount_paid }} {{$currency}}</p>
				                	</dd>
				                </dl>

				                <dl>
				                	<dt>
				                		Balance
				                	</dt>
				                	<dd>
				                		<p>{{ $total_cost - $booking->amount_paid }} {{$currency}}</p>
				                	</dd>
				                </dl>				                

				                <dl>
				                	<dt>
				                		Agent Commission
				                	</dt>
				                	<dd>
				                		<p>{{ $total_agent_commision }} {{$currency}}</p>
				                	</dd>
				                </dl>				               


				            </article>
				            <!-- card-body.// -->
				        </aside>
				        <!-- col.// -->
				    </div>
				    <!-- row.// -->
				</div>
            </div>
        </div>
    </div>
@endsection


@section('js')

@endsection