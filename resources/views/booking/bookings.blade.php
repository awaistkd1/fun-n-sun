@extends('layouts.dashboard')

@section('main-content')
<style type="text/css">
    .modal-backdrop{ display: none; }
</style>
<!--row start-->
<div class="row">
    <!--col-md-12 start-->
    <div class="col-md-12">
        <div class="page-heading">
            <h1>All Bookings</h1>
        </div>
    </div>
    <!--col-md-12 end-->
</div>
<!--row end-->
@if (session('status'))
<div class="alert alert-success alert-dismissible fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Success!</strong> {{ session('status') }}
</div>
@endif
<div class="row" id="search">
    <form id="search-form">
        <div class="form-group col-xs-9">
            <input class="form-control" id="search-text" type="text" placeholder="Search using ref. code" />
        </div>
        <div class="form-group col-xs-3">
            <button type="submit" class="btn btn-block btn-primary">Search</button>
        </div>
    </form>
</div>

<!--row start-->
<div class="row">
    <!--col-md-12 start-->
    <div class="col-md-12" style="text-align: right;">
        <a href="{{ route('make_booking', ['id' => $trip_id]) }}" class="btn btn-default">Add more</a>
    </div>
    <div class="col-md-12">
        <table id="all-bookings-table" class="display table table-bordered table-striped nowrap" style="width:100%">
            <thead>
                <tr>   
                    <th>Id</th>
                    <th>Ref.</th>
                    <th>Customer</th>
                    <th>Trip</th>
                    <th>Sale Agent</th>
                    <th>Tickets(Qty)</th>
                    <th>Amount Paid</th>
                    <th>Balance</th>
                    <th>Bill</th>
                    @if($user_group->bookings_delete == 0)

                    @else
                    <th>Action</th>
                    @endif
                </tr>
            </thead>
        </table>
    </div>
    <!--col-md-12 end-->
</div>
<!--row end-->

<div class="modal fade" id="collectBillModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×   </span><span class="sr-only">Close</span>

                </button>
                 <h4 class="modal-title" id="myModalLabel">Modal title</h4>

            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="collect-save-btn" class="btn btn-primary">Collect</button>
            </div>
        </div>
    </div>
</div>


@endsection


@section('js')
<script src="{{ asset('app/bookings.js') }}"></script>

<script>

var oTable = $('#all-bookings-table').DataTable({
    dom: "lrtp",
    processing: true,
    serverSide: true,
    responsive: {
        details: {
            display: $.fn.dataTable.Responsive.display.modal( {
                header: function ( row ) {
                    var data = row.data();
                    console.log(data);
                    return 'Details:';
                }
            } ),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                tableClass: 'table'
            } )
        }
    },
    ajax:{
        "url": '{{ route("get_bookings_table") }}',
        "dataType": "json",
        "type": "POST",
        data: function (d) {
            d._token = "{{ csrf_token() }}";
            d.trip_id = "{{ $trip_id }}";
            d.name = $('#search-text').val();
        }
    },
    "columns":[
        { data: "id" },
        {
            data:'ref',
            render : function(data, type, row, meta){
                if(type === 'display'){

                    data = '<a href="{{route('view_booking_by_id')}}/?id='+row.id+'">' + row.ref + '</a>';
                }
                return data;
            }
        },        
        { 
            data: "customer",
            render : function(data, type, row, meta){
                if(type === 'display'){

                    data = '<a href="{{route('view_customer_by_id')}}/?id='+data.id+'">' + data.name + '</a>';
                }
                return data;
            }
        },
        { 
            data: "trip",
            render : function(data, type, row, meta){
                if(type === 'display'){
                    data = '<a href="{{route('view_trip_by_id')}}/?id='+data.id+'">' + data.name + '</a>';
                }
                return data;
            }
        },
        { data: "sale_agent" },
        { data: "tickets_qty" },
        { data: "amount_paid" },
        { data: "balance" },
        { 
            data: null,
            render : function(data, type, row, meta){
                if(type === 'display'){
                    data = '<a href="#" data-id='+row.id+' class="collect-bill-btn">Collect Bill</a>';
                }
                return data;
            }            
        }
        @if( ($user_group->bookings_delete == 0))

        @else
        ,{
            data: null,
            render: function(data, type, row, meta){
                if(type === 'display'){
                    data = '@if($user_group->bookings_delete == 1)<form method="POST" action="{{route('delete_booking')}}" class="form-action" action="">{{csrf_field()}} <input type="hidden" name="booking_id" value="'+row.id+'" /> <button type="submit"><i class="fa fa-trash-o" style="font-size:18px;margin:0 6px;"></i></button></form>@endif @if(Auth::id() == 1)<form method="GET" action="{{route('update_booking')}}" class="form-action" action="">{{csrf_field()}} <input type="hidden" name="booking_id" value="'+row.id+'" /> <button type="submit"><i class="fa fa-pencil" style="font-size:18px;margin:0 6px;"></i></button></form>@endif';
                }
                return data;
            }
        }
        @endif
    ]  
});


$("#search-form").submit(function(e){
    e.preventDefault();
    oTable.draw();
});

$(document).on('click','.collect-bill-btn', function(){

    var id = $(this).data('id');
    console.log(id);

    var form = '<p style="color: red; text-align: center;" id="error-collect"></p><form id="collect_amount_form" action="{{ route('collect_amount') }}" method="POST">'
      +'<div class="form-group">'
        +'<label for="amount">Amount:</label>'
        +'<input type="text" name="amount" class="form-control" />'
        +'<input type="hidden" name="booking_id" value="'+id+'" />'
      +'</div>'
    +'</form>';


    $("#collectBillModal .modal-body").html(form);
    $("#collectBillModal").modal('show');
});


$(document).on('click','#collect-save-btn',function(){
    var amount = $("#collect_amount_form input[name=amount]").val();
    var booking_id = $("#collect_amount_form input[name=booking_id]").val();


    $.ajax({
        url: $("#collect_amount_form").attr('action'),
        method: 'POST',
        data: { amount: amount, booking_id: booking_id, _token: csrf_token },
        success: function(response) {
            if(typeof response.success != 'undefined'){
                oTable.draw();
                $("#collectBillModal").modal('hide');
            }else{
                $("#error-collect").text("Invalid amount or booking.");
            }
        },
        error: function(err) {
            alert("Error");
        }
    });
});

</script>

@endsection