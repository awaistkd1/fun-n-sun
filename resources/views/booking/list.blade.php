@extends('layouts.dashboard')

@section('main-content')

<!--row start-->
<div class="row">
    <!--col-md-12 start-->
    <div class="col-md-12">
        <div class="page-heading">
            <h1>bookings<small> Listing</small></h1>
        </div>
    </div>
    <!--col-md-12 end-->
</div>
<!--row end-->
@if (session('status'))
<div class="alert alert-success alert-dismissible fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Success!</strong> {{ session('status') }}
</div>
@endif

@if (session('error'))
<div class="alert alert-danger alert-dismissible fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Error!</strong> {{ session('error') }}
</div>
@endif

<div class="row" id="search">
    <form id="search-form">
        <div class="form-group col-xs-9">
            <input class="form-control" id="search-text" type="text" placeholder="Search" />
        </div>
        <div class="form-group col-xs-3">
            <button type="submit" class="btn btn-block btn-primary">Search</button>
        </div>
    </form>
</div>
<div class="row" id="filter">
    <form>
        <div class="form-group col-sm-3 col-xs-6">
            <input type="date" id="trip_date" name="date" class="form-control">
        </div>
        <div class="form-group col-sm-3 col-xs-6">
            <select id="asset_id" data-filter="model" class="filter-model filter form-control">
                <option value="">Select Asset</option>
                @foreach($assets as $asset)
                <option value="{{$asset->id}}">{{$asset->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-sm-3 col-xs-6">
            <select id="trip_repeat" data-filter="type" class="filter-type filter form-control">
                <option value="">Select Repeat</option>
                <option value="daily">Daily</option>
                <option value="weekly">Weekly</option>
                <option value="none">None</option>
            </select>
        </div>
        <div class="form-group col-sm-3 col-xs-6">
            <select id="trip_status" data-filter="price" class="filter-price filter form-control">
                <option value="">Select Status</option>
                @foreach(\Config::get('trip') as $key => $code )
                <option value="{{ $code }}">{{ ucfirst($key) }}</option>
                @endforeach
            </select>
        </div>
    </form>
</div>


<!--row start-->
<div class="row">
    <!--col-md-12 start-->
    <div class="col-md-12">
        <table id="available-trips-table" class="display table table-bordered table-striped nowrap" style="width:100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Date</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <th>Repeat</th>
                    <th>Status</th>
                </tr>
            </thead>
        </table>
    </div>
    <!--col-md-12 end-->
</div>
<!--row end-->

@endsection


@section('js')
<script src="{{ asset('app/bookings.js') }}"></script>

<script>

var oTable = $('#available-trips-table').DataTable({
    dom: "lrtp",
    processing: true,
    serverSide: true,
    responsive: {
        details: {
            display: $.fn.dataTable.Responsive.display.modal( {
                header: function ( row ) {
                    var data = row.data();
                    console.log(data);
                    return 'Details:';
                }
            } ),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                tableClass: 'table'
            } )
        }
    },
    ajax:{
        "url": '{{ route("get_available_bookings_ajax") }}',
        "dataType": "json",
        "type": "POST",
        data: function (d) {
            d._token = "{{ csrf_token() }}";
            d.asset_id = $('#asset_id').val();
            d.name = $('#search-text').val();
            d.repeat = $('#trip_repeat').val();
            d.status = $('#trip_status').val();
            d.date = $('#trip_date').val();
        }
    },
    "columns":[
        { data: "id" },
        { 
            data: "name",
            render : function(data, type, row, meta){
                if(type === 'display'){
                    data = '<a href="{{route('view_trip_by_id')}}/?id='+row.id+'">' + data + '</a>';
                }
                return data;
            }
        },
        { data: "date" },
        { data: "start_date" },
        { data: "end_date" },
        { data: "repeat" },
        {
            data: "status",
            render : function(data, type, row, meta){
                
                if(type === 'display'){
                    if(data == 0){
                        data = "Open";
                    }else if(data == 1){
                        data = "Closed";
                    }else{
                        data = "Canceled";
                    }
                }
                
                return data;
            }
        }
    ]  
});


$("#search-form").submit(function(e){
    e.preventDefault();
    oTable.draw();
});


$("#filter select,#filter input").change(function(){
    oTable.draw(); 
});

</script>

@endsection