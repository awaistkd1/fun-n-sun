@extends('layouts.dashboard')


@section('main-content')

	<style type="text/css">
		.panel-body{
			padding-left: 40px;
		}
		.customer-fields{ display: none; }
		.search-field{ display: block; }
		.ui-autocomplete {
			z-index: 10000;
		}

		#add-new-customer-btn{ cursor: pointer; }

/*jQuery UI autocomplete for bootstrap*/
.ui-autocomplete {
  position: absolute;
  top: 100%;
  left: 0;
  z-index: 1000;
  float: left;
  display: none;
  min-width: 160px;
  _width: 160px;
  padding: 4px 0;
  margin: 2px 0 0 0;
  list-style: none;
  background-color: #ffffff;
  border-color: #ccc;
  border-color: rgba(0, 0, 0, 0.2);
  border-style: solid;
  border-width: 1px;
  -webkit-border-radius: 5px;
  -moz-border-radius: 5px;
  border-radius: 5px;
  -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  -webkit-background-clip: padding-box;
  -moz-background-clip: padding;
  background-clip: padding-box;
  *border-right-width: 2px;
  *border-bottom-width: 2px;
}
.ui-autocomplete .ui-menu-item > a.ui-corner-all {
  display: block;
  padding: 3px 15px;
  clear: both;
  font-weight: normal;
  line-height: 18px;
  color: #555555;
  white-space: nowrap;
}
.ui-autocomplete .ui-menu-item > a.ui-corner-all.ui-state-hover, .ui-autocomplete .ui-menu-item > a.ui-corner-all.ui-state-active, .ui-autocomplete .ui-menu-item > a.ui-corner-all.ui-state-focus {
  color: #ffffff;
  text-decoration: none;
  background-color: #0088cc;
  border-radius: 0px;
  -webkit-border-radius: 0px;
  -moz-border-radius: 0px;
  background-image: none;
}


.ui-menu-item {
    display: block;
    clear: both;
    font-weight: normal;
    line-height: 18px;
    color: #555555;
    white-space: nowrap;

  }
.ui-menu-item-wrapper{      padding: 3px 15px;
}

    .ui-state-active, .ui-state-hover {
      color: #ffffff;
      text-decoration: none;
      background-color: #0088cc;
      border-radius: 0;
      -webkit-border-radius: 0;
      -moz-border-radius: 0;
      background-image: none;
    }


	</style>

    <div class="row">
        <!--col-md-12 start-->
        <div class="col-md-12">
            <div class="page-heading">
                <h1>Make Booking</h1>
            </div>
        </div>
        <!--col-md-12 end-->
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box-info1">
           	
	            <form id="form-make-booking" class="form-horizontal row-border" action="{{ route('save_booking') }}" method="POST">

	                @foreach ($errors->all() as $message)

	                    <div class="alert alert-danger alert-dismissible fade in">
	                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                        <strong>Error!</strong> {{ $message }}.
	                    </div>

	                @endforeach

	                {{ csrf_field() }}
	                <input type="hidden" name="trip_id" value="{{$trip->id}}" />
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Customer</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									
									<div class="form-group lable-padd search-field">
										<div class="input-group">
											<input type="text" class="form-control" id="customer-phone-number" aria-describedby="customer-phone-number" placeholder="Search existing customer using phone number">
											<span title="Add new customer" class="input-group-addon" id="add-new-customer-btn"><i class="fa fa-plus"></i></span>
										</div>	
									</div>

									<div class="customer-fields" @if(count($errors->all())) style="display: block;" @endif>
					                    <div class="form-group lable-padd">
					                        <label>First Name</label>
					                        <div>
					                            <input type="text" class="form-control" name="first_name" value="{{old('first_name')}}" id="first_name" />
					                        </div>
					                    </div>

					                    <div class="form-group lable-padd">
					                        <label>Middle Name</label>
					                        <div>
					                            <input type="text" class="form-control" name="middle_name" value="{{old('middle_name')}}" id="middle_name" />
					                        </div>
					                    </div>

					                    <div class="form-group lable-padd">
					                        <label>Last Name</label>
					                        <div>
					                            <input type="text" class="form-control" name="last_name" value="{{old('last_name')}}" id="last_name" />
					                        </div>
					                    </div>

					                    <div class="form-group lable-padd">
					                        <label>Date of Birth</label>
					                        <div>
					                            <input type="text" class="form-control default-date-picker" value="{{old('dob')}}" name="dob" id="dob" />
					                        </div>
					                    </div>


					                    <div class="form-group lable-padd">
					                        <label>Email</label>
					                        <div>
					                            <input type="text" class="form-control" name="email" value="{{old('email')}}" id="email" />
					                        </div>
					                    </div>
					                    <div class="form-group lable-padd">
					                        <label>Mobile #</label>
					                        <div>
					                            <input type="text" class="form-control" name="mobile" value="{{old('mobile')}}" id="mobile" />
					                        </div>
					                    </div>
					                    <div class="form-group lable-padd">
					                        <label>Description</label>
					                        <div>
					                            <textarea class="form-control" name="description" id="description">{{old('description')}}</textarea>
					                        </div>
					                    </div>


					                    <div class="form-group lable-padd">
					                        <label>Referral Name</label>
					                        <div>
					                            <input type="text" class="form-control" name="referral_name" value="{{old('referral_name')}}" id="referral_name" />
					                        </div>
					                    </div>

					                    <div class="form-group lable-padd">
					                        <label>Referral Email</label>
					                        <div>
					                            <input type="text" class="form-control" name="referral_email" value="{{old('referral_email')}}" id="referral_email" />
					                        </div>
					                    </div>

					                    <div class="form-group lable-padd">
					                        <label>Tickets Qty</label>
					                        <div>
					                            <input type="text" class="form-control" id="tickets_qty" name="tickets_qty" value="{{old('tickets_qty')}}" id="tickets_qty" />
					                        </div>
					                    </div>										
									</div>

								</div>
							</div>
	
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Agent</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group lable-padd">
										<label>Coupon Code</label>
										<div>
											<input type="text" class="form-control" name="coupon_code" id="coupon_code" value="{{old('coupon_code')}}" />
										</div>
									</div>

									<div class="form-group lable-padd">
										<label>Sale Agent Commission</label>
										<div>
											<select class="form-control" name="commissioned_sales_agent" id="commissioned_sales_agent" value="{{old('commissioned_sales_agent')}}">
												<option value="">Select</option>
												<option value="1">Yes</option>
												<option value="0">No</option>
											</select>
										</div>
									</div>

									<div class="form-group lable-padd">
										<label>Sale Agent</label>
										<div>
											<select class="form-control" id="sales_agent_id" name="sales_agent_id" value="{{ old('sales_agent_id') }}">
												<option value="">Select</option>
												@foreach($agents as $agent)
												<option value="{{ $agent->id }}">{{ $agent->name }}</option>
												@endforeach
											</select>
										</div>
									</div>									
								</div>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Quantity</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group lable-padd">
										<label>Adult ({{ $trip->adult_price }}{{$currency}})</label>
										<input type="number" class="form-control" name="adult_qty" value="@if(old('adult_qty')!=null){{old('adult_qty')}}@else{{'0'}}@endif" id="adult_qty" placeholder="Quantity">
									</div>
									<div class="form-group lable-padd">
										<label>Child ({{ $trip->child_price }}{{$currency}})</label>
										<input type="number" class="form-control" placeholder="Quantity" name="child_qty" value="@if(old('child_qty')!=null){{old('child_qty')}}@else{{'0'}}@endif" id="child_qty" />
									</div>
									<div class="form-group lable-padd">
										<label>Infant ({{ $trip->infant_price }}{{$currency}})</label>
										<input type="number" class="form-control" placeholder="Quantity" name="infant_qty" value="@if(old('infant_qty')!=null){{old('infant_qty')}}@else{{'0'}}@endif" id="infant_qty" />
									</div>		
								</div>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Other Services</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									<div class="skin-section">
										<ul class="list">
											@foreach( $trip_extra_services as $trip_extra_service)
											<li data-id="{{$trip_extra_service->extra_service_id}}" class="trip-extra-service">
												<input type="checkbox" name="extra_service[]" value="{{$trip_extra_service->extra_service_id}}" />
												<label for="services-1-11">{{$trip_extra_service->extraService()->first()->name}} - ({{$trip_extra_service->extraService()->first()->price}}{{$currency}})</label>
												<input type="number" class="form-control" name="extra_service_qty[]" value="" placeholder="Quantity">
											</li>
											@endforeach
										</ul>
									</div>		
								</div>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Payments</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group lable-padd">
										<label>Payment Type</label>
										<select class="form-control" name="payment_type" value="{{old('payment_type')}}" id="payment_type">
											<option>Select</option>
											@foreach($payment_types as $id => $type)
											<option value="{{$id}}">{{$type}}</option>
											@endforeach
										</select>
									</div>
									<div class="form-group lable-padd">
										<label>Notes</label>
										<input class="form-control" type="text" name="payment_notes" value="{{old('payment_notes')}}" id="payment_notes" />
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-body">
							<input type="submit" value="Submit" class="btn btn-primary pull-left" />
							<a href="{{ route('view_trip') }}" class="btn btn-default pull-right">Cancel</a>
						</div>
					</div>

	            </form>

            </div>
        </div>
    </div>

@endsection


@section('js')
<script src="{{asset('app/bookings.js')}}"></script>

<!-- autocomplete customer -->
<script>

    function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }


    function clearCustomerForm() {
		$("#first_name").val("");
		$("#middle_name").val("");
		$("#last_name").val("");
		$("#dob").val("");
		$("#email").val("");
		$("#mobile").val("");
		$("#description").val("");
		$("#referral_name").val("");
		$("#referral_email").val("");
    }

    $(document).on('click','#add-new-customer-btn',function(){
    	
    	clearCustomerForm();
    	$("#customer-phone-number").val("");
    	$(".customer-fields").show();

    });

	$( "#customer-phone-number" ).autocomplete({
		minLength: 0,
		source: function(request, response){
			$.getJSON( '{{ route('search_customer_by_phone') }}' , {
				term: extractLast( request.term ),
				_token: csrf_token
			}, response );
		},
		focus: function( event, ui ) {
			$( "#customer-phone-number" ).val( ui.item.phone );
			return false;
		},
		select: function( event, ui ) {
			$( "#customer-phone-number" ).val( ui.item.phone );
			
			// populate the form
			$("#first_name").val(ui.item.first_name);
			$("#middle_name").val(ui.item.middle_name);
			$("#last_name").val(ui.item.last_name);
			$("#dob").val(ui.item.dob);
			$("#email").val(ui.item.email);
			$("#mobile").val(ui.item.phone);
			$("#description").val(ui.item.description);
			$("#referral_name").val(ui.item.referral_name);
			$("#referral_email").val(ui.item.referral_email);

			$(".customer-fields").show();
			return false;
		}
	})
	.autocomplete( "instance" )._renderItem = function( ul, item ) {
		console.log(item);
		return $( "<li>" )
		.append( "<div>" + item.first_name + "<br>" + item.phone + "</div>" )
		.appendTo( ul );
	};
</script>


@endsection
