@extends('layouts.dashboard')

@section('main-content')

<div class="row">
    <!--col-md-12 start-->
    <div class="col-md-12">
        <div class="page-heading">
            <h1>
                Update Profile
            </h1>
        </div>
    </div>
    <!--col-md-12 start-->
</div>


<div class="row">
    <div class="col-md-12">
        <div class="box-info">
            <h3>Information</h3>
            <hr>

            <form id="form-user" class="form-horizontal row-border user-form" action="{{ route('update_profile') }}" method="POST">


                @foreach ($errors->all() as $message)

                    <div class="alert alert-danger alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error!</strong> {{ $message }}.
                    </div>

                @endforeach


                {{ csrf_field() }}
                
                <input type="hidden" name="user_id" value="{{$user->id}}">

                <div class="col-md-9">

                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Name</label>
                        <div class="col-sm-6">
                            <input type="text" name="name" id="name" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Email (Username)</label>
                        <div class="col-sm-6">
                            <input type="text" name="email" id="email" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Mobile</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="mobile" id="mobile" placeholder="" />
                        </div>
                    </div>                    
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Description</label>
                        <div class="col-sm-6">
                            <textarea class="form-control" name="description" id="description" placeholder=""></textarea>
                        </div>
                    </div>
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Image</label>
                        <div class="col-sm-6">
                            <div class="dropzone"></div>
                        </div>
                    </div>

                </div>
                <div class="col-md-3">
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-lg-12 action-button">
                            <button class="btn-danger btn">Save</button>
                            <a class="btn-default btn" href="{{ route('view_users') }}">Cancel</a>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>



@endsection



@section('js')
    
    <script>
        $(document).ready(function(){
            $('#name').val('{{ $user->name }}');
            $('#email').val('{{ $user->email }}');
            $("#mobile").val('{{ $user->mobile }}');
            $('#description').val('{{ $user->description }}');
        });
    </script>

    <script>
        
        var existingFiles = [
            { name: "{{ $user->picture }}", size: {{ Storage::disk('local')->size('public/'.$user->picture) }}, accepted: true }
        ];

    </script>

    <script src="{{ asset('app/user_edit.js') }}"></script>

@endsection