@extends('layouts.dashboard')

@section('main-content')

<div class="row">
    <!--col-md-12 start-->
    <div class="col-md-12">
        <div class="page-heading">
            <h1>
                Update Password
            </h1>
        </div>
    </div>
    <!--col-md-12 start-->
</div>

@if (session('status'))
<div class="alert alert-success alert-dismissible fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Success!</strong> {{ session('status') }}
</div>
@endif

@if (session('error'))
<div class="alert alert-danger alert-dismissible fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Error!</strong> {{ session('error') }}
</div>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="box-info">
            <h3>Information</h3>
            <hr>

            <form id="form-update-password" class="form-horizontal row-border user-form" action="{{ route('update_password') }}" method="POST">


                @foreach ($errors->all() as $message)

                    <div class="alert alert-danger alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error!</strong> {{ $message }}.
                    </div>

                @endforeach


                {{ csrf_field() }}
                
                <input type="hidden" name="user_id" value="{{ Auth::id() }}">

                <div class="col-md-9">

                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Current Password</label>
                        <div class="col-sm-6">
                            <input type="password" name="current" id="current" class="form-control">
                        </div>
                    </div>
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">New Password</label>
                        <div class="col-sm-6">
                            <input type="password" name="new" id="new" class="form-control">
                        </div>
                    </div>
                    
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Confirm Password</label>
                        <div class="col-sm-6">
                            <input type="password" name="confirm" id="confirm" class="form-control">
                        </div>
                    </div>

                </div>
                <div class="col-md-3">
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-lg-12 action-button">
                            <button class="btn-danger btn">Change</button>
                            <a class="btn-default btn" href="{{ route('home') }}">Cancel</a>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>



@endsection



@section('js')
    <script src="{{ asset('app/password.js') }}"></script>

@endsection