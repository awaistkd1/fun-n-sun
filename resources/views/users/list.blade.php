@extends('layouts.dashboard')

@section('main-content')

<!--row start-->
<div class="row">
    <!--col-md-12 start-->
    <div class="col-md-12">
        <div class="page-heading">
            <h1>Users<small> Listing</small></h1>
        </div>
    </div>
    <!--col-md-12 end-->
</div>
<!--row end-->
@if (session('status'))
<div class="alert alert-success alert-dismissible fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Success!</strong> {{ session('status') }}
</div>
@endif

<div class="row" id="filter">
    <form id="search-form">
        <div class="form-group col-sm-3 col-xs-6">
            <input type="text" id="name" name="name" class="form-control" placeholder="Name">
        </div>
        <div class="form-group col-sm-3 col-xs-6">
            <input type="text" id="email" name="email" class="form-control" placeholder="Email">
        </div>
        <div class="form-group col-sm-3 col-xs-6">
            <input type="text" id="phone" name="phone" class="form-control" placeholder="Phone">
        </div>
        <div class="form-group col-sm-3 col-xs-6">
            <select id="user_group" class="filter form-control">
                <option value="">Select Group</option>
                @foreach($user_groups as $group)
                <option value="{{ $group->id }}">{{ $group->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-sm-3 col-xs-6 pull-right">
            <button type="submit" class="btn btn-block btn-primary">Search</button>
        </div>       
    </form>

</div>


<!--row start-->
<div class="row">
    <div class="col-md-12" style="text-align: right;">
        <a href="{{ route('add_users') }}" class="btn btn-success btn-sm">Add more</a>
    </div>    
    <!--col-md-12 start-->
    <div class="col-md-12">
        <table id="all-users-table" class="display table table-bordered table-striped nowrap" style="width:100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>User Group</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
    <!--col-md-12 end-->
</div>
<!--row end-->

@endsection


@section('js')
<script src="{{ asset('app/users.js') }}"></script>

<script>

var oTable = $('#all-users-table').DataTable({
    dom: "lrtp",
    processing: true,
    serverSide: true,
    responsive: {
        details: {
            display: $.fn.dataTable.Responsive.display.modal( {
                header: function ( row ) {
                    var data = row.data();
                    console.log(data);
                    return 'Details:';
                }
            } ),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                tableClass: 'table'
            } )
        }
    },
    ajax:{
        "url": '{{ route("get_users_ajax") }}',
        "dataType": "json",
        "type": "POST",
        data: function (d) {
            d._token = "{{ csrf_token() }}";
            d.name = $('#name').val();
            d.email = $('#email').val();
            d.phone = $('#phone').val();
            d.user_group = $('#user_group').val();
        }
    },
    "columns":[
        { data: "id" },
        { 
            data: "name",
            render : function(data, type, row, meta){
                if(type === 'display'){

                    data = '<a href="{{route('view_user_by_id')}}/?id='+row.id+'">' + data + '</a>';
                }
                return data;
            }
        },
        { data: 'email'},
        { data: 'mobile'},
        { data: "user_group" },

        {
            data: null,
            render: function(data, type, row, meta){
                if(type === 'display'){
                    data = '<form class="form-action" action="{{ route('edit_user') }}" method="GET">{{csrf_field()}} <input type="hidden" name="user_id" value="'+row.id+'" /> <button type="submit"><i class="fa fa-pencil" style="font-size:18px;margin:0 6px;"></i></button></form>'+'<form method="POST" action="{{route('delete_user')}}" class="form-action" action="">{{csrf_field()}} <input type="hidden" name="user_id" value="'+row.id+'" /> <button type="submit"><i class="fa fa-trash-o" style="font-size:18px;margin:0 6px;"></i></button></form>';
                }
                return data;
            }
        }
    ]  
});


$("#search-form").submit(function(e){
    e.preventDefault();
    oTable.draw();
});

</script>

@endsection