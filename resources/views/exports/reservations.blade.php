<table>
    <tr><td>Date</td><td>{{$date}}</td></tr>
</table>

<table>
    <thead>
    <tr>
        @foreach($columns as $col)
        <th>{{$col}}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($reservations as $res)
        <tr>
            <td>{{ $res['time'] }}</td>
            <td>{{ $res['total_amount'] }}</td>
            <td>{{ $res['mobile'] }}</td>
            @foreach($res['extra_services'] as $ex)
            <td>{{ $ex['price'] }}</td>
            <td>{{ $ex['qty'] }}</td>
            @endforeach

            <td>{{ $res['unpaid'] }}</td>
            <td>{{ $res['paid_amount'] }}</td>
            <td>{{ $res['coupon_code'] }}</td>
            <td>{{ $res['infant_qty'] }}</td>
            <td>{{ $res['child_qty'] }}</td>
            <td>{{ $res['adult_qty'] }}</td>
            <td>{{ $res['agent'] }}</td>
            <td>{{ $res['customer'] }}</td>
            <td>{{ $res['reservation_id'] }}</td>
        </tr>
    @endforeach
    </tbody>
</table>