<table>
    <tr><td>From Date:</td><td>{{$from_date}}</td></tr>
    <tr><td>To Date:</td><td>{{$to_date}}</td></tr>    
</table>

<table>
    <thead>
    <tr>
        @foreach($columns as $col)
        <th>{{$col}}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($trips as $trip)
        <tr>
            <td>{{ $trip['trip_id'] }}</td>
            <td>{{ $trip['date'] }}</td>
            <td>{{ $trip['start_time'] }}</td>
            <td>{{ $trip['end_time'] }}</td>
            <td>{{ $trip['total_adult'] }}</td>
            <td>{{ $trip['total_child'] }}</td>
            <td>{{ $trip['total_infant'] }}</td>
            <td>{{ $trip['total_pax'] }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

<table>
    <tr><td>Total</td><td>{{$total_pax_all_trip}}</td></tr>
    <tr><td>Trip Count</td><td>{{count($trips)}}</td></tr>    
</table>
