@extends('layouts.app')

@section('content')


<div class="container login-bg">
    <form action="{{ route('login') }}" method="POST" class="login-form-signin">
        
        {{ csrf_field() }}

        <div class="login-logo"><img src="images/logo.png"></div>
        <h2 class="login-form-signin-heading">Login Your Account</h2>

        <div class="login-wrap">
            <input type="text" autofocus placeholder="User ID" class="form-control" name="email" value="{{ old('email') }}" />
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif

            <input type="password" name="password" placeholder="Password" class="form-control">
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
            
            <label class="checkbox">
                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
            </label>

            <label class="checkbox">
                <span class="pull-right">
                    <a href="{{ route('password.request') }}">Forgot Password?</a>
                </span>
            </label>

            <button type="submit" class="btn btn-lg btn-primary btn-block">Sign in</button>
        </div>
    </form>
</div>


@endsection
