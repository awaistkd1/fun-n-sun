@extends('layouts.dashboard')

@section('main-content')

<style type="text/css">
    
    .box-info{
        padding: 20px;
    }

    .form-horizontal .form-group{
        margin-left: 0;
        margin-right: 0;
    }
</style>
<!--row start-->
<div class="row">
    <!--col-md-12 start-->
    <div class="col-md-12">
        <div class="page-heading">
            <h1>Reports</h1>
        </div>
    </div>
    <!--col-md-12 end-->
</div>
<!--row end-->
@if (session('status'))
<div class="alert alert-success alert-dismissible fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Success!</strong> {{ session('status') }}
</div>
@endif

<!--row start-->
<div class="row">
    <div class="col-md-6">
        <div class="box-info">
            <h2>Bookins per Agent</h2>
            <form action="{{ route('agent_report') }}" method="GET" class="form-horizontal">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="date-field">Select Agent</label>
                    <select class="form-control" name="agent_id" required>
                        @foreach($agents as $agent)
                        <option value="{{$agent->id}}">{{$agent->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="date-field">Start Date</label>
                    <input type="date" id="start_date" class="form-control" style="width: 300px" name="start_date" />
                </div>

                <div class="form-group">
                    <label for="date-field">End Date</label>
                    <input type="date" id="end_date" class="form-control" style="width: 300px" name="end_date" />
                </div>

                <button type="submit" class="btn btn-primary">Generate</button>
            </form>    
        </div>
    </div>

    <div class="col-md-6">
        <div class="box-info">
            <h2>Bookings per Asset</h2>
            <form action="{{ route('assets_report') }}" method="GET" class="form-horizontal">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="date-field">Start Date</label>
                    <input type="date" id="from_date" class="form-control" style="width: 300px" name="from_date" />
                </div>

                <div class="form-group">
                    <label for="date-field">End Date</label>
                    <input type="date" id="to_date" class="form-control" style="width: 300px" name="to_date" />
                </div>

                <button type="submit" class="btn btn-primary">Generate</button>
            </form>
        </div>
    </div>


</div>

<div class="row">
    <div class="col-md-6">
        <div class="box-info">
            <h2>Reservations By Date</h2>
            <form action="{{ route('reservation_report') }}" method="GET" class="form-horizontal">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="date-field">Select Trip</label>
                    <select class="form-control" name="trip_id" required>
                        @foreach($trips as $trip)
                        <option value="{{$trip->id}}">{{$trip->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="date-field">Date</label>
                    <input type="date" id="date-field" class="form-control" style="width: 300px" name="date" />
                </div>
                <button type="submit" class="btn btn-primary">Generate</button>
            </form>
    
        </div>
    </div>

</div>
<!--row end-->

@endsection


@section('js')
<script src="{{ asset('app/reports.js') }}"></script>
@endsection