
@extends('layouts.dashboard')

@section('main-content')

<div class="row">
    <!--col-md-12 start-->
    <div class="col-md-12">
        <div class="page-heading">
            <h1>
                Add New User Group
            </h1>
        </div>
    </div>
    <!--col-md-12 start-->
</div>


<div class="row">
    <div class="col-md-12">
        <div class="box-info">
            <h3>Information</h3>
            <hr>
            
            @foreach ($errors->all() as $message)
                <div class="alert alert-danger alert-dismissible fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Error!</strong> {{ $message }}.
                </div>
            @endforeach
            
            <form id="form-user-group" class="form-horizontal row-border" action="{{ route('save_user_group') }}" method="POST">

                {{ csrf_field() }}

                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group lable-padd">
                                <label class="col-sm-3">Name</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="name" id="name" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        @for ($i = 0; $i < count($system); $i++)

                        <?php
                        $systemType = $system[$i];
                        $systemHeading = str_replace("-", " ", $systemType);
                        ?>

                        <div class="col-md-6">
                            <div class="form-group lable-padd">
                                <label class="col-sm-3" style="text-transform:capitalize">{{ $systemHeading }}</label>
                                <div class="col-sm-6">
                                    <div class="skin-section">
                                    <ul class="list">
                                        <li>
                                        <input type="checkbox" id="{{$systemType}}_add" name="{{$systemType}}_add" value="1">
                                        <label for="{{ $systemType}}_add">Add</label>
                                        </li>
                                        <li>
                                        <input type="checkbox" id="{{ $systemType }}_update" name="{{ $systemType }}_update" value="1" />
                                        <label for="{{ $systemType}}_edit">Edit</label>
                                        </li>
                                        <li>
                                        <input type="checkbox" id="{{ $systemType }}_delete" name="{{ $systemType }}_delete" value="1" />
                                        <label for="{{ $systemType }}_delete">Delete</label>
                                        </li>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endfor
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group lable-padd">
                                <label class="col-sm-3">Description</label>
                                <div class="col-sm-6">
                                    <textarea class="form-control" name="description" id="description"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-lg-12 action-button">
                            <button class="btn-danger btn">Submit</button>
                            <a class="btn-default btn" href="{{ route('home') }}">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection



@section('js')
<script src="{{ asset('app/group-rights.js') }}"></script>
@endsection