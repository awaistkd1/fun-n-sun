
@extends('layouts.dashboard')

@section('main-content')

<div class="row">
    <!--col-md-12 start-->
    <div class="col-md-12">
        <div class="page-heading">
            <h1>
                Add New User Group
            </h1>
        </div>
    </div>
    <!--col-md-12 start-->
</div>


<div class="row">
    <div class="col-md-12">
        <div class="box-info">
            <h3>Information</h3>
            <hr>
            
            @foreach ($errors->all() as $message)
                <div class="alert alert-danger alert-dismissible fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Error!</strong> {{ $message }}.
                </div>
            @endforeach
            
            <form id="form-user-group" class="form-horizontal row-border" action="{{ route('update_user_group') }}" method="POST">

                {{ csrf_field() }}
                <input type="hidden" name="group_id" value="{{ $group->id }}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group lable-padd">
                                <label class="col-sm-3">Name</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="name" id="name" value="{{ $group->name }}" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        @for ($i = 0; $i < count($system); $i++)

                        <?php
                        $systemType = $system[$i];
                        $systemHeading = str_replace("-", " ", $systemType);
                        ?>

                        <div class="col-md-6">
                            <div class="form-group lable-padd">
                                <label class="col-sm-3" style="text-transform:capitalize">{{ $systemHeading }}</label>
                                <div class="col-sm-6">
                                    <div class="skin-section">
                                    <ul class="list">
                                        <li>
                                        <input type="checkbox" id="{{$systemType}}_add" name="{{$systemType}}_add" value="1">
                                        <label for="{{ $systemType}}_add">Add</label>
                                        </li>
                                        <li>
                                        <input type="checkbox" id="{{ $systemType }}_update" name="{{ $systemType }}_update" value="1" />
                                        <label for="{{ $systemType}}_edit">Edit</label>
                                        </li>
                                        <li>
                                        <input type="checkbox" id="{{ $systemType }}_delete" name="{{ $systemType }}_delete" value="1" />
                                        <label for="{{ $systemType }}_delete">Delete</label>
                                        </li>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endfor
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group lable-padd">
                                <label class="col-sm-3">Description</label>
                                <div class="col-sm-6">
                                    <textarea class="form-control" name="description" id="description">{{ $group->description }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-lg-12 action-button">
                            <button class="btn-danger btn">Submit</button>
                            <a class="btn-default btn" href="{{ route('view_user_group') }}">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection



@section('js')
<script src="{{ asset('app/group-rights.js') }}"></script>

<script>
    $(document).ready(function(){



        $("#assets_add").iCheck(@if( $group->assets_add == 1 ) 'check' @else 'uncheck' @endif);
        $("#assets_update").iCheck(@if( $group->assets_update == 1 ) 'check' @else 'uncheck' @endif);
        $("#assets_delete").iCheck(@if( $group->assets_delete == 1 ) 'check' @else 'uncheck' @endif);
        

        $("#trips_add").iCheck(@if( $group->trips_add == 1 ) 'check' @else 'uncheck' @endif);
        $("#trips_update").iCheck(@if( $group->trips_update == 1 ) 'check' @else 'uncheck' @endif);
        $("#trips_delete").iCheck(@if( $group->trips_delete == 1 ) 'check' @else 'uncheck' @endif);
        
        
        $("#bookings_add").iCheck(@if( $group->bookings_add == 1 ) 'check' @else 'uncheck' @endif);
        $("#bookings_update").iCheck(@if( $group->bookings_update == 1 ) 'check' @else 'uncheck' @endif);
        $("#bookings_delete").iCheck(@if( $group->bookings_delete == 1 ) 'check' @else 'uncheck' @endif);
        
        
        $("#customers_add").iCheck(@if( $group->customers_add == 1 ) 'check' @else 'uncheck' @endif);
        $("#customers_update").iCheck(@if( $group->customers_update == 1 ) 'check' @else 'uncheck' @endif);
        $("#customers_delete").iCheck(@if( $group->customers_delete == 1 ) 'check' @else 'uncheck' @endif);
        
        
        $("#sales_agents_add").iCheck(@if( $group->sales_agents_add == 1 ) 'check' @else 'uncheck' @endif);
        $("#sales_agents_update").iCheck(@if( $group->sales_agents_update == 1 ) 'check' @else 'uncheck' @endif);
        $("#sales_agents_delete").iCheck(@if( $group->sales_agents_delete == 1 ) 'check' @else 'uncheck' @endif);

        
        $("#coupons_add").iCheck(@if( $group->coupons_add == 1 ) 'check' @else 'uncheck' @endif);
        $("#coupons_update").iCheck(@if( $group->coupons_update == 1 ) 'check' @else 'uncheck' @endif);
        $("#coupons_delete").iCheck(@if( $group->coupons_delete == 1 ) 'check' @else 'uncheck' @endif);

    });
</script>
@endsection