
@extends('layouts.dashboard')

@section('main-content')

<div class="row">
    <!--col-md-12 start-->
    <div class="col-md-12">
        <div class="page-heading">
            <h1>
                Edit Customer
            </h1>
        </div>
    </div>
    <!--col-md-12 start-->
</div>


<div class="row">
    <div class="col-md-12">
        <div class="box-info">
            <h3>Information</h3>
            <hr>
            <form id="form-customer" class="form-horizontal row-border" action="{{route('save_edit_customer')}}" method="POST">

                {{csrf_field()}}
                @foreach ($errors->all() as $message)

                    <div class="alert alert-danger alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error!</strong> {{ $message }}.
                    </div>

                @endforeach
                <input type="hidden" name="customer_id" value="{{ $customer->id }}" />
                <div class="col-md-6">
                    <div class="form-group lable-padd">
                            <label class="col-sm-3">First Name</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="first_name" value="{{$customer->first_name}}" id="first_name" />
                        </div>
                    </div>

                    <div class="form-group lable-padd">
                            <label class="col-sm-3">Middle Name</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="middle_name" value="{{$customer->middle_name}}" id="middle_name" />
                        </div>
                    </div>

                    <div class="form-group lable-padd">
                            <label class="col-sm-3">Last Name</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="last_name" value="{{$customer->last_name}}" id="last_name" />
                        </div>
                    </div>

                    <div class="form-group lable-padd">
                            <label class="col-sm-3">Date of Birth</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control default-date-picker" value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $customer->dob)->format('m-d-Y') }}" name="dob" id="dob" />
                        </div>
                    </div>


                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Email</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="email" value="{{ $customer->email }}" id="email" />
                        </div>
                    </div>
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Mobile #</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="mobile" value="{{ $customer->phone }}" id="mobile" />
                        </div>
                    </div>
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Description</label>
                        <div class="col-sm-6">
                            <textarea class="form-control" name="description" id="description"></textarea>
                        </div>
                    </div>


                    <div class="form-group lable-padd">
                            <label class="col-sm-3">Referral Name</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="referral_name" value="{{ $customer->referral_name }}" id="referral_name" />
                        </div>
                    </div>

                    <div class="form-group lable-padd">
                            <label class="col-sm-3">Referral Email</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="referral_email" value="{{ $customer->referral_email }}" id="referral_email" />
                        </div>
                    </div>


                </div>
                <div class="col-md-6">
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-lg-12 action-button">
                            <button type="submit" class="btn-danger btn">Save</button>
                            <a class="btn-default btn" href="{{ route('view_customer') }}">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection



@section('js')
<script src="{{ asset('app/customer.js') }}"></script>
@endsection