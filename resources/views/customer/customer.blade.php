@extends('layouts.dashboard')


@section('main-content')

<style>
	.gallery-wrap .img-big-wrap img {
	    height: 450px;
	    width: auto;
	    display: inline-block;
	    cursor: zoom-in;
	}


	.gallery-wrap .img-small-wrap .item-gallery {
	    width: 60px;
	    height: 60px;
	    border: 1px solid #ddd;
	    margin: 7px 2px;
	    display: inline-block;
	    overflow: hidden;
	}

	.gallery-wrap .img-small-wrap {
	    text-align: center;
	}
	.gallery-wrap .img-small-wrap img {
	    max-width: 100%;
	    max-height: 100%;
	    object-fit: cover;
	    border-radius: 4px;
	    cursor: zoom-in;
	}
</style>

    <div class="row">
        <!--col-md-12 start-->
        <div class="col-md-12">
            <div class="page-heading">
                <h1>Customer<small> Details</small></h1>
            </div>
        </div>
        <!--col-md-12 end-->
    </div>

	@if (session('status'))
	<div class="alert alert-success alert-dismissible fade in">
	    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	    <strong>Success!</strong> {{ session('status') }}
	</div>
	@endif    


    <div class="row">
        <div class="col-md-12">
            <div class="box-info">

				<div class="card">
				    <div class="row">
				        <aside class="col-sm-12">
				            <article class="card-body p-5">
				                
				                <!-- price-detail-wrap .// -->
				                <dl class="item-property">
				                    <dt>First Name</dt>
				                    <dd>
				                        <p>{{ $customer->first_name }}</p>
				                    </dd>
				                </dl>
				                <dl class="param param-feature">
				                    <dt>Middle Name</dt>
				                    <dd>{{ $customer->middle_name }}</dd>
				                </dl>
				                <!-- item-property-hor .// -->
				                <dl class="param param-feature">
				                    <dt>Last Name</dt>
				                    <dd>{{$customer->last_name}}</dd>
				                </dl>
								<!-- item-property-hor .// -->
				                <dl class="param param-feature">
				                    <dt>Description</dt>
				                    <dd>{{$customer->description}}</dd>
				                </dl>				                
				                <!-- item-property-hor .// -->
				                <dl class="param param-feature">
				                    <dt>Phone</dt>
				                    <dd>{{ $customer->phone }}</dd>
				                </dl>
				                <!-- item-property-hor .// -->
				                <dl class="param param-feature">
				                    <dt>Email</dt>
				                    <dd>{{ $customer->email }}</dd>
				                </dl>

				                <!-- item-property-hor .// -->
				                <dl class="param param-feature">
				                    <dt>Dob</dt>
				                    <dd>{{ $customer->dob }}</dd>
				                </dl>

				                <!-- item-property-hor .// -->
				                <dl class="param param-feature">
				                    <dt>Referral Name</dt>
				                    <dd>{{ $customer->referral_name }}</dd>
				                </dl>

				                <!-- item-property-hor .// -->
				                <dl class="param param-feature">
				                    <dt>Referral Email</dt>
				                    <dd>{{ $customer->referral_email }}</dd>
				                </dl>

				                <!-- item-property-hor .// -->
				                <dl class="param param-feature">
				                    <dt>Created By</dt>
				                    <dd>{{ App\User::where('id','=',$customer->created_by)->first()->name  }}</dd>
				                </dl>				                        
				                <!-- item-property-hor .// -->
				                <hr>

				            </article>
				            <!-- card-body.// -->
				        </aside>
				        <!-- col.// -->
				    </div>
				    <!-- row.// -->
				</div>
            </div>
        </div>
    </div>
@endsection


@section('js')
<script src="{{asset('app/customer.js')}}"></script>
@endsection