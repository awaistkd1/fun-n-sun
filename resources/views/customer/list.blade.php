@extends('layouts.dashboard')

@section('main-content')

<!--row start-->
<div class="row">
    <!--col-md-12 start-->
    <div class="col-md-12">
        <div class="page-heading">
            <h1>Assets<small> Listing</small></h1>
        </div>
    </div>
    <!--col-md-12 end-->
</div>
<!--row end-->
@if (session('status'))
<div class="alert alert-success alert-dismissible fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Success!</strong> {{ session('status') }}
</div>
@endif

<div class="row" id="filter">
    <form id="filter-form">
        <div class="form-group col-sm-3 col-xs-6">
            <input class="form-control" id="search-text" type="text" placeholder="Search" />
        </div>
        <div class="form-group col-sm-3 col-xs-6">
            <input type="text" placeholder="Phone" id="phone" name="phone" class="form-control">
        </div>
        <div class="form-group col-sm-3 col-xs-6">
            <input type="text" placeholder="Email" id="email" name="email" class="form-control">
        </div>
        <div class="form-group col-sm-3 col-xs-6">
            <button type="submit" class="btn btn-block btn-primary">Search</button>
        </div>
    </form>
</div>


<!--row start-->
<div class="row">
    <div class="col-md-12" style="text-align: right;">
        <a href="{{ route('add_customer') }}" class="btn btn-success btn-sm">Add more</a>
    </div>    
    <!--col-md-12 start-->
    <div class="col-md-12">
        <table id="all-assets-table" class="display table table-bordered table-striped nowrap" style="width:100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>First Name</th>
                    <th>Middle Name</th>
                    <th>Last Name</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Dob</th>
                    <th>Referral Name</th>
                    <th>Referral Email</th>
                    @if( ($user_group->customers_delete == 0) and ($user_group->customers_update == 0))
                    
                    @else
                    <th>Actions</th>
                    @endif
                </tr>
            </thead>
        </table>
    </div>
    <!--col-md-12 end-->
</div>
<!--row end-->

@endsection


@section('js')
<script src="{{ asset('app/customer.js') }}"></script>

<script>

var oTable = $('#all-assets-table').DataTable({
    dom: "lrtp",
    processing: true,
    serverSide: true,
    responsive: {
        details: {
            display: $.fn.dataTable.Responsive.display.modal( {
                header: function ( row ) {
                    var data = row.data();
                    console.log(data);
                    return 'Details:';
                }
            } ),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                tableClass: 'table'
            } )
        }
    },
    ajax:{
        "url": '{{ route("get_customers_ajax") }}',
        "dataType": "json",
        "type": "POST",
        data: function (d) {
            d._token = "{{ csrf_token() }}";
            d.name = $('#search-text').val();
            d.phone = $('#phone').val();
            d.email = $('#email').val();
        }
    },
    "columns":[
        { data: "id" },
        { 
            data: "first_name",
            render : function(data, type, row, meta){
                if(type === 'display'){

                    data = '<a href="{{route('view_customer_by_id')}}/?id='+row.id+'">' + data + '</a>';
                }
                return data;
            }
        },
        { data: 'middle_name'},
        { data: 'last_name'},
        { data: "phone" },
        { data: "email" },
        { data: "dob" },
        { data: "referral_name" },
        { data: "referral_email" }
        @if( ($user_group->customers_delete == 0) and ($user_group->customers_update == 0))

        @else
        ,{
            data: null,
            render: function(data, type, row, meta){
                if(type === 'display'){
                    data = '@if($user_group->customers_update == 1)<form class="form-action" action="{{ route('edit_customer') }}" method="GET">{{csrf_field()}} <input type="hidden" name="customer_id" value="'+row.id+'" /> <button type="submit"><i class="fa fa-pencil" style="font-size:18px;margin:0 6px;"></i></button></form>@endif'+'@if($user_group->customers_delete == 1)<form method="POST" action="{{route('delete_customer')}}" class="form-action" action="">{{csrf_field()}} <input type="hidden" name="customer_id" value="'+row.id+'" /> <button type="submit"><i class="fa fa-trash-o" style="font-size:18px;margin:0 6px;"></i></button></form>@endif';
                }
                return data;
            }
        }
        @endif
    ]  
});


$("#filter-form").submit(function(e){
    e.preventDefault();
    oTable.draw();
});

</script>

@endsection