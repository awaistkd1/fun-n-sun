




@extends('layouts.dashboard')

@section('main-content')

<style type="text/css">
    .dropzone{
        margin-bottom: 20px;
    }
</style>


<div class="row">
    <!--col-md-12 start-->
    <div class="col-md-12">
        <div class="page-heading">
            <h1>
                Add New Trip
            </h1>
        </div>
    </div>
    <!--col-md-12 start-->
</div>


<div class="row">
    <div class="col-md-12">
        <div class="box-info">
            <h3>Information</h3>
            <hr>
            <form id="form-trip" class="form-horizontal row-border trip-form" action="{{route('save_trip')}}" method="POST">

                @foreach ($errors->all() as $message)

                    <div class="alert alert-danger alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error!</strong> {{ $message }}.
                    </div>

                @endforeach

                {{ csrf_field() }}
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group lable-padd">
                                <label class="col-sm-3">Asset</label>
                                <div class="col-sm-6">
                                    <Select name="asset_id" id="asset_id" class="form-control">
                                        <option value="">Select</option>
                                        @foreach($assets as $asset)
                                            <option value="{{$asset->id}}">{{$asset->name}}</option>
                                        @endforeach
                                    </Select>
                                </div>
                            </div>
                            <div class="form-group lable-padd">
                                <label class="col-sm-3">Name</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="name" id="name" />
                                </div>
                            </div>

                            <div class="form-group lable-padd">
                                <label class="col-sm-3">Repeat</label>
                                <div class="col-sm-6">
                                    <Select id="repeat" name="repeat" class="form-control">
                                        <option value="daily">Daily</option>
                                        <option value="weekly">Weekly</option>
                                        <option value="none">None</option>
                                    </Select>
                                </div>
                            </div>

                            <div class="form-group lable-padd">
                                <label class="col-sm-3">Date</label>
                                <div class="col-sm-6">
                                    <input class="form-control form-control-inline input-medium default-date-picker"  size="16" type="text" value="" id="date" />
                                </div>
                            </div>
                            <div class="form-group lable-padd">
                                <label class="col-sm-3">Start Time</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="start_time" id="start_time" />
                                </div>
                            </div>
                            <div class="form-group lable-padd">
                                <label class="col-sm-3">End Time</label>
                                <div class="col-sm-6">
                                    <input type="text" name="end_time" id="end_time" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group lable-padd">
                                <label class="col-sm-3">Description</label>
                                <div class="col-sm-6">
                                    <textarea name="description" id="description" class="form-control" placeholder=""></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <h2>Price List with Sale Agent Groups Discount</h2>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group lable-padd">
                                <label class="col-sm-12">Adult (Price)</label>
                                <div class="col-sm-12">

                                    <div class="input-group">
                                        <input type="text" class="form-control" name="adult_price" id="adult_price" />
                                        <div class="input-group-addon">{{ $currency }}</div>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group lable-padd">
                                <label class="col-sm-12"><strong>Discount</strong></label>
                            </div>
                            @foreach ($user_groups as $group)
                            <div class="form-group lable-padd">
                                <label class="col-sm-12 user_group_title" data-id="{{$group->id}}">{{$group->name}}</label>
                                <div class="col-sm-12">

                                    <div class="input-group">
                                        <input type="text" class="form-control adult_price" name="adult_price_{{$group->id}}" id="adult_price_{{$group->id}}" required="required" />
                                        <div class="input-group-addon">{{ $currency }}</div>
                                    </div>

                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="col-md-2">
                            <div class="form-group lable-padd">
                                <label class="col-sm-12">Child (Price)</label>
                                <div class="col-sm-12">
                                    
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="child_price" id="child_price"/>
                                        <div class="input-group-addon">{{ $currency }}</div>
                                    </div>


                                </div>
                            </div>
                            <div class="form-group lable-padd">
                                <label class="col-sm-12"><strong>&nbsp;</strong></label>
                            </div>
                            @foreach ($user_groups as $group)
                            <div class="form-group lable-padd">
                                <label class="col-sm-12">&nbsp;</label>
                                <div class="col-sm-12">

                                    <div class="input-group">
                                        <input type="text" class="form-control" name="child_price_{{$group->id}}" id="child_price_{{$group->id}}" required="required">
                                        <div class="input-group-addon">{{ $currency }}</div>
                                    </div>

                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="col-md-2">
                            <div class="form-group lable-padd">
                                <label class="col-sm-12">Infant (Price)</label>
                                <div class="col-sm-12">

                                    <div class="input-group">
                                        <input type="text" class="form-control" name="infant_price" id="infant_price" />
                                        <div class="input-group-addon">{{ $currency }}</div>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group lable-padd">
                                <label class="col-sm-12"><strong>&nbsp;</strong></label>
                            </div>
                            @foreach ($user_groups as $group)
                            <div class="form-group lable-padd">
                                <label class="col-sm-12">&nbsp;</label>
                                <div class="col-sm-12">
                                    
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="infant_price_{{$group->id}}" id="infant_price_{{$group->id}}" required="required" />    
                                        <div class="input-group-addon">{{ $currency }}</div>
                                    </div>

                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <h2>Other Services</h2>
                            <div id="trip_services_list" class="form-group lable-padd">
                            </div>
                            <div class="form-group lable-padd">
                                <label class="col-sm-3">
                                    <div id="add_more_trip_services_btn" style="cursor:pointer;float:left;">
                                        <i class="fa fa-plus"></i>
                                        <span>Add More</span>
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <h2>Trip Photos</h2>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="dropzone"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>                

                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-lg-12 action-button">
                            <button type="submit" class="btn-danger btn">Submit</button>
                            <a class="btn-default btn" href="#">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<div id="trip_services_list_append" style="display:none">
    <div class="trip_services_list">
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12">
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Name</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="name" required="required">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Price</label>
                        <div class="col-sm-6">

                            <div class="input-group">
                                <input type="text" class="form-control" name="price" required="required">    
                                <div class="input-group-addon">{{ $currency }}</div>
                            </div>


                            
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group lable-padd">
                        <label class="col-sm-12"><strong>Discount</strong></label>
                    </div>
                </div>
                @foreach($user_groups as $group)
                <div class="col-md-12">
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">{{$group->name}}</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <input type="text" class="form-control service_discount_{{$group->id}}" name="discount" required="required">
                                <div class="input-group-addon">{{ $currency }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

</div>

@endsection



@section('js')
<script src="{{ asset('app/trip.js') }}"></script>
@endsection