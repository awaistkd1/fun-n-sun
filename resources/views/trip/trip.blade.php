@extends('layouts.dashboard')


@section('main-content')

<style>

.carousel{
	width: 100%;
	height: 550px;
}

.middle {
  position: relative;
  height: 550px;
}

.middle img {
  max-width: 100%;
  max-height:100%;
  margin: auto;
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
}

.box-info{
	margin-top: 10px;
	margin-bottom: 10px; 
}

.table td,.table th {
   text-align: center;   
}

</style>

    <div class="row">
        <!--col-md-12 start-->
        <div class="col-md-12">
            <div class="page-heading">
                <h1>Trip<small> Details</small></h1>
            </div>
        </div>
        <!--col-md-12 end-->
    </div>

	@if (session('status'))
	<div class="alert alert-success alert-dismissible fade in">
	    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	    <strong>Success!</strong> {{ session('status') }}
	</div>
	@endif 

	@if(Auth::id() != 1)
		@if($trip->status == \Config::get('trip.close'))
		<div class="alert alert-info alert-dismissible fade in">
		    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		    <strong>Info!</strong> Booking for this trip is closed by admin.
		</div>
		@endif

		@if($trip->status == \Config::get('trip.cancel'))
		<div class="alert alert-info alert-dismissible fade in">
		    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		    <strong>Info!</strong> This trip is canceled.
		</div>
		@endif
	@endif

	@if($tickets_left == 0)
		<div class="alert alert-danger alert-dismissible fade in">
		    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		    <strong></strong>No seats left.
		</div>
	@endif
	@if(Auth::id() != 1)
		@if($my_limit == 0)
			<div class="alert alert-danger alert-dismissible fade in">
			    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			    <strong></strong>You have reached your maximum booking limit.
			</div>		
		@endif
	@endif

	<div class="row">
		<div class="col-md-12">
			<div id="trip-img-slider" class="carousel slide" data-ride="carousel">
			    <!-- Indicators -->
			    <ol class="carousel-indicators">
			    	@if($trip->tripImages()->count())
			    	@foreach($trip->tripImages()->get() as $img)
			        <li data-target="#trip-img-slider" data-slide-to="{{ $loop->iteration-1 }}" @if($loop->iteration == 1) class="active" @endif ></li>
			        @endforeach
			        @else
			        <li data-target="#trip-img-slider" data-slide-to="0" class="active"></li>
			        @endif
			    </ol>
			    <!-- Wrapper for slides -->
			    <div class="carousel-inner" role="listbox">
			    	@if($trip->tripImages()->count())
			    	@foreach($trip->tripImages()->get() as $img)
			        <div class="item @if($loop->iteration == 1) active @endif">
			            <div class="middle">
			            	<img src="{{ Storage::disk('local')->url( $img->path ) }}" alt="{{$loop->iteration}}">
			        	</div>
			        </div>

			        @endforeach
			        @else
					<div class="item active">
						<div class="middle">
			            	<img src="{{ Storage::disk('local')->url( 'no-image.png' ) }}" alt="0">
			        	</div>
			        </div>       
			        @endif
			    </div>
			    <!-- Controls -->
			    <a class="left carousel-control" href="#trip-img-slider" role="button" data-slide="prev">
			    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			    </a>
			    <a class="right carousel-control" href="#trip-img-slider" role="button" data-slide="next">
			    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			    </a>
			</div>			
		</div>
	</div>

    <div class="row">
        <div class="col-md-12">
        	<div class="box-info"><h1>{{ $trip->name }}</h1></div>
            <div class="box-info">
            	<h2>Information</h2>
            	<table class="details-table">
            		<tr>
            			<td class="rowlabel">Asset Name</td>
            			<td class="data">{{ $trip->asset()->first()->name }}</td>
            		</tr>

            		<tr>
            			<td class="rowlabel">Repeat</td>
            			<td class="data">{{ $trip->repeat }}</td>
            		</tr>

            		<tr>
            			<td class="rowlabel">Date</td>
            			<td class="data">{{ $trip->date }}</td>
            		</tr>

            		<tr>
            			<td class="rowlabel">Start Time</td>
            			<td class="data">{{$trip->start_date}}</td>
            		</tr>


            		<tr>
            			<td class="rowlabel">End Time</td>
            			<td class="data">{{ $trip->end_date }}</td>
            		</tr>            		

            		<tr>
            			<td class="rowlabel">Status</td>
            			<td class="data">@if($trip->status == 0) Open @elseif($trip->status == 1) Closed @else Cancled @endif</td>
            		</tr>            		

            	</table>
            </div>

            <div class="box-info">
            	<h2>Description</h2>
            	<p style="font-size: 16px; line-height: 1.6em;">{{ $trip->description }}</p>
            </div>

            <div class="box-info">
            	<h2>Tickets</h2>

            	<table class="details-table">
            		@if(Auth::id() == 1)
            		<tr>
            			<td class="rowlabel">Tickets Available</td>
            			<td class="data">{{ $tickets_left }}</td>
            		</tr>

            		<tr>
            			<td class="rowlabel">Total Tickets Booked</td>
            			<td class="data">{{ $total_tickets_booked }}</td>
            		</tr>
            		@endif

	                @if(Auth::id() != 1)
            		<tr>
            			<td class="rowlabel">Your Bookings Left</td>
            			<td class="data">{{ $my_limit }}</td>
            		</tr>
	                @endif

            	</table>

            </div>

            @if(Auth::id() == 1)
            <div class="box-info">
            	<h2>Revenue</h2>

            	<table class="details-table">
            		<tr>
            			<td class="rowlabel">Total Revenue</td>
            			<td class="data">{{$total_revenue}} {{$currency}}</td>
            		</tr>

            		<tr>
            			<td class="rowlabel">Total Paid</td>
            			<td class="data">{{$total_paid}} {{$currency}}</td>
            		</tr>

            		<tr>
            			<td class="rowlabel">Total Un-Paid</td>
            			<td class="data">{{$total_unpaid}} {{$currency}}</td>
            		</tr>
            	</table>
            </div>
            @endif

            <div class="box-info">
            	<h2>Pricing</h2>

            	<table class="table">
            		<thead>
            			<tr>
            				<th>Adult</th>
            				<th>Child</th>
            				<th>Infant</th>
            			</tr>
            		</thead>
            		<tbody>
            			<tr>
            				<td>{{ $trip->adult_price }} {{ $currency }}</td>
            				<td>{{ $trip->child_price }} {{ $currency }}</td>
            				<td>{{ $trip->infant_price }} {{ $currency }}</td>
            			</tr>
            		</tbody>
            	</table>            	
            </div>

            <div class="box-info">
            	<h2>Discounts</h2>

            	<table class="table">
            		<thead>
            			<tr>
            				<th>User Group</th>
            				<th>Adult</th>
            				<th>Child</th>
            				<th>Infant</th>
            			</tr>
            		</thead>
            		<tbody>
            			@foreach($discounts as $discount)
            			<tr>
            				<td>{{ $discount['user_group'] }}</td>
            				<td>{{ $discount['adult'] }} {{ $currency }}</td>
            				<td>{{ $discount['child'] }} {{ $currency }}</td>
            				<td>{{ $discount['infant'] }} {{ $currency }}</td>
            			</tr>
            			@endforeach
            		</tbody>
            	</table>

            </div>

            <div class="box-info">
            	<h2>Extra Service</h2>
            	
            	<table class="details-table">

                	@foreach($trip->extraServices()->get() as $service)
                	<tr>
                		<td class="rowlabel">{{$service->name}}</td>
                		<td class="data">{{$service->price}} {{ $currency }}</td>
                	</tr>
                	@endforeach
            	</table>            	
            
            </div>

            <div class="box-info">
            	<h2>Actions</h2>

                @if( ($trip->status == \Config::get('trip.open')) && ($my_limit != 0) && ($tickets_left != 0) )
                <a href="{{ route('make_booking', ['id' => $trip->id]) }}" class="btn btn-lg btn-primary text-uppercase"> Make Booking </a>
                
                @elseif(Auth::id() == 1)
                	<a href="{{ route('make_booking', ['id' => $trip->id]) }}" class="btn btn-lg btn-primary text-uppercase"> Make Booking </a>
                @endif

                <a href="{{ route('view_all_bookings_of_trip', $trip->id) }}" class="btn btn-lg btn-outline-primary text-uppercase"> View All Bookings</a>
                
                @if( Auth::id() == 1 )
                <form action="{{route('change_status')}}" method="POST" style="display: inline-block;">
                	{{csrf_field()}}
                	<input type="hidden" name="trip_id" value="{{$trip->id}}" />
                	<input type="hidden" name="status" value="{{ \Config::get('trip.close') }}" />
                	
                	@if($trip->status == \Config::get('trip.open'))
                		<input type="submit" class="btn btn-lg btn-outline-primary text-uppercase" value="Close Booking">
                	@else
                		<input type="submit" class="btn btn-lg btn-outline-primary text-uppercase" disabled="disabled" value="Booking Closed">
                	@endif
                </form>
				<form action="{{route('change_status')}}" method="POST" style="display: inline-block;">
                	{{csrf_field()}}
                	<input type="hidden" name="trip_id" value="{{$trip->id}}" />
                	<input type="hidden" name="status" value="{{ \Config::get('trip.cancel') }}" />
                	
                	@if($trip->status != \Config::get('trip.cancel'))
                		<input type="submit" class="btn btn-lg btn-outline-primary text-uppercase" value="Cancel Booking">
                	@else
                		<input type="submit" class="btn btn-lg btn-outline-primary text-uppercase" disabled="disabled" value="Booking Canceled">
                	@endif
                </form>
                @endif
            </div>
        </div>
    </div>

@endsection


@section('js')
<script src="{{asset('app/trip.js')}}"></script>
@endsection