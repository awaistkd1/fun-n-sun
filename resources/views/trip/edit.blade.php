@extends('layouts.dashboard')

@section('main-content')

<style type="text/css">
    .dropzone{
        margin-bottom: 20px;
    }
</style>


<div class="row">
    <!--col-md-12 start-->
    <div class="col-md-12">
        <div class="page-heading">
            <h1>
                Add New Trip
            </h1>
        </div>
    </div>
    <!--col-md-12 start-->
</div>


<div class="row">
    <div class="col-md-12">
        <div class="box-info">
            <h3>Information</h3>
            <hr>
            <form id="form-trip" class="form-horizontal row-border trip-form" action="{{route('update_trip')}}" method="POST">

                @foreach ($errors->all() as $message)

                    <div class="alert alert-danger alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error!</strong> {{ $message }}.
                    </div>

                @endforeach

                {{ csrf_field() }}
                <input type="hidden" name="trip_id" value="{{$trip->id}}" />
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group lable-padd">
                                <label class="col-sm-3">Asset</label>
                                <div class="col-sm-6">
                                    <Select name="asset_id" id="asset_id" class="form-control">
                                        <option value="">Select</option>
                                        @foreach($assets as $asset)
                                            <option value="{{$asset->id}}">{{$asset->name}}</option>
                                        @endforeach
                                    </Select>
                                </div>
                            </div>
                            <div class="form-group lable-padd">
                                <label class="col-sm-3">Name</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="name" id="name" />
                                </div>
                            </div>
                            <div class="form-group lable-padd">
                                <label class="col-sm-3">Date</label>
                                <div class="col-sm-6">
                                    <input class="form-control form-control-inline input-medium default-date-picker"  size="16" type="text" value="" id="date" />
                                </div>
                            </div>
                            <div class="form-group lable-padd">
                                <label class="col-sm-3">Start Time</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="start_time" id="start_time" />
                                </div>
                            </div>
                            <div class="form-group lable-padd">
                                <label class="col-sm-3">End Time</label>
                                <div class="col-sm-6">
                                    <input type="text" name="end_time" id="end_time" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group lable-padd">
                                <label class="col-sm-3">Repeat</label>
                                <div class="col-sm-6">
                                    <Select id="repeat" name="repeat" class="form-control">
                                        <option value="daily">Daily</option>
                                        <option value="weekly">Weekly</option>
                                        <option value="none">None</option>
                                    </Select>
                                </div>
                            </div>
                            <div class="form-group lable-padd">
                                <label class="col-sm-3">Description</label>
                                <div class="col-sm-6">
                                    <textarea name="description" id="description" class="form-control" placeholder=""></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <h2>Price List with Sale Agent Groups Discount</h2>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group lable-padd">
                                <label class="col-sm-12">Adult (Price)</label>
                                <div class="col-sm-12">
                                    
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="adult_price" id="adult_price" />
                                        <div class="input-group-addon">{{ $currency }}</div>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group lable-padd">
                                <label class="col-sm-12"><strong>Discount</strong></label>
                            </div>
                            @foreach ($user_groups as $group)
                            <div class="form-group lable-padd">
                                <label class="col-sm-12 user_group_title" data-id="{{$group->id}}">{{$group->name}}</label>
                                <div class="col-sm-12">

                                    <div class="input-group">
                                        <input type="text" class="form-control adult_price" name="adult_price_{{$group->id}}" id="adult_price_{{$group->id}}" required="required" />
                                        <div class="input-group-addon">{{ $currency }}</div>
                                    </div>

                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="col-md-2">
                            <div class="form-group lable-padd">
                                <label class="col-sm-12">Child (Price)</label>
                                <div class="col-sm-12">

                                    <div class="input-group">
                                        <input type="text" class="form-control" name="child_price" id="child_price"/>
                                        <div class="input-group-addon">{{ $currency }}</div>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group lable-padd">
                                <label class="col-sm-12"><strong>&nbsp;</strong></label>
                            </div>
                            @foreach ($user_groups as $group)
                            <div class="form-group lable-padd">
                                <label class="col-sm-12">&nbsp;</label>
                                <div class="col-sm-12">

                                    <div class="input-group">
                                        <input type="text" class="form-control" name="child_price_{{$group->id}}" id="child_price_{{$group->id}}" required="required">
                                        <div class="input-group-addon">{{ $currency }}</div>
                                    </div>

                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="col-md-2">
                            <div class="form-group lable-padd">
                                <label class="col-sm-12">Infant (Price)</label>
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="infant_price" id="infant_price" />
                                        <div class="input-group-addon">{{ $currency }}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group lable-padd">
                                <label class="col-sm-12"><strong>&nbsp;</strong></label>
                            </div>
                            @foreach ($user_groups as $group)
                            <div class="form-group lable-padd">
                                <label class="col-sm-12">&nbsp;</label>
                                <div class="col-sm-12">

                                    <div class="input-group">
                                        <input type="text" class="form-control" name="infant_price_{{$group->id}}" id="infant_price_{{$group->id}}" required="required" />

                                        <div class="input-group-addon">{{ $currency }}</div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <h2>Other Services</h2>
                            <div id="trip_services_list" class="form-group lable-padd">
                            </div>
                            <div class="form-group lable-padd">
                                <label class="col-sm-3">
                                    <div id="add_more_trip_services_btn" style="cursor:pointer;float:left;">
                                        <i class="fa fa-plus"></i>
                                        <span>Add More</span>
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <h2>Trip Photos</h2>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="dropzone"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-lg-12 action-button">
                            <button type="submit" class="btn-danger btn">Save</button>
                            <a class="btn-default btn" href="{{ route('view_trip') }}">Discard</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<div id="trip_services_list_append" style="display:none">
    <div class="trip_services_list">
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12">
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Name</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="name" required="required">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Price</label>
                        <div class="col-sm-6">
                            
                            <div class="input-group">
                                <input type="text" class="form-control" name="price" required="required">
                                    
                                <div class="input-group-addon">{{ $currency }}</div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group lable-padd">
                        <label class="col-sm-12"><strong>Discount</strong></label>
                    </div>
                </div>
                @foreach($user_groups as $group)
                <div class="col-md-12">
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">{{$group->name}}</label>
                        <div class="col-sm-6">
                            
                            <div class="input-group">
                                <input type="text" class="form-control service_discount_{{$group->id}}" name="discount" required="required">
                                    
                                <div class="input-group-addon">{{ $currency }}</div>
                            </div>

                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>


@endsection



@section('js')

<script>
    var existingFiles = [
        
        @foreach( $trip->tripImages()->get() as $img )            
        { name: "{{ $img->path }}", size: {{Storage::disk('local')->size('public/'.$img->path)}}, accepted: true },
        @endforeach

    ];

</script>


<script src="{{ asset('app/trip_edit.js') }}"></script>
<script>
    $(document).ready(function(){
        
        $("#asset_id").val({{ $trip->asset_id }});
        $("#name").val('{{ $trip->name }}');
        $("#date").val('{{ \Carbon\Carbon::createFromFormat('Y-m-d', $trip->date)->format('m-d-Y') }}');
        $("#start_time").val('{{ $trip->start_date }}');
        $("#end_time").val('{{ $trip->end_date }}');

        $("#repeat").val('{{ $trip->repeat }}');
        $("#description").val('{{ $trip->description }}');

        $("#adult_price").val({{ $trip->adult_price }});
        $("#child_price").val({{ $trip->child_price }});
        $("#infant_price").val({{ $trip->infant_price }});


        // group discounts
        @foreach($trip->tripDiscounts()->get() as $discount)
            $("#adult_price_{{$discount->user_group_id}}").val({{ $discount->adult }});
            $("#child_price_{{$discount->user_group_id}}").val({{ $discount->child }});
            $("#infant_price_{{$discount->user_group_id}}").val({{ $discount->infant }});
        @endforeach
        var html;
        // extra services
        @foreach($extraServices as $service)
                listCounter++;
                removeBtn = '<div class="col-md-12"><div class="form-group lable-padd"><label class="col-lg-12"><div onclick="removeTripServicesList('+listCounter+')" class="remove_trip_services_btn" style="cursor:pointer;float:left;"><i class="fa fa-trash-o"></i> <span>Remove</span></div></div></div>';

                html = '<div class="trip_services_list"> <div class="row"> <div class="col-md-6"> <div class="col-md-12"> <div class="form-group lable-padd"> <label class="col-sm-3">Name</label> <div class="col-sm-6"> <input type="text" class="form-control" name="name" required="required" value="{{ $service['name'] }}"> </div> </div> </div> <div class="col-md-12"> <div class="form-group lable-padd"> <label class="col-sm-3">Price</label> <div class="col-sm-6"> <div class="input-group"><input type="text" class="form-control" name="price" value="{{ $service['price'] }}" required="required"><div class="input-group-addon">{{ $currency }}</div> </div></div> </div> </div> <div class="col-md-12"> <div class="form-group lable-padd"> <label class="col-sm-12"><strong>Discount</strong></label> </div> </div>' +

                @foreach($service['discounts'] as $group_discounts)
                 '<div class="col-md-12"> <div class="form-group lable-padd"> <label class="col-sm-3">{{$group_discounts['user_group_name']}}</label> <div class="col-sm-6"> <div class="input-group"><input type="text" class="form-control service_discount_{{$group_discounts['user_group_id']}}" name="discount" value="{{$group_discounts['discount']}}" required="required"> <div class="input-group-addon">{{ $currency }}</div> </div></div> </div> </div>'+
                @endforeach
                
                '</div></div></div>';

                $("#trip_services_list").append("<div id='trip_services_list_append_"+listCounter+"'>"+html+removeBtn+"</div>"+sep);

        @endforeach
    });

</script>

@endsection