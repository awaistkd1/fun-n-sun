@extends('layouts.dashboard')

@section('main-content')

<!--row start-->
<div class="row">
    <!--col-md-12 start-->
    <div class="col-md-12">
        <div class="page-heading">
            <h1>Assets<small> Listing</small></h1>
        </div>
    </div>
    <!--col-md-12 end-->
</div>
<!--row end-->
@if (session('status'))
<div class="alert alert-success alert-dismissible fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Success!</strong> {{ session('status') }}
</div>
@endif

<div class="row" id="filter">
    <form id="search-form">
        <div class="form-group col-sm-5 col-xs-12">
            <input class="form-control" id="search-text" type="text" placeholder="Search" />
        </div>
        <div class="form-group col-sm-5 col-xs-12">
            <select id="state" class="form-control">
                <option value="">Select State</option>
                <option value="valid">Valid</option>
                <option value="expired">Expired</option>
            </select>
        </div>
        <div class="form-group col-sm-2 col-xs-12">
            <button type="submit" class="btn btn-block btn-primary">Search</button>
        </div>
    </form>
</div>


<!--row start-->
<div class="row">

    <div class="col-md-12" style="text-align: right;">
        <a href="{{ route('add_coupon') }}" class="btn btn-success btn-sm">Add more</a>
    </div>

    <!--col-md-12 start-->
    <div class="col-md-12">
        <table id="all-coupon-table" class="display table table-bordered table-striped nowrap" style="width:100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Code</th>
                    <th>Valid From</th>
                    <th>Valid To</th>
                    <th>Number of Use</th>
                    <th>Discount Type</th>
                    <th>Discount Amount</th>
                    @if( ($user_group->coupons_delete == 0) and ($user_group->coupons_update == 0))
                    
                    @else
                    <th>Actions</th>
                    @endif
                </tr>
            </thead>
        </table>
    </div>
    <!--col-md-12 end-->
</div>
<!--row end-->

@endsection


@section('js')
<script src="{{ asset('app/coupon.js') }}"></script>

<script>

var oTable = $('#all-coupon-table').DataTable({
    dom: "lrtp",
    processing: true,
    serverSide: true,
    responsive: {
        details: {
            display: $.fn.dataTable.Responsive.display.modal( {
                header: function ( row ) {
                    var data = row.data();
                    console.log(data);
                    return 'Details:';
                }
            } ),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                tableClass: 'table'
            } )
        }
    },
    ajax:{
        "url": '{{ route("get_coupons_ajax") }}',
        "dataType": "json",
        "type": "POST",
        data: function (d) {
            d._token = "{{ csrf_token() }}";
            d.name = $('#search-text').val();
            d.state = $('#state').val();
        }
    },
    "columns":[
        { data: "id" },
        { 
            data: "code",
            render : function(data, type, row, meta){
                if(type === 'display'){

                    data = '<a href="{{route('view_coupon_by_id')}}/?id='+row.id+'">' + data + '</a>';
                }
                return data;
            }
        },
        { data: 'valid_from'},
        { data: 'valid_to'},
        { data: "number_of_use" },
        { 
            data: "discount_type", 
            render : function(data, type, row, meta){
                if(type === 'display'){

                    if(row.discount_type == 1){
                        data = "Fixed";
                    }else{
                        data = "Percentage";
                    }

                }
                return data;
            }

        },
        { data: "discount_amount" }
        @if( ($user_group->coupons_delete == 0) and ($user_group->coupons_update == 0))

        @else
        ,{
            data: null,
            render: function(data, type, row, meta){
                if(type === 'display'){
                    data = '@if($user_group->coupons_update == 1)<form class="form-action" action="{{ route('edit_coupon') }}" method="GET">{{csrf_field()}} <input type="hidden" name="coupon_id" value="'+row.id+'" /> <button type="submit"><i class="fa fa-pencil" style="font-size:18px;margin:0 6px;"></i></button></form>@endif'+'@if($user_group->coupons_delete == 1)<form method="POST" action="{{route('delete_coupon')}}" class="form-action" action="">{{csrf_field()}} <input type="hidden" name="coupon_id" value="'+row.id+'" /> <button type="submit"><i class="fa fa-trash-o" style="font-size:18px;margin:0 6px;"></i></button></form>@endif';
                }
                return data;
            }
        }
        @endif
    ]  
});


$("#search-form").submit(function(e){
    e.preventDefault();
    oTable.draw();
});

</script>

@endsection