
@extends('layouts.dashboard')

@section('main-content')

<div class="row">
    <!--col-md-12 start-->
    <div class="col-md-12">
        <div class="page-heading">
            <h1>
                Edit Coupon
            </h1>
        </div>
    </div>
    <!--col-md-12 start-->
</div>


<div class="row">
    <div class="col-md-12">
        <div class="box-info">
            <h3>Information</h3>
            <hr>
            <form id="form-coupon" class="form-horizontal row-border" action="{{ route('update_coupon') }}" method="POST">

                @foreach ($errors->all() as $message)
                    <div class="alert alert-danger alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error!</strong> {{ $message }}.
                    </div>
                @endforeach


                {{ csrf_field() }}
                <input type="hidden" name="coupon_id" value="{{ $coupon->id }}">
                <div class="col-md-12">
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Code</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="code" id="code" value="{{ $coupon->code }}" />
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Trips</label>
                        <div class="col-sm-3">
                            <select multiple="multiple" class="multi-select" id="trips" name="trips[]">
                                @foreach ($trips as $trip)
                                    <option @if(in_array($trip->id,$valid_trips)) selected="selected"  @endif value="{{$trip->id}}">{{ $trip->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Validity Start and End Date</label>
                        <div class="col-sm-3">
                            <div class="input-group input-large" data-date="" data-date-format="mm/dd/yyyy">
                            <input type="text" class="form-control dpd1" name="valid_from" value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $coupon->valid_from)->format('m/d/Y') }}" id="valid_from" />
                            <span class="input-group-addon">To</span>
                            <input type="text" class="form-control dpd2" name="valid_to" value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $coupon->valid_to)->format('m/d/Y') }}" id="valid_to" />
                            </div>
                            <span class="help-block">Select date range</span> 
                        </div>
                    </div>
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Number of Use</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="number_of_use" value="{{ $coupon->number_of_use }}" id="number_of_use" />
                        </div>
                    </div>
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Discount Type</label>
                        <div class="col-sm-3">
                            <Select class="form-control" name="discount_type" id="discount_type">
                                <option value="1">Fixed</option>
                                <option value="2">Percentage</option>
                            </Select>
                        </div>
                    </div>

                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Discount Amount</label>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <input type="text" class="form-control" name="discount_amount" value="{{ $coupon->discount_amount }}" id="discount_amount">
                                <div class="input-group-addon" id="type-addon">{{$currency}}</div>    
                            </div>
                            
                        </div>
                    </div>

                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-lg-12 action-button">
                            <button type="submit" class="btn-danger btn">Save</button>
                            <a class="btn-default btn" href="{{ route('view_coupon') }}">Cancel</a>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>


@endsection



@section('js')
<script src="{{asset('app/coupon.js')}}"></script>
<script>
    $(document).ready(function(){
        $("#discount_type").val({{ $coupon->discount_type }});

        $("#discount_type").change(function(){
            if($("#discount_type").val() == '1'){
                $("#type-addon").text('{{$currency}}');
            }else{
                $("#type-addon").text('%');
            }
        });

    });


</script>
@endsection