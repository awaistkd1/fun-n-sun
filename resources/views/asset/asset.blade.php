@extends('layouts.dashboard')


@section('main-content')

<style>
	.gallery-wrap .img-big-wrap img {
	    height: 450px;
	    width: auto;
	    display: inline-block;
	    cursor: zoom-in;
	}


	.gallery-wrap .img-small-wrap .item-gallery {
	    width: 60px;
	    height: 60px;
	    border: 1px solid #ddd;
	    margin: 7px 2px;
	    display: inline-block;
	    overflow: hidden;
	}

	.gallery-wrap .img-small-wrap {
	    text-align: center;
	}
	.gallery-wrap .img-small-wrap img {
	    max-width: 100%;
	    max-height: 100%;
	    object-fit: cover;
	    border-radius: 4px;
	    cursor: zoom-in;
	}
</style>

    <div class="row">
        <!--col-md-12 start-->
        <div class="col-md-12">
            <div class="page-heading">
                <h1>Asset<small> Details</small></h1>
            </div>
        </div>
        <!--col-md-12 end-->
    </div>

	@if (session('status'))
	<div class="alert alert-success alert-dismissible fade in">
	    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	    <strong>Success!</strong> {{ session('status') }}
	</div>
	@endif    


    <div class="row">
        <div class="col-md-12">
            <div class="box-info">

				<div class="card">
				    <div class="row">
				        <aside class="col-sm-7">
				            <article class="card-body p-5">
				                <h3 class="title mb-3">{{ $asset->name }}</h3>
				                
				                <!-- price-detail-wrap .// -->
				                <dl class="item-property">
				                    <dt>Description</dt>
				                    <dd>
				                        <p>{{ $asset->description }}</p>
				                    </dd>
				                </dl>
				                <dl class="param param-feature">
				                    <dt>Min Capacity</dt>
				                    <dd>{{ $asset->min_capacity }}</dd>
				                </dl>
				                <!-- item-property-hor .// -->
				                <dl class="param param-feature">
				                    <dt>Max Capacity</dt>
				                    <dd>{{$asset->max_capacity}}</dd>
				                </dl>
				                <!-- item-property-hor .// -->
				                <dl class="param param-feature">
				                    <dt>Agent Booking Limit</dt>
				                    <dd>{{ $asset->per_agent_booking_limit }}</dd>
				                </dl>
				                <!-- item-property-hor .// -->
				                <dl class="param param-feature">
				                    <dt>Ref</dt>
				                    <dd>{{ $asset->asset_ref }}</dd>
				                </dl>

				            </article>
				            <!-- card-body.// -->
				        </aside>
				        <!-- col.// -->
				    </div>
				    <!-- row.// -->
				</div>
            </div>
        </div>
    </div>
@endsection


@section('js')

@endsection