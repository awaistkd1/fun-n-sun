@extends('layouts.dashboard')

@section('main-content')

<!--row start-->
<div class="row">
    <!--col-md-12 start-->
    <div class="col-md-12">
        <div class="page-heading">
            <h1>Assets<small> Listing</small></h1>
        </div>
    </div>
    <!--col-md-12 end-->
</div>
<!--row end-->
@if (session('status'))
<div class="alert alert-success alert-dismissible fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Success!</strong> {{ session('status') }}
</div>
@endif
<div class="row" id="search">
    <form id="search-form">
        <div class="form-group col-xs-9">
            <input class="form-control" id="search-text" type="text" placeholder="Search" />
        </div>
        <div class="form-group col-xs-3">
            <button type="submit" class="btn btn-block btn-primary">Search</button>
        </div>
    </form>
</div>

<!--row start-->
<div class="row">

    <div class="col-md-12" style="text-align: right;">
        <a href="{{ route('add_asset') }}" class="btn btn-success btn-sm">Add more</a>
    </div>

    <!--col-md-12 start-->
    <div class="col-md-12">
        <table id="all-assets-table" class="display table table-bordered table-striped nowrap" style="width:100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Min Capacity</th>
                    <th>Max Capacity</th>
                    <th>Max Reservations</th>
                    @if( ($user_group->assets_delete == 0) and ($user_group->assets_update == 0))
                    
                    @else
                    <th>Actions</th>
                    @endif
                </tr>
            </thead>
        </table>
    </div>
    <!--col-md-12 end-->
</div>
<!--row end-->

@endsection


@section('js')
<script src="{{ asset('app/assets.js') }}"></script>

<script>

var oTable = $('#all-assets-table').DataTable({
    dom: "lrtp",
    processing: true,
    serverSide: true,
    responsive: {
        details: {
            display: $.fn.dataTable.Responsive.display.modal( {
                header: function ( row ) {
                    var data = row.data();
                    console.log(data);
                    return 'Details:';
                }
            } ),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                tableClass: 'table'
            } )
        }
    },
    ajax:{
        "url": '{{ route("get_assets_ajax") }}',
        "dataType": "json",
        "type": "POST",
        data: function (d) {
            d._token = "{{ csrf_token() }}";
            d.name = $('#search-text').val();
        }
    },
    "columns":[
        { data: "id" },
        { 
            data: "name",
            render : function(data, type, row, meta){
                if(type === 'display'){
                    data = '<a href="{{route('view_asset_by_id')}}/?id='+row.id+'">' + data + '</a>';
                }
                return data;
            }
        },
        { data: "min_capacity" },
        { data: "max_capacity" },
        { data: "per_agent_booking_limit" }
        @if( ($user_group->assets_delete == 0) and ($user_group->assets_update == 0))

        @else
        ,{
            data: null,
            render: function(data, type, row, meta){
                if(type === 'display'){
                    data = '@if($user_group->assets_update == 1)<form class="form-action" action="{{ route('edit_asset') }}" method="GET">{{csrf_field()}} <input type="hidden" name="asset_id" value="'+row.id+'" /> <button type="submit"><i class="fa fa-pencil" style="font-size:18px;margin:0 6px;"></i></button></form>@endif'+'@if($user_group->assets_delete == 1)<form method="POST" action="{{route('delete_asset')}}" class="form-action" action="">{{csrf_field()}} <input type="hidden" name="asset_id" value="'+row.id+'" /> <button type="submit"><i class="fa fa-trash-o" style="font-size:18px;margin:0 6px;"></i></button></form>@endif';
                }
                return data;
            }
        }
        @endif
    ]  
});


$("#search-form").submit(function(e){
    e.preventDefault();
    oTable.draw();
});

</script>

@endsection