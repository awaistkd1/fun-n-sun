@extends('layouts.dashboard')

@section('main-content')

<div class="row">
    <!--col-md-12 start-->
    <div class="col-md-12">
        <div class="page-heading">
            <h1>
                Edit Asset
            </h1>
        </div>
    </div>
    <!--col-md-12 start-->
</div>


<div class="row">
    <div class="col-md-12">
        <div class="box-info">
            <h3>Information</h3>
            <hr>

            <form id="form-asset" class="form-horizontal row-border asset-form" action="{{ route('save_edit_asset') }}" method="POST">


                @foreach ($errors->all() as $message)

                    <div class="alert alert-danger alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error!</strong> {{ $message }}.
                    </div>

                @endforeach


                {{ csrf_field() }}
                
                <input type="hidden" name="asset_id" value="{{$asset->id}}">

                <div class="col-md-9">
                    <div class="form-group lable-padd">
                            <label class="col-sm-3">Name</label>
                        <div class="col-sm-6">
                            <input type="text"  name="name" id="name" class="form-control" value="" placeholder="" />
                        </div>
                    </div>
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Min Capacity</label>
                        <div class="col-sm-6">
                            <input type="text" name="min_capacity" id="min_capacity" value=""  class="form-control" />
                        </div>
                    </div>
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Max Capacity</label>
                        <div class="col-sm-6">
                            <input type="text" name="max_capacity" id="max_capacity" value="" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Max Reservations</label>
                        <div class="col-sm-6">
                            <input type="text" name="per_agent_booking_limit" id="per_agent_booking_limit" value="" class="form-control">
                        </div>
                    </div>
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Description</label>
                        <div class="col-sm-6">
                            <textarea name="description" id="description" value="" class="form-control"></textarea>
                        </div>
                    </div>

                </div>
                <div class="col-md-3">
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-lg-12 action-button">
                            <button class="btn-danger btn">Save</button>
                            <a class="btn-default btn" href="{{ route('view_asset') }}">Cancel</a>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>



@endsection



@section('js')
    <script>
        $(document).ready(function(){
            
            $('#name').val('{{ $asset->name }}');
            $('#min_capacity').val('{{ $asset->min_capacity }}');
            $('#max_capacity').val('{{ $asset->max_capacity }}');
            $('#per_agent_booking_limit').val('{{ $asset->per_agent_booking_limit }}');
            $('#description').val('{{ $asset->description }}');

        });
    </script>

    <script src="{{ asset('app/assets.js') }}"></script>
@endsection