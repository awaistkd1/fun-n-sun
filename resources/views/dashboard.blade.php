@extends('layouts.dashboard')

@section('main-content')
    <!--row start-->
    <div class="row"> 
      <!--col-md-12 start-->
      <div class="col-md-12">
        <div class="page-heading">
          <h1>Dashboard</h1>
        </div>
      </div>
      <!--col-md-12 end-->
      <div class="col-sm-6 col-md-4">
        <div class="box-info">
          <div class="title-box">
            <h3>Assets</h3>
            <div class="blue-bg">
              <p>Total Assets</p>
              <h1>{{ $total_assets }}</h1>
            </div>
            <i class="fa fa-book"></i>
            <div class="pull-right box-info-action-icons">
                @if($user_group->assets_add)
                <a href="{{ route('add_asset') }}">
                    <i class="fa fa-plus"></i>
                </a>
                @endif
                <a href="{{ route('view_asset') }}">
                    <i class="fa fa-list-ul"></i>
                </a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-4">
        <div class="box-info">
          <div class="title-box">
            <h3>Trips</h3>
            <div class="red-bg">
              <p>Total Trips</p>
              <h1>{{ $total_trips }}</h1>
            </div>
            <i class="fa fa-map-marker"></i>
            <div class="pull-right box-info-action-icons">
                @if($user_group->trips_add)
                <a href="{{ route('add_trip') }}">
                    <i class="fa fa-plus"></i>
                </a>
                @endif
                <a href="{{ route('view_trip') }}">
                    <i class="fa fa-list-ul"></i>
                </a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-4">
        <div class="box-info">
          <div class="title-box">
            <h3>Bookings</h3>
            <div class="green-bg">
              <p>Booking Reservations </p>
              <h1>{{ $total_bookings }}</h1>
            </div>
            <i class="fa fa-table"></i>
            <div class="pull-right box-info-action-icons">
                <a href="{{ route('bookings') }}">
                    <i class="fa fa-list-ul"></i>
                </a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-4">
        <div class="box-info">
          <div class="title-box">
            <h3>Customer</h3>
            <div class="maroon-bg">
              <p>Total Customers</p>
              <h1>{{ $total_customers }}</h1>
            </div>
            <i class="fa fa-user"></i>
            <div class="pull-right box-info-action-icons">
                @if($user_group->customers_add)
                <a href="{{ route('add_customer') }}">
                    <i class="fa fa-plus"></i>
                </a>
                @endif
                <a href="{{ route('view_customer') }}">
                    <i class="fa fa-list-ul"></i>
                </a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-4">
        <div class="box-info">
          <div class="title-box">
            <h3>Sales Agent</h3>
            <div class="maroon-bg" style="background:#d9532d">
              <p>Total Sales Agent</p>
              <h1>{{ $total_agents }}</h1>
            </div>
            <i class="fa fa-user"></i>
            <div class="pull-right box-info-action-icons">
              @if($user_group->sales_agents_add)
                <a href="{{ route('add_agent') }}">
                    <i class="fa fa-plus"></i>
                </a>
              @endif
                <a href="{{ route('view_agent') }}">
                    <i class="fa fa-list-ul"></i>
                </a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-4">
        <div class="box-info">
          <div class="title-box">
            <h3>Coupons</h3>
            <div class="maroon-bg" style="background:#a200ab">
              <p>Total Coupons</p>
              <h1>{{ $total_coupons }}</h1>
            </div>
            <i class="fa fa-gift"></i>
            <div class="pull-right box-info-action-icons">
              @if($user_group->coupons_add)
                <a href="{{ route('add_coupon') }}">
                    <i class="fa fa-plus"></i>
                </a>
              @endif
                <a href="{{ route('view_coupon') }}">
                    <i class="fa fa-list-ul"></i>
                </a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-4">
        <div class="box-info">
          <div class="title-box">
            <h3>Users</h3>
            <div class="green-bg" style="background:#00a200">
              <p>Registered Users</p>
              <h1>{{ $total_users }}</h1>
            </div>
            <i class="fa fa-users"></i>
            <div class="pull-right box-info-action-icons">
              @if($user_group->id == 1)
                <a href="{{ route('add_users') }}">
                    <i class="fa fa-plus"></i>
                </a>
                <a href="{{ route('view_users') }}">
                    <i class="fa fa-list-ul"></i>
                </a>
              @endif
            </div>
          </div>
        </div>
      </div>
      
    </div>
    <!--row end-->

@endsection