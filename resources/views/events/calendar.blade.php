@extends('layouts.dashboard')

@section('main-content')
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' />
<div class="row">
    <!--col-md-12 start-->
    <div class="col-md-12">
        <div class="page-heading">
            <h1>
                Calendar
            </h1>
        </div>
    </div>
    <!--col-md-12 start-->
</div>


<div class="row">
    <div class="col-md-12">
        <div class="box-info">
            <div id='calendar'></div>
        </div>
    </div>
</div>

@endsection


@section('js')
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>

    <script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js'></script>

    <script src="{{asset('app/events.js')}}"></script>

    <script>
    $(document).ready(function() {
        $('#calendar').fullCalendar({
            events : [
                @foreach($events as $event)
                {
                    title : '{{ $event->name }}',
                    start : '{{ $event->date }}',
                    url : '{{ route('edit_event').'?event_id='.$event->id }}'
                },
                @endforeach
            ]
        })
    });
    </script>


@endsection