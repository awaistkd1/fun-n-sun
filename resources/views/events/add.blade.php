@extends('layouts.dashboard')

@section('main-content')

<div class="row">
    <!--col-md-12 start-->
    <div class="col-md-12">
        <div class="page-heading">
            <h1>
                Add New Event
            </h1>
        </div>
    </div>
    <!--col-md-12 start-->
</div>


<div class="row">
    <div class="col-md-12">
        <div class="box-info">
            <h3>Information</h3>
            <hr>
            
            @foreach ($errors->all() as $message)
                <div class="alert alert-danger alert-dismissible fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Error!</strong> {{ $message }}.
                </div>
            @endforeach

            <form id="form-add-event" class="form-horizontal row-border" action="{{route('save_event')}}" method="POST">
                {{ csrf_field() }}
                <div class="col-md-6">
                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Name</label>
                        <div class="col-sm-6">
                            <input type="text" name="name" id="name" class="form-control" placeholder="">
                        </div>
                    </div>

                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Date</label>
                        <div class="col-sm-6">
                            <input type="date" name="date" id="date" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group lable-padd">
                        <label class="col-sm-3">Description</label>
                        <div class="col-sm-6">
                            <textarea class="form-control" name="description" id="description" placeholder=""></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-sm-12 action-button">
                            <button type="submit" class="btn-danger btn">Submit</button>
                            <a class="btn-default btn" href="{{ route('home') }}">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection


@section('js')
    <script src="{{asset('app/events.js')}}"></script>
@endsection