Dropzone.autoDiscover = false;

var uploaded_files = {};

var myDropzone = new Dropzone("div.dropzone",{
    url: upload_url,
    paramName: 'file',
    params: { '_token' : csrf_token },
    maxFilesize: 2, // MB
    maxFiles: 5,
    acceptedFiles: ".jpeg,.jpg,.png,.gif",
    addRemoveLinks: true,
    removedfile: function(file) {
        var name = file.name;
        
        if( typeof uploaded_files[file.name] !== 'undefined'){
            console.log(uploaded_files[file.name]['id']);
            $('#'+uploaded_files[file.name]['id']).remove();
            
            // TODO send ajax request to delete the file
            // param uploaded_files[file.name]['path']            
        }

        var _ref;
        return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
    },
    init: function() {
        this.on("success", function(file, response) {
            var parts = response.split('/');
            uploaded_files[file.name] = { 'id': parts[parts.length-1].split('.')[0], 'path' : response};
            var image_hidden_input = "<input type='hidden' class='trip_image' name='images[]' value='"+response+"' id='"+uploaded_files[file.name]['id']+"' />";
            $('form.trip-form').append(image_hidden_input);
        });
    }
});



for (i = 0; i < existingFiles.length; i++) {

    myDropzone.emit("addedfile", existingFiles[i]);
    myDropzone.emit("thumbnail", existingFiles[i], '/storage/' + existingFiles[i].name);
    myDropzone.emit("complete", existingFiles[i]);
    myDropzone.files.push(existingFiles[i]);
    var parts = existingFiles[i].name.split('/');
    uploaded_files[parts[parts.length-1]] = { 'id': parts[parts.length-1].split('.')[0] , 'path' : existingFiles[i].name };

    var image_hidden_input = "<input type='hidden' class='trip_image' name='images[]' value='"+ existingFiles[i].name +"' id='"+uploaded_files[parts[parts.length-1]]['id']+"' />";
    $('form.trip-form').append(image_hidden_input);
}



function post_data(path, parameters) {
    var form = $('<form></form>');

    form.attr("method", "post");
    form.attr("action", path);

    $.each(parameters, function(key, value) {
        if ( typeof value == 'object' || typeof value == 'array' ){
            $.each(value, function(subkey, subvalue) {
                if ( typeof subvalue == 'object' || typeof subvalue == 'array' ){
	                
	                $.each(subvalue, function(subsubkey, subsubvalue) {
	                	if ( typeof subsubvalue == 'object' || typeof subsubvalue == 'array' ){
	                		$.each(subsubvalue, function(subsubsubkey, subsubsubvalue) {
			                	if ( typeof subsubsubvalue == 'object' || typeof subsubsubvalue == 'array' ){
			                		$.each(subsubsubvalue, function(subsubsubsubkey, subsubsubsubvalue) {
					                	var field = $('<input />');
						                field.attr("type", "hidden");
						                field.attr("name", key+'['+subkey+']['+subsubkey+']['+subsubsubkey+'][]');
						                field.attr("value", subsubsubsubvalue);
						                form.append(field);
			                		});
			                	}else{
				                	var field = $('<input />');
					                field.attr("type", "hidden");
					                field.attr("name", key+'['+subkey+']['+subsubkey+'][]');
					                field.attr("value", subsubsubvalue);
					                form.append(field);			                		
			                	}
         			
	                		});
	                	}else{
		                	var field = $('<input />');
			                field.attr("type", "hidden");
			                field.attr("name", key+'['+subkey+'][]');
			                field.attr("value", subsubvalue);
			                form.append(field);	                		
	                	}

	                });

                }else{
            		var field = $('<input />');
	                field.attr("type", "hidden");
	                field.attr("name", key+'[]');
	                field.attr("value", subvalue);
	                form.append(field);                	
                }

            });
        } else {
            var field = $('<input />');
            field.attr("type", "hidden");
            field.attr("name", key);
            field.attr("value", value);
            form.append(field);
        }
    });
    $(document.body).append(form);
    form.submit();
}


$(document).ready(function(){
	$('#start_time').timepicker({ 'step': 15 });
	$('#end_time').timepicker({
	    'step': function(i) {
	        return (i%2) ? 15 : 45;
	    }
	});


	$("#form-trip").validate({
        rules: {
            name: {
                required: true
            },
            asset_id:{
            	required: true
            },
            date: {
            	required: true
            },
            start_time: {
            	required: true
            },
            end_time: {
            	required: true
            },
            repeat:{
            	required: true
            },
            adult_price: {
            	required: true
            },
            child_price:{
            	required: true
            },
            infant_price:{
            	required: true
            }
        },
        messages: {
            name: "Please enter a valid asset name",
            description: { maxlength: "Description cannot be more than 1000 letters." }
        },
        submitHandler: function(form) {




            var trip = {
            	name: $("#name").val(),
            	asset_id : $("#asset_id").val(),
            	date: $("#date").val(),
            	start_time: $("#start_time").val(),
            	end_time: $("#end_time").val(),
            	repeat: $("#repeat").val(),
            	description: $("#description").val(),
            	adult_price: $("#adult_price").val(),
            	child_price: $("#child_price").val(),
            	infant_price: $("#infant_price").val()
            };

            if($("input[name=trip_id]").length){
                trip['id'] = $("input[name=trip_id]").val();
            }
            
            var user_groups = $(".user_group_title");
            var user_group_ids = [];
            for (var i = 0; i < user_groups.length; i++) {
            	user_group_ids.push($(user_groups[i]).data('id'));
            }

            var price_discounts = [];
            for (var i = 0; i < user_group_ids.length; i++) {
            	price_discounts.push({
            		user_group_id: user_group_ids[i],
            		adult_price: $("#adult_price_"+user_group_ids[i]).val(),
            		child_price: $("#child_price_"+user_group_ids[i]).val(),
            		infant_price: $("#infant_price_"+user_group_ids[i]).val()
            	});
            }

            trip['price_discounts'] = price_discounts;


            var services = $("#trip_services_list .trip_services_list");
            var extra_services = [];
            for (var i = 0; i < services.length; i++) {
            	
            	var service_name = $(services[i]).find('input[name=name]').val();
            	var service_price = $(services[i]).find('input[name=price]').val();
				var service_discounts = [];
				
				for (var j = 0; j < user_group_ids.length; j++) {
				    var discount_price = $(services[i]).find('.service_discount_'+user_group_ids[j]).val();
				    service_discounts.push({
				    	user_group_id: user_group_ids[j],
				    	price: discount_price
				    });
				}

				extra_services.push({
					name: service_name,
					price: service_price,
					discounts: service_discounts 
				});

            }

            trip['extra_services'] = extra_services;

            trip['_token'] = csrf_token;

            // add images

            var images = $(".trip_image");
            
            var trip_images = [];

            for(var i=0; i< images.length; i++){
                trip_images.push($(images[i]).val());
            }

            trip['images'] = trip_images;


            // submit the data

            var url = $("#form-trip").attr('action');
            post_data(url,trip);
        }
    });  
});