$(document).ready(function(){
    $("#form-add-event").validate({
        rules: {
            name: {
                required: true
            },
            description: {
                maxlength: 1000
            },
            date:{
                required: true
            }
        },
        messages: {
            name: "Please enter a valid name",
            description: { maxlength: "Description cannot be more than 1000 letters." },
            date: "Please select a date"
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
});