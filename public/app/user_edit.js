Dropzone.autoDiscover = false;

var uploaded_files = {};


var myDropzone = new Dropzone("div.dropzone",{
    url: upload_url,
    paramName: 'file',
    params: { '_token' : csrf_token },
    maxFilesize: 2, // MB
    maxFiles: 1,
    acceptedFiles: ".jpeg,.jpg,.png,.gif",
    addRemoveLinks: true,
    removedfile: function(file) {
        var name = file.name;
        
        if(name.indexOf('/') != -1){
            var name_parts = name.split('/');
            file.name = name_parts[name_parts.length-1];
        }

        if( typeof uploaded_files[file.name] !== 'undefined'){

            $('#'+uploaded_files[file.name]['id']).remove();
            
            // TODO send ajax request to delete the file
            // param uploaded_files[file.name]['path']
        }

        var _ref;
        return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
    },
    init: function() {
        this.on("success", function(file, response) {
            var parts = response.split('/');
            uploaded_files[file.name] = { 'id': parts[parts.length-1].split('.')[0], 'path' : response};
            var image_hidden_input = "<input type='hidden' name='images[]' value='"+response+"' id='"+uploaded_files[file.name]['id']+"' />";
            console.log(image_hidden_input);
            $('form.user-form').append(image_hidden_input);
        });
    }
});


for (i = 0; i < existingFiles.length; i++) {

    myDropzone.emit("addedfile", existingFiles[i]);
    myDropzone.emit("thumbnail", existingFiles[i], '/storage/' + existingFiles[i].name);
    myDropzone.emit("complete", existingFiles[i]);
    myDropzone.files.push(existingFiles[i]);
    var parts = existingFiles[i].name.split('/');
    uploaded_files[parts[parts.length-1]] = { 'id': parts[parts.length-1].split('.')[0] , 'path' : existingFiles[i].name };

    var image_hidden_input = "<input type='hidden' name='images[]' value='"+ existingFiles[i].name +"' id='"+uploaded_files[parts[parts.length-1]]['id']+"' />";
    $('form.user-form').append(image_hidden_input);
}
