$(document).ready(function(){
    $("#form-update-password").validate({
        rules: {
            current:{
                required: true,
                minlength: 5
            },
            new: {
                required: true,
                minlength: 5
            },
            confirm:{
                required: true,
                minlength: 5,
                equalTo: "#new"
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
});
