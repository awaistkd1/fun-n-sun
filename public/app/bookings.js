$(document).ready(function(){
	
        $("#form-make-booking").validate({
        rules: {
            first_name: {
                required: true,
            },
            last_name: {
                required: true
            },
            email:{
                email: true
            },
            description: {
                maxlength: 1000
            },
            mobile:{
                required: true,
                maxlength: 15,
                number: true
            },
            referral_email: {
                email: true
            },
            commissioned_sales_agent: {
                required: true
            },
            adult_qty: {
                required: true
            },
            child_qty: {
                required: true
            },
            infant_qty: {
                required: true
            },
            payment_type: {
                required: true
            },
            payment_notes:{
                required: true,
                maxlength: 100
            }
        },
        messages: {
            email:{
                email: "Please provide a valid email address"
            },
            mobile: {
                maxlength: 'Please enter a valid mobile number',
                number: 'Please enter a valid mobile number'
            },
            description: { maxlength: "Description cannot be more than 1000 letters." },
            referral_email: { email: "Please provide a valid email address" }
        },
        invalidHandler: function(form, validator) {
            $(".customer-fields").show();
        },
        submitHandler: function(form) {
            form.submit();
        }
    });


});