$(document).ready(function(){
    $("#form-user-group").validate({
        rules: {
            name: {
                required: true,

            },
            description: {
                maxlength: 1000
            }
        },
        messages: {
            name: "Please enter a valid asset name",
            description: { maxlength: "Description cannot be more than 1000 letters." }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
});