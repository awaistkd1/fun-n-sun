
$(document).ready(function(){
    $("#form-asset").validate({
        rules: {
            name: {
                required: true,

            },
            min_capacity: {
                required: true,
                number: true
            },
            max_capacity: {
                required: true,
                number: true
            },
            per_agent_booking_limit:{
                required: true,
                number: true
            },
            description:{
                maxlength: 1000
            }
        },
        messages: {
            name: "Please enter a valid asset name",
            min_capacity: {
                required: "This field is required.",
                number: "Min capacity must be a number."
            },
            max_capacity: {
                required: "This field is required.",
                number: "Max capacity must be a number."
            },
            per_agent_booking_limit: {
                required: "This field is required.",
                number: "Max reservations must be a number."
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
});
