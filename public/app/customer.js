$(document).ready(function(){

    $("#form-customer").validate({
        rules: {
            first_name: {
                required: true,
            },
            last_name: {
                required: true
            },
            email:{
                required: true,
                email: true
            },
            description: {
                maxlength: 1000
            },
            mobile:{
                maxlength: 15,
                number: true
            },
            referral_email:{
                email: true
            }
        },
        messages: {
            name: "Please enter a valid asset name",
            description: { maxlength: "Description cannot be more than 1000 letters." }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

});