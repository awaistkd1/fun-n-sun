Dropzone.autoDiscover = false;

var uploaded_files = {};

var myDropzone = new Dropzone("div.dropzone",{
    url: upload_url,
    paramName: 'file',
    params: { '_token' : csrf_token },
    maxFilesize: 2, // MB
    maxFiles: 1,
    acceptedFiles: ".jpeg,.jpg,.png,.gif",
    addRemoveLinks: true,
    removedfile: function(file) {
        var name = file.name;
        
        if( typeof uploaded_files[file.name] !== 'undefined'){
            console.log(uploaded_files[file.name]['id']);
            $('#'+uploaded_files[file.name]['id']).remove();
            
            // TODO send ajax request to delete the file
            // param uploaded_files[file.name]['path']            
        }

        var _ref;
        return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
    },
    init: function() {
        this.on("success", function(file, response) {
            var parts = response.split('/');
            uploaded_files[file.name] = { 'id': parts[parts.length-1].split('.')[0], 'path' : response};
            var image_hidden_input = "<input type='hidden' name='images[]' value='"+response+"' id='"+uploaded_files[file.name]['id']+"' />";
            console.log(image_hidden_input);
            $('#form-add-user').append(image_hidden_input);
        });
    }
});




$(document).ready(function(){
    $("#form-add-user").validate({
        rules: {
            user_group_id:{
                required: true
            },
            name: {
                required: true,
            },
            email:{
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 5
            },
            description: {
                maxlength: 1000
            },
            mobile:{
                maxlength: 15,
                number: true
            }
        },
        messages: {
            user_group_id: "This field is required",
            name: "Please enter a valid name",
            email:{
                required: "This field is required",
                email: "Please provide a valid email address"
            },
            password:{
                required: "This field is required",
                minlength: "Password must have at least 5 characters"
            },
            mobile: {
                maxlength: 'Please enter a valid mobile number',
                number: 'Please enter a valid mobile number'
            },
            description: { maxlength: "Description cannot be more than 1000 letters." }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
});
