$(document).ready(function(){

    $("#form-coupon").validate({
        rules: {
        	code: {
        		required: true
        	},
        	trips:{
        		required: true
        	},
        	valid_from:{
        		required: true
        	},
        	valid_to:{
        		required: true
        	},
        	number_of_use:{
        		required: true
        	},
        	discount_amount:{
        		required: true
        	},
        	discount_type:{
        		required: true
        	}
        },
        messages: {

        },
        submitHandler: function(form) {

        	if($("#trips").val() == null){
        		alert("Please at least one trip");
        		return;
        	}
        	
            form.submit();
        }
    });

});