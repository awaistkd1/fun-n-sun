<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('home');


// Assets routes

// asset_view
Route::get('/assets/view', 'AssetController@show')->name('view_asset');
Route::get('/assets/view/id', 'AssetController@viewById')->name('view_asset_by_id');
Route::post('/assets/ajax/get','AssetController@getAssetData')->name('get_assets_ajax');


// asset_add
Route::get('/assets/add', 'AssetController@form')->name('add_asset');
Route::post('/assets/save', 'AssetController@save')->name('save_asset');


// asset update
Route::get('/assets/edit','AssetController@editAsset')->name('edit_asset');
Route::post('/assets/edit/save','AssetController@saveEditAsset')->name('save_edit_asset');

// asset delete
Route::post('/assets/delete','AssetController@deleteAsset')->name('delete_asset');

//======================================================================


// Coupon routes

//	coupon get
Route::get('/coupon/view','CouponController@show')->name('view_coupon');
Route::post('/coupon/ajax/get', 'CouponController@getCouponData')->name('get_coupons_ajax');
Route::get('/coupon/view/id','CouponController@viewById')->name('view_coupon_by_id');

// coupon add
Route::get('/coupon/add','CouponController@form')->name('add_coupon');
Route::post('/coupon/save','CouponController@save')->name('save_coupon');


// coupon update
Route::get('/coupon/edit','CouponController@showEditForm')->name('edit_coupon');
Route::post('/coupon/update','CouponController@saveEditForm')->name('update_coupon');

// delete coupon
Route::post('/coupon/delete', 'CouponController@deleteCoupon')->name('delete_coupon');
//=========================================================================


// Customer routes

// customer view
Route::get('/customer/view','CustomerController@show')->name('view_customer');
Route::post('/customer/ajax/get','CustomerController@getCustomerData')->name('get_customers_ajax');
Route::get('/customer/view/id','CustomerController@viewById')->name('view_customer_by_id');
Route::get('/customer/search/phone', 'CustomerController@searchByPhone')->name('search_customer_by_phone');
// customer add
Route::get('/customer/add','CustomerController@form')->name('add_customer');
Route::post('/customer/save','CustomerController@save')->name('save_customer');

// customer update
Route::get('/customer/edit', 'CustomerController@editCustomer')->name('edit_customer');
Route::post('/customer/edit/save','CustomerController@saveEditCustomer')->name('save_edit_customer');

// customer delete
Route::post('/customer/delete', 'CustomerController@deleteCustomer')->name('delete_customer');

//=======================================================================



// Sales Agent route categories

// sales agent view
Route::get('/sales-agent/view','SalesAgentController@show')->name('view_agent');
Route::post('/sales-agent/ajax/get','SalesAgentController@getSalesAgentData')->name('get_agents_ajax');
Route::get('/sales-agent/view/id','SalesAgentController@viewById')->name('view_agent_by_id');


// sales agent add
Route::get('/sales-agent/add','SalesAgentController@form')->name('add_agent');


// sales agent update
Route::get('/sales-agent/edit','SalesAgentController@showEditForm')->name('edit_agent');
Route::post('/sales-agent/update','SalesAgentController@saveEditAgent')->name('update_agent');

// delete sales agent
Route::post('/sales-agent/delete','SalesAgentController@deleteAgent')->name('delete_agent');

//=======================================================================

// Trip routes

// trip view
Route::get('/trip/view','TripController@show')->name('view_trip');
Route::post('/trips/ajax/get','TripController@getTripData')->name('get_trips_ajax');
Route::get('/trip/view/id','TripController@viewById')->name('view_trip_by_id');

// trip add
Route::get('/trip/add','TripController@form')->name('add_trip');
Route::post('/trip/save','TripController@save')->name('save_trip');

// trip update
Route::post('/trip/change-status/','TripController@changeTripStatus')->name('change_status');
Route::get('/trip/edit','TripController@showEditForm')->name('edit_trip');
Route::post('/trip/update','TripController@saveEditTrip')->name('update_trip');

// trip delete
Route::post('/trip/delete','TripController@deleteTrip')->name('delete_trip');

//============================================================================

// Group Right routes

// group right view
Route::get('/user-group/view','UserGroupController@show')->name('view_user_group');
Route::post('/user-group/ajax/get','UserGroupController@getUserGroupData')->name('get_user_groups_ajax');
Route::get('/user-group/view/id','UserGroupController@viewById')->name('view_user_group_by_id');

// group right add
Route::get('/user-group/add','UserGroupController@form')->name('add_user_group');
Route::post('/user-group/save','UserGroupController@save')->name('save_user_group');


// group right update
Route::get('/user-group/edit','UserGroupController@showEditForm')->name('edit_user_group');
Route::post('/user-group/update','UserGroupController@saveEditForm')->name('update_user_group');

// group right delete
Route::post('/user-group/delete','UserGroupController@deleteUserGroup')->name('delete_user_group');

// Users routes

// users view
Route::get('/users/view','UsersController@show')->name('view_users');
Route::post('/users/ajax/get','UsersController@getUsersData')->name('get_users_ajax');
Route::get('/users/view/id','UsersController@viewById')->name('view_user_by_id');

// users add
Route::get('/users/add','UsersController@form')->name('add_users');
Route::post('/users/save','UsersController@save')->name('save_users');


// users update
Route::get('/users/edit','UsersController@showEditForm')->name('edit_user');
Route::post('/users/update','UsersController@saveEditUser')->name('update_user');

// users delete
Route::post('/users/delete','UsersController@deleteUser')->name('delete_user');

//================================================
// profile routes
Route::get('/profile','UsersController@getProfile')->name('view_profile');
Route::post('/profile/update','UsersController@updateProfile')->name('update_profile');
Route::get('/profile/password','UsersController@updatePasswordForm')->name('change_password');
Route::post('/profile/password/update','UsersController@updatePassword')->name('update_password');


// Bookings routes

// booking view
Route::get('/bookings', 'BookingController@show')->name('bookings');
Route::get('/bookings/view/all/{id}', 'BookingController@viewAllBookingsOfTrip')->name('view_all_bookings_of_trip');
Route::post('/bookings/available/ajax/get','BookingController@getAvailableTripData')->name('get_available_bookings_ajax');

Route::get('/booking/view/id','BookingController@viewBookingById')->name('view_booking_by_id');

Route::post('/bookings/ajax/get/trips', 'BookingController@getAllBookingsAjax')->name('get_bookings_table');
Route::post('/booking/ajax/delete', 'BookingController@deleteBookingById')->name('delete_booking');
// edit booking

Route::post('/bookings/payment/add','BookingController@collectAmount')->name('collect_amount');
Route::get('/bookings/edit/form','BookingController@showEditForm')->name('update_booking');
Route::post('/booking/edit/save','BookingController@saveEditBooking')->name('save_edit_booking');

// booking addd
Route::get('/bookings/make/{id}', 'BookingController@displayBookingForm')->name('make_booking');
Route::post('/bookings/save','BookingController@saveBooking')->name('save_booking');



// File Upload Routes
Route::post('/upload' , 'UploadController@upload');


// events calender route

// add event

Route::get('/calender/add','EventController@showAddEventForm')->name('add_event');
Route::post('/calender/save','EventController@saveEvent')->name('save_event');

// view calender
Route::get('/calender/list','EventController@listEvents')->name('view_events');
Route::get('/calender','EventController@showCalender')->name('view_event_calander');
Route::post('/calender/ajax/get', 'EventController@getEventDate')->name('get_events_ajax');

// update event
Route::get('/calender/edit','EventController@showEditForm')->name('edit_event');
Route::post('/calender/edit/save','EventController@updateEvent')->name('update_event');

// delete event
Route::post('/calender/delete','EventController@deleteEvent')->name('delete_event');



// Reports routes
Route::get('/reports', 'ReportController@viewAll')->name('view_reports_page');

Route::get('/reports/reservation','ReportController@displayDailyTripReservationReport')->name('reservation_report');

Route::get('/reports/agent','ReportController@agentReport')->name('agent_report');

Route::get('/reports/assets','ReportController@assetReport')->name('assets_report');

?>