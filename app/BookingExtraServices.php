<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $booking_id
 * @property int $extra_service_id
 * @property Booking $booking
 * @property ExtraService $extraService
 */
class BookingExtraServices extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'BookingExtraServices';
    /**
     * @var array
     */
    protected $fillable = ['booking_id', 'extra_service_id','qty'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function booking()
    {
        return $this->belongsTo('App\Booking', 'booking_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function extraService()
    {
        return $this->belongsTo('App\ExtraService', 'extra_service_id');
    }
}
