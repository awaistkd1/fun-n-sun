<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $customer_id
 * @property int $created_user_id
 * @property int $sales_agent_id
 * @property int $coupon_id
 * @property int $ticket_qty
 * @property string $request_info
 * @property boolean $sales_agents_commission
 * @property int $adult_qty
 * @property int $child_qty
 * @property int $infant_qty
 * @property int $status
 * @property string $booking_ref
 * @property Coupon $coupon
 * @property Customer $customer
 * @property User $user
 * @property User $user
 * @property ExtraService[] $extraServices
 */
class Booking extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'Booking';
    /**
     * @var array
     */
    protected $fillable = ['customer_id', 'created_user_id', 'sales_agent_id', 'coupon_id', 'ticket_qty', 'request_info', 'sales_agents_commission', 'adult_qty', 'child_qty', 'infant_qty', 'status', 'booking_ref','payment_type', 'payment_notes','trip_id','amount_paid'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function coupon()
    {
        return $this->belongsTo('App\Coupon', 'coupon_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo('App\Customer', 'customer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'created_user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sales_agent()
    {
        return $this->belongsTo('App\User', 'sales_agent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function trip()
    {
        return $this->belongsTo('App\Trip', 'trip_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function extraServices()
    {
        return $this->belongsToMany('App\ExtraService', 'BookingExtraServices', 'booking_id', 'extra_service_id');
    }
}
