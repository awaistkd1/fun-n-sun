<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $code
 * @property string $valid_from
 * @property string $valid_to
 * @property int $number_of_use
 * @property int $discount_type
 * @property int $discount_amount
 * @property Booking[] $bookings
 * @property UserGroup[] $userGroups
 */
class Coupon extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'Coupon';
    /**
     * @var array
     */
    protected $fillable = ['code', 'valid_from', 'valid_to', 'number_of_use', 'discount_type', 'discount_amount','created_by'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookings()
    {
        return $this->hasMany('App\Booking', 'coupon_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function userGroups()
    {
        return $this->belongsToMany('App\UserGroup', 'CouponValidGroups', 'coupon_id', 'user_group_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('App\User', 'created_by');
    }

}
