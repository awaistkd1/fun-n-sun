<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $phone
 * @property string $email
 * @property string $dob
 * @property string $referral_name
 * @property string $referral_email
 * @property Booking[] $bookings
 */
class Customer extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'Customer';
    /**
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'middle_name', 'phone', 'email', 'dob', 'referral_name', 'referral_email','created_by','description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookings()
    {
        return $this->hasMany('App\Booking', 'customer_id');
    }
}
