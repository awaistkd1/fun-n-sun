<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $coupon_id
 * @property int $trip_id
 */
class CouponValidTrips extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'CouponValidTrips';
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'coupon_id';

    /**
     * @var array
     */
    protected $fillable = ['trip_id','coupon_id'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function coupon()
    {
        return $this->belongsTo('App\Coupon', 'coupon_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function trip()
    {
        return $this->belongsTo('App\Trip', 'trip_id');
    }
    
}
