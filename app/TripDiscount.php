<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $trip_id
 * @property int $user_group_id
 * @property float $adult
 * @property float $child
 * @property float $infant
 * @property Trip $trip
 * @property UserGroup $userGroup
 */
class TripDiscount extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'TripDiscount';
    /**
     * @var array
     */
    protected $fillable = ['trip_id', 'user_group_id', 'adult', 'child', 'infant'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function trip()
    {
        return $this->belongsTo('App\Trip', 'trip_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userGroup()
    {
        return $this->belongsTo('App\UserGroup', 'user_group_id');
    }
}
