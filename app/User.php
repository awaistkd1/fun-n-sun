<?php

namespace App;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Notifications\Notifiable;
/**
 * @property int $id
 * @property int $user_group_id
 * @property string $names
 * @property string $email
 * @property string $password
 * @property string $create_time
 * @property string $picture
 * @property string $mobile
 * @property string $remember_token
 * @property string $created_at
 * @property string $updated_at
 * @property string $description
 * @property UserGroup $userGroup
 * @property Asset[] $assets
 * @property Booking[] $bookings
 * @property Booking[] $bookings
 */
class User extends \Eloquent implements Authenticatable,CanResetPasswordContract
{
    use Notifiable;
    use CanResetPassword;
    use AuthenticableTrait;
     
    /**
     * @var array
     */
    protected $fillable = ['user_group_id', 'name', 'email', 'password', 'create_time', 'picture', 'mobile', 'remember_token', 'created_at', 'updated_at','description','created_by'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userGroup()
    {
        return $this->belongsTo('App\UserGroup', 'user_group_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function assets()
    {
        return $this->hasMany('App\Asset', 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookings()
    {
        return $this->hasMany('App\Booking', 'created_user_id');
    }
}
