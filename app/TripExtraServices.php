<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $trip_id
 * @property int $extra_service_id
 * @property ExtraService $extraService
 * @property Trip $trip
 */
class TripExtraServices extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'TripExtraServices';
    /**
     * @var array
     */
    protected $fillable = ['trip_id', 'extra_service_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function extraService()
    {
        return $this->belongsTo('App\ExtraService', 'extra_service_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function trip()
    {
        return $this->belongsTo('App\Trip', 'trip_id');
    }
}
