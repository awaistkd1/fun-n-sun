<?php

namespace App\Helper;

use Carbon\Carbon;

use App\Booking;
use App\BookingExtraServices;
use App\Coupon;
use App\TripExtraServices;
use App\Trip;
use App\User;
use App\TripDiscount;
use App\ExtraServiceDiscount;

use Config;

class TripHelper
{

	public function __construct(){

	}


    public static function getTotalBookingRevenue($booking){
        $trip = $booking->trip()->first();
        $trip_extra_services = $booking->extraServices()->get();
        // total price
        $ticket_cost = ($booking->adult_qty * $trip->adult_price) + 
                ($booking->child_qty * $trip->child_price) + 
                ($booking->infant_qty* $trip->infant_price);

        $extra_service_cost = 0;

        foreach ($trip_extra_services as $service) {
            $p = $service->price;
            
            $qty =  BookingExtraServices::where('booking_id','=',$booking->id)->orWhere('extra_service_id','=',$service->id)->first()->qty;

            $t = $p*$qty;
            $extra_service_cost += $t;
        }

        $total_cost = $ticket_cost + $extra_service_cost;

        // check if coupon is available or not
        if($booking->coupon_id != NULL){
            $coupon = $booking->coupon()->first();
            $total_cost -= $coupon->discount_amount;
        }

        return $total_cost;
    }


    public static function calculateTripRevenue($trip){
        // get all trip bookings

        $bookings = Booking::where('trip_id','=',$trip->id)->get();

        $total_revenue  = 0;
        $total_paid = 0;

        foreach ($bookings as $booking) {
            
            $sub_total = TripHelper::getTotalBookingRevenue($booking);
            $paid_amount = $booking->amount_paid;
            
            $total_revenue += $sub_total;
            $total_paid += $paid_amount;

        }

        return ['total' => $total_revenue, 'paid' => $total_paid, 'unpaid' => $total_revenue - $total_paid];
    }


    public static function totalTicketsBooked($trip){

    }

    public static function calculateTotalAgentCommission($booking,$user_group_id ){
        

        // check if discount available for this group

        $total_discount = 0;

        if(TripDiscount::where('trip_id','=',$booking->trip_id)->where('user_group_id','=',$user_group_id)->exists()){

            $trip_discount = TripDiscount::where('trip_id','=',$booking->trip_id)->where('user_group_id','=',$user_group_id)->first();


            $total_discount += ($trip_discount->adult * $booking->adult_qty) + ($trip_discount->child * $booking->child_qty) + ($trip_discount->infant * $booking->infant_qty);

        }

        // check if the booking have any extra services

        if(BookingExtraServices::where('booking_id','=',$booking->id)->exists()){

            $booking_extra_servies = BookingExtraServices::where('booking_id','=',$booking->id)->get();


            foreach ($booking_extra_servies as $es) {

                // check if discount exists for that service and user group
                if(ExtraServiceDiscount::where('extra_service_id','=',$es->extra_service_id)->where('user_group_id','=',$user_group_id)->exists()){
                    
                    $extra_service_discount = ExtraServiceDiscount::where('extra_service_id','=',$es->extra_service_id)->where('user_group_id','=',$user_group_id)->first();

                    $total_discount += $extra_service_discount->discount*$es->qty;

                }

            }

        }

        return $total_discount;
    }

}
?>