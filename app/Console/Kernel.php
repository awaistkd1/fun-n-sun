<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Event;
use Carbon\Carbon;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            

            $events = Event::all();

            foreach ($events as $event) {
                $event_date = Carbon::createFromFormat('Y-m-d',$event->date);

                $today = Carbon::today();
                $days = $today->diffInDays($event_date);

                if($days == 0){

                    $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
                    $beautymail->send('email.event', [ 'description' => $event->description,'title'=>$event->name ], function($message) use($event)
                    {   
                        $user = $event->user()->first();
                        $message
                            ->from('test@fun-n-sun.com')
                            ->to($user->email, $user->name)
                            ->subject('Alert!');
                    });
                }

            }


        })->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
