<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $created_by
 * @property string $name
 * @property int $min_capacity
 * @property int $max_capacity
 * @property int $per_agent_booking_limit
 * @property string $description
 * @property string $asset_ref
 * @property User $user
 * @property AssetImage[] $assetImages
 * @property Trip[] $trips
 */
class Asset extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'Asset';
    /**
     * @var array
     */
    protected $fillable = ['created_by', 'name', 'min_capacity', 'max_capacity', 'per_agent_booking_limit', 'description', 'asset_ref'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function assetImages()
    {
        return $this->hasMany('App\AssetImage', 'asset_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function trips()
    {
        return $this->hasMany('App\Trip', 'asset_id');
    }
}
