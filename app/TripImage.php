<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $trip_id
 * @property string $path
 * @property Trip $trip
 */
class TripImage extends Model
{

    
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'TripImage';
    /**
     * @var array
     */
    protected $fillable = [ 'trip_id' , 'path' ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function trip()
    {
        return $this->belongsTo('App\Trip', 'trip_id');
    }
}
