<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;


use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;

use \Maatwebsite\Excel\Writer;
use \Maatwebsite\Excel\Sheet;

Sheet::macro('cellWidth',function(Sheet $sheet, array $cellRange, array $style){

    foreach ($cellRange as $cell) {
        $sheet->getDelegate()->getColumnDimension($cell)->setAutoSize(true);
    }

});


class AgentExport implements FromView,WithEvents
{

	public function __construct($reservations,$columns,$start_date,$end_date,$agent){
		$this->reservations = $reservations;
		$this->columns = $columns;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        $this->agent = $agent;
	}

    public function view(): View
    {
        return view('exports.agents', [
            'reservations' => $this->reservations,
            'columns' => $this->columns,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'agent' => $this->agent
        ]);
    }


    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function(BeforeExport $event) {
            
            },
            AfterSheet::class    => function(AfterSheet $event) {

                $event->sheet->cellWidth(range('A','Z'),[]);
            
            },
        ];
    }

}
?>