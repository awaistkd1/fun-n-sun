<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\WithTitle;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Trip;
use App\Booking;

use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;

use \Maatwebsite\Excel\Writer;
use \Maatwebsite\Excel\Sheet;


Writer::macro('setCreator', function (Writer $writer, string $creator) {
    $writer->getDelegate()->getProperties()->setCreator($creator);
});


Sheet::macro('setOrientation', function (Sheet $sheet, $orientation) {
    $sheet->getDelegate()->getPageSetup()->setOrientation($orientation);
});


Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
    $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
});

Sheet::macro('cellWidth',function(Sheet $sheet, array $cellRange, array $style){

    foreach ($cellRange as $cell) {
        $sheet->getDelegate()->getColumnDimension($cell)->setAutoSize(true);
    }

});

class TripPerAssetSheet implements FromView, WithTitle,WithEvents
{
    private $from_date;
    private $to_date;
    private $asset_id;
    private $_title;
    private $total_records;

    public function __construct($from_date, $to_date,$asset_id,$title)
    {
        $this->from_date = $from_date;
        $this->to_date  = $to_date;
        $this->asset_id = $asset_id;
        $this->_title = $title;
    }

    public function view(): View
    {
        $columns = ['Trip No.', 'Date', 'Start Time', 'End Time', 'Total Adult','Total Child','Total Infant','Total Pax'];

        $trips = Trip::whereBetween('date', [$this->from_date, $this->to_date])->where('asset_id','=',$this->asset_id)->get();

        $all_data = [];
        $total_pax_all_trip = 0;
        foreach ($trips as $trip) {
            
            $total_adult = Booking::where('trip_id','=',$trip->id)->sum('adult_qty');
            $total_child = Booking::where('trip_id','=',$trip->id)->sum('child_qty');
            $total_infant = Booking::where('trip_id','=',$trip->id)->sum('infant_qty');


            $total_pax = $total_adult + $total_child + $total_infant;

            $data = [
                'trip_id' => $trip->id,
                'date' => $trip->date,
                'start_time' => $trip->start_date,
                'end_time' => $trip->end_date,
                'total_adult' => $total_adult,
                'total_child' => $total_child,
                'total_infant' => $total_infant,
                'total_pax' => $total_pax
            ];
            $total_pax_all_trip +=$total_pax;
            array_push($all_data, $data);
        }

        $this->total_records = count($all_data) + 4;

        return view('exports.trip_per_assets', [
            'trips' => $all_data,
            'columns' => $columns,
            'from_date'=>$this->from_date,
            'to_date'=>$this->to_date,
            'total_pax_all_trip' => $total_pax_all_trip
        ]);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->_title;
    }


    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function(BeforeExport $event) {
                $event->writer->setCreator('Fun-N-Sun');
            },
            AfterSheet::class    => function(AfterSheet $event) {
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);

                $event->sheet->styleCells(
                    'A1:B2',
                    [
                        'borders' => [
                            'allBorders'=>[
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,    
                            ]
                        ]
                    ]
                );

                $event->sheet->styleCells(
                    "A4:H".$this->total_records,
                    [
                        'borders' => [
                            'allBorders'=>[
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,    
                            ]
                        ]
                    ]
                );

                $event->sheet->cellWidth(range('A','Z'),[]);
            },
        ];
    }


}