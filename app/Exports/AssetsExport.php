<?php
namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\TripPerAssetSheet;
use App\Asset;

class AssetsExport implements WithMultipleSheets
{
    use Exportable;

    protected $year;

    public function __construct($start_date, $end_date)
    {
        $this->start_date = $start_date;
        $this->end_date = $end_date;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $assets = Asset::all();

        foreach ($assets as $asset) {
            $sheets[] = new TripPerAssetSheet($this->start_date, $this->end_date, $asset->id, $asset->name);
        }

        return $sheets;
    }
}