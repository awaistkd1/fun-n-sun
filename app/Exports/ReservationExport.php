<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ReservationExport implements FromView
{

	public function __construct($reservations,$columns,$date){
		$this->reservations = $reservations;
		$this->columns = $columns;
        $this->date = $date;
	}

    public function view(): View
    {
        return view('exports.reservations', [
            'reservations' => $this->reservations,
            'columns' => $this->columns,
            'date' => $this->date
        ]);
    }
}
?>