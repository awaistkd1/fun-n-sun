<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $asset_id
 * @property string $name
 * @property string $date
 * @property string $start_date   TODO: change the name to start_time
 * @property string $end_date     TODO: change the name to end_time
 * @property string $repeat
 * @property string $description
 * @property float $adult_price
 * @property float $child_price
 * @property float $infant_price
 * @property string $trip_ref
 * @property int    $created_by
 * @property Asset $asset
 * @property TripDiscount[] $tripDiscounts
 * @property ExtraService[] $extraServices
 */
class Trip extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'Trip';
    /**
     * @var array
     */
    protected $fillable = ['asset_id', 'name', 'date', 'start_date', 'end_date', 'repeat', 'description', 'adult_price', 'child_price', 'infant_price', 'trip_ref','created_by','status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function asset()
    {
        return $this->belongsTo('App\Asset', 'asset_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tripDiscounts()
    {
        return $this->hasMany('App\TripDiscount', 'trip_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function extraServices()
    {
        return $this->belongsToMany('App\ExtraService', 'TripExtraServices', 'trip_id', 'extra_service_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tripImages()
    {
        return $this->hasMany('App\TripImage', 'trip_id');
    }

}
