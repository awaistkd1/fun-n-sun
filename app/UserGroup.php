<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $description
 * @property boolean $assets_add
 * @property boolean $assets_update
 * @property boolean $assets_delete
 * @property boolean $trips_add
 * @property boolean $trips_update
 * @property boolean $trips_delete
 * @property boolean $bookings_add
 * @property boolean $bookings_update
 * @property boolean $bookings_delete
 * @property boolean $customers_add
 * @property boolean $customers_update
 * @property boolean $customers_delete
 * @property boolean $sales_agents_add
 * @property boolean $sales_agents_update
 * @property boolean $sales_agents_delete
 * @property boolean $coupons_add
 * @property boolean $coupons_update
 * @property boolean $coupons_delete
 * @property Coupon[] $coupons
 * @property ExtraServiceDiscount[] $extraServiceDiscounts
 * @property TripDiscount[] $tripDiscounts
 * @property User[] $users
 */
class UserGroup extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'UserGroup';
    /**
     * @var array
     */
    protected $fillable = ['name', 'description', 'assets_add', 'assets_update', 'assets_delete', 'trips_add', 'trips_update', 'trips_delete', 'bookings_add', 'bookings_update', 'bookings_delete', 'customers_add', 'customers_update', 'customers_delete', 'sales_agents_add', 'sales_agents_update', 'sales_agents_delete', 'coupons_add', 'coupons_update', 'coupons_delete'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function coupons()
    {
        return $this->belongsToMany('App\Coupon', 'CouponValidGroups', 'user_group_id', 'coupon_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function extraServiceDiscounts()
    {
        return $this->hasMany('App\ExtraServiceDiscount', 'user_group_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tripDiscounts()
    {
        return $this->hasMany('App\TripDiscount', 'user_group_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('App\User', 'user_group_id');
    }
}
