<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $extra_service_id
 * @property int $user_group_id
 * @property float $discount
 * @property ExtraService $extraService
 * @property UserGroup $userGroup
 */
class ExtraServiceDiscount extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'ExtraServiceDiscount';
    
    /**
     * @var array
     */
    protected $fillable = ['extra_service_id', 'user_group_id', 'discount'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function extraService()
    {
        return $this->belongsTo('App\ExtraService', 'extra_service_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userGroup()
    {
        return $this->belongsTo('App\UserGroup', 'user_group_id');
    }
}
