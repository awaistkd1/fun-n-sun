<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Event;
use DataTables;
use Auth;

class EventController extends Controller
{

	public function __construct()
    {
        $this->middleware([ 'auth','check_permission' ]);
    }


    public function showAddEventForm(){
    	return view('events/add');
    }

    public function saveEvent(Request $request){
        
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'date' => 'required'
        ]);
        
        if ($validator->fails()) {
            return redirect(route('add_event'))
                        ->withErrors($validator)
                        ->withInput();
        }

        // 
		$data = [
			'name' => $request->input('name'),
			'description' => ($request->has('description') ? $request->input('description') : ''),
			'date' => $request->input('date'),
			'created_by' => Auth::id()
		];

		$event = Event::create($data);

        return redirect(route('view_events'))->with('status', 'Event Created Successfully!');
    }

    public function listEvents(){
    	return view('events/list');
    }

    public function showCalender(){
    	$events = Event::all();
    	return view('events/calendar' , [ 'events' => $events ]);
    }


    public function showEditForm(Request $request){

    	if(!$request->has('event_id')){
    		return redirect(route('view_events'));
    	}

    	$event = Event::where( 'id' , '=' , $request->input('event_id') )->first();

		return view('events/edit', [ 'event' => $event ]);
    }

    public function updateEvent(Request $request){
        $validator = Validator::make($request->all(), [
            'event_id' => 'required',
            'name' => 'required',
            'date' => 'required'
        ]);
        
        if ($validator->fails()) {
            return redirect(route('view_events'))
                        ->withErrors($validator)
                        ->withInput();
        }

        // 
		$data = [
			'name' => $request->input('name'),
			'description' => ($request->has('description') ? $request->input('description') : ''),
			'date' => $request->input('date'),
		];

		Event::where( 'id', '=', $request->input('event_id') )->update($data);

        return redirect(route('view_events'))->with('status', 'Event Created Successfully!');    	
    }


    public function deleteEvent(Request $request){
    	if(!$request->has('event_id')){
    		return redirect(route('view_events'));
    	}
    	
    	Event::where( 'id' , '=' , $request->input('event_id') )->delete();
    	
    	return redirect(route('view_events'))->with('status', 'Event Deleted Successfully!');
    }

    public function getEventDate(Request $request){

        // query filter
        $q = "";
        if($request->has('name') &&  !empty($request->input('name'))){
            $q = $request->input('name');
        }

    	$query = Event::where('name','LIKE','%'.$q.'%');

    	$query->orWhere('description','LIKE','%'.$q.'%');

    	$events = $query->orderBy('id', 'DESC')->get();

    	return Datatables::of($events)->make();
    }
}
