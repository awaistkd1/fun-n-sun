<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PdfReport;
use App\Booking;
use Carbon\Carbon;
use App\BookingExtraServices;
use App\Coupon;
use App\TripExtraServices;
use App\Exports\ReservationExport;
use App\Exports\AgentExport;
use App\Exports\AssetsExport;
use App\Trip;
use Config;
use Excel;
use App\User;
use App\Helper\TripHelper;

class ReportController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware([ 'auth','check_permission' ]);
    }	

    public function viewAll(){
    	return view('report/list',['trips' => Trip::all(),'agents'=>User::where('id','!=',1)->get()]);
    }

	public function displayDailyTripReservationReport(Request $request) {
		
		if(!$request->has('date')){
			die("Please select a date");
		}

		$date = $request->input('date');
		$trip_id = $request->input('trip_id');

		$date_carbon = Carbon::createFromFormat('Y-m-d',$date);

		$q = Booking::where('trip_id','=',$trip_id);

		$q->whereDay('created_at', '=', $date_carbon->day);
		$q->whereMonth('created_at', '=', $date_carbon->month);
		$q->whereYear('created_at', '=', $date_carbon->year);

		$allbookings = [];

		foreach ($q->get() as $booking) {
			// calculate total cost
			$total_cost = TripHelper::getTotalBookingRevenue($booking);

			$time_for_reservation = Carbon::parse($booking->created_at)->format('h:i:s A');

			$paid_amount = $booking->amount_paid;
			$unpaid = $total_cost - $paid_amount;

			if($booking->coupon_id == NULL){
				$coupon_code = "";
			}else{

				if(Coupon::where('id','=',$booking->coupon_id)->exists()){
					$coupon_code = Coupon::where('id','=',$booking->coupon_id)->first()->code;
				}else{
					$coupon_code = "This coupon code have deleted";		
				}
			}


			$infant_qty = $booking->infant_qty;
			$child_qty = $booking->child_qty;
			$adult_qty = $booking->adult_qty;

			// get all extra services
			$extraServices = [];

			$trip_extra_services = TripExtraServices::where('trip_id','=',$trip_id)->get();


			foreach ($trip_extra_services as $extra_service) {

				$es = $extra_service->extraService()->first();
				$booking_extra_service = NULL;
				if(BookingExtraServices::where('booking_id','=',$booking->id)->where('extra_service_id','='.$es->id)->exists()){
					$booking_extra_service = BookingExtraServices::where('booking_id','=',$booking->id)->where('extra_service_id','='.$es->id)->first();
				}

				array_push($extraServices, [
					'name' => $es->name,
					'price' => $es->price,
					'qty' => ($booking_extra_service == NULL) ? 0 : $extra_service->qty
				]);
			}


			$agent = $booking->sales_agent()->first();
			$customer = $booking->customer()->first();

			$customer_name = $customer->first_name;
			
			if($customer->middle_name != NULL){
				$customer_name .= " ".$customer->middle_name;
			}

			$customer_name .= " ".$customer->last_name;

			$data = [
				'time' => $time_for_reservation,
				'total_amount' => $total_cost,
				'extra_services' => $extraServices,
				'unpaid' => $unpaid,
				'paid_amount' => $paid_amount,
				'coupon_code' => $coupon_code,
				'infant_qty' => $infant_qty,
				'child_qty' => $child_qty,
				'adult_qty' => $adult_qty,
				'reservation_id' => $booking->id,
				'agent' => $agent->name,
				'customer' => $customer_name,
				'mobile' => $customer->phone
			];

			array_push($allbookings, $data);
		}


		$colums = [
			'Time of Reservation',	
			'Total Cash (net)',	
			'Mobile'
		];


		$trip_extra_services = TripExtraServices::where('trip_id','=',$trip_id)->get();

		foreach ($trip_extra_services as  $tes) {
			$es = $tes->extraService()->first();
			$colums[]= $es->name." (".Config::get('preferences.currency').")";
			$colums[]= $es->name." (Qty)";
		}

		$colums[] = "UnPaid";
		$colums[] = "Paid";
		$colums[] = "Voucher Used";
		$colums[] = "Infant";
		$colums[] = "Child";
		$colums[] = "Adult";
		$colums[] = "User";
		$colums[] = "Name";
		$colums[] = "Reservation No.";

		ob_end_clean();
		return Excel::download(new ReservationExport($allbookings,$colums,$date), 'reservations.xlsx');

	}


	public function agentReport(Request $request){

		$from = $request->input('start_date');
		$to = $request->input('end_date');
		$agent_id = $request->input('agent_id');

		$bookings = Booking::whereBetween('created_at', [$from, $to])->where('sales_agent_id','=',$agent_id)->get();




		$allbookings = [];

		foreach ($bookings as $booking) {
			// calculate total cost
			$total_cost = TripHelper::getTotalBookingRevenue($booking);

			$time_for_reservation = Carbon::parse($booking->created_at)->format('h:i:s A');

			$paid_amount = $booking->amount_paid;
			$unpaid = $total_cost - $paid_amount;

			if($booking->coupon_id == NULL){
				$coupon_code = "";
			}else{

				if(Coupon::where('id','=',$booking->coupon_id)->exists()){
					$coupon_code = Coupon::where('id','=',$booking->coupon_id)->first()->code;
				}else{
					$coupon_code = "This coupon code have deleted";		
				}
			}


			$infant_qty = $booking->infant_qty;
			$child_qty = $booking->child_qty;
			$adult_qty = $booking->adult_qty;

			// get all extra services
			$extraServices = [];

			$trip_extra_services = TripExtraServices::where('trip_id','=',$booking->trip_id)->get();

			$extra_service_total = 0;

			foreach ($trip_extra_services as $extra_service) {

				$es = $extra_service->extraService()->first();
				$booking_extra_service = NULL;
				if(BookingExtraServices::where('booking_id','=',$booking->id)->where('extra_service_id','='.$es->id)->exists()){
					$booking_extra_service = BookingExtraServices::where('booking_id','=',$booking->id)->where('extra_service_id','='.$es->id)->first();
				}

				$extra_service_total += ($booking_extra_service == NULL) ? 0 : $extra_service->qty*$es->price;
			}


			$agent = $booking->sales_agent()->first();
			$customer = $booking->customer()->first();

			$customer_name = $customer->first_name;
			
			if($customer->middle_name != NULL){
				$customer_name .= " ".$customer->middle_name;
			}

			$customer_name .= " ".$customer->last_name;

			$data = [
				'time' => $time_for_reservation,
				'total_amount' => $total_cost,
				'extra_services' => $extraServices,
				'unpaid' => $unpaid,
				'paid_amount' => $paid_amount,
				'coupon_code' => $coupon_code,
				'infant_qty' => $infant_qty,
				'child_qty' => $child_qty,
				'adult_qty' => $adult_qty,
				'reservation_id' => $booking->id,
				'agent' => $agent->name,
				'customer' => $customer_name,
				'mobile' => $customer->phone,
				'extra_service_total' => $extra_service_total
			];

			array_push($allbookings, $data);
		}


		$colums = [
			'Time of Reservation',	
			'Total Cash (net)',	
			'Mobile'
		];

		$colums[] = "Extra Service Total";
		$colums[] = "UnPaid";
		$colums[] = "Paid";
		$colums[] = "Voucher Used";
		$colums[] = "Infant";
		$colums[] = "Child";
		$colums[] = "Adult";
		$colums[] = "User";
		$colums[] = "Name";
		$colums[] = "Reservation No.";
		$agent_name = User::where('id','=',$agent_id)->first()->name;
		ob_end_clean();
		return Excel::download(new AgentExport($allbookings,$colums,$from,$to,$agent_name), 'reservations.xlsx');
	}

	public function assetReport(Request $request){
		$from_date = $request->input('from_date');
		$to_date = $request->input('to_date');
		
		ob_end_clean();
		return Excel::download(new AssetsExport($from_date,$to_date), 'asset-report.xlsx'); 
	}
}
?>