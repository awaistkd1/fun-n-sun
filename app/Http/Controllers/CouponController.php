<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use Carbon\Carbon;


use DataTables;

use App\CouponValidTrips;
use App\Trip;
use App\Coupon;



class CouponController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware([ 'auth','check_permission' ]);
    }


    public function form(){
        $trips = Trip::all();
    	return view('coupon/add',['trips' => $trips]);
    }

    public function show(){
    	return view('coupon/list');
    }

    public function save(Request $request){

        $validator = Validator::make($request->all(), [
            'trips' => 'required',
            'code' => 'required',
            'valid_from' => 'required',
            'valid_to' => 'required',
            'number_of_use' => 'required|numeric',
            'discount_amount' => 'required',
            'discount_type' => 'required|numeric'
        ]);

        
        if ($validator->fails()) {
            return redirect(route('add_coupon'))
                        ->withErrors($validator)
                        ->withInput();
        }


        $data = [
            'code' => $request->input('code'),
            'valid_from' => Carbon::CreateFromFormat('m/d/Y',$request->input('valid_from'))->format('Y-m-d'),
            'valid_to' => Carbon::CreateFromFormat('m/d/Y',$request->input('valid_to'))->format('Y-m-d'),
            'number_of_use' => $request->input('number_of_use'),
            'discount_type' => $request->input('discount_type'),
            'discount_amount' => $request->input('discount_amount'),
            'created_by' => Auth::id()
        ];

        $coupon = Coupon::create($data);

        $valid_trips = $request->input('trips');

        foreach ($valid_trips as $trip) {
            
            CouponValidTrips::create([
                'trip_id' => $trip,
                'coupon_id' => $coupon->id
            ]);

        }

        return redirect(route('view_coupon'))->with('status', 'Coupon Added Successfully!');

    }

    public function viewById(Request $request){

        if(!$request->has('id')){
            return redirect(route('view_coupon'));
        }


        $coupon_id = $request->input('id');

        $coupon = Coupon::where('id','=',$coupon_id)->first();

        return view('coupon/coupon',['coupon' => $coupon]);

    }


    public function showEditForm(Request $request){

        if(!$request->has('coupon_id')){
            return redirect(route('view_coupon'));
        }

        $coupon_id = $request->input('coupon_id');
        $coupon = Coupon::where('id','=',$coupon_id)->first();

        $trips_ids = CouponValidTrips::select(['trip_id'])->where('coupon_id', '=', $coupon->id)->get();
        
        $valid_trips = [];

        foreach ($trips_ids as $t_id) {
            $valid_trips[]=$t_id->trip_id;
        }

        $trips = Trip::all();

        return view('coupon/edit',[ 
            'trips' => $trips, 
            'coupon' => $coupon, 
            'valid_trips' => $valid_trips
        ]);

    }

    public function saveEditForm(Request $request){
        
        $validator = Validator::make($request->all(), [
            'coupon_id' => 'required',
            'trips' => 'required',
            'code' => 'required',
            'valid_from' => 'required',
            'valid_to' => 'required',
            'number_of_use' => 'required|numeric',
            'discount_amount' => 'required',
            'discount_type' => 'required|numeric'
        ]);

        
        if ($validator->fails()) {
            return redirect(route('add_coupon'))
                        ->withErrors($validator)
                        ->withInput();
        }


        $data = [
            'code' => $request->input('code'),
            'valid_from' => Carbon::CreateFromFormat('m/d/Y',$request->input('valid_from'))->format('Y-m-d'),
            'valid_to' => Carbon::CreateFromFormat('m/d/Y',$request->input('valid_to'))->format('Y-m-d'),
            'number_of_use' => $request->input('number_of_use'),
            'discount_type' => $request->input('discount_type'),
            'discount_amount' => $request->input('discount_amount')
        ];

        $coupon_id = $request->input('coupon_id');

        $coupon = Coupon::where('id','=',$coupon_id)->update($data);

        $valid_trips = $request->input('trips');

        CouponValidTrips::where('coupon_id', '=', $coupon_id)->delete();

        foreach ($valid_trips as $trip) {
            
            CouponValidTrips::create([
                'trip_id' => $trip,
                'coupon_id' => $coupon_id
            ]);

        }

        return redirect(route('view_coupon'))->with('status', 'Coupon Updated Successfully!');
    }

    public function deleteCoupon(Request $request){
        $validator = Validator::make($request->all(), [
            'coupon_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return redirect(route('view_coupon'))
                        ->withErrors($validator)
                        ->withInput();
        }

        $coupon_id = $request->input('coupon_id');

        if(Coupon::where( 'id' , '=' , $coupon_id )->exists()){

            Coupon::where( 'id' , '=' , $coupon_id )->delete();

        }else{
            return redirect(route('view_coupon'))
                        ->withErrors(['trip_err'=>'Coupon do not exist.']);
        }

        return redirect(route('view_coupon'))->with('status', 'Coupon Deleted Successfully!');        
    }

    public function getCouponData(Request $request){

        $coupons = Coupon::select(['id','code', 'valid_from', 'valid_to', 'number_of_use', 'discount_type', 'discount_amount']);

        // query filter
        $q = "";

        if($request->has('name') &&  !empty($request->input('name'))){
            $q = $request->input('name');
        }

        $coupons->where('code','LIKE','%'.$q.'%');


        if($request->has('state') &&  !empty($request->input('state'))){
            $state = $request->input('state');
            if($state == 'valid'){
                $coupons->where('valid_to','>=',Carbon::today()->toDateString());
            }else{
                $coupons->where('valid_to','<',Carbon::today()->toDateString());
            }
        }

        $coupons->orderBy('id', 'DESC');

        return Datatables::of($coupons)->make();
    }

}
