<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use Config;
use Storage;
use Validator;

use Carbon\Carbon;
use App\User;
use App\UserGroup;

class SalesAgentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware([ 'auth','check_permission' ]);
    }

    public function form(){
    	return view('sales_agents/add');
    }

    public function show(){
        $user_groups = UserGroup::where('id','!=','1')->get();
    	return view('sales_agents/list',['user_groups' => $user_groups]);
    }


    public function viewById(Request $request){
        if(!$request->has('id')){
            return redirect(route('view_agent'));
        }

        $agent_id = $request->input('id');
        $agent = User::where('id','=',$agent_id)->first();

        return view('sales_agents/agent',[ 'agent' => $agent ]);        
    }


    public function showEditForm(Request $request){
        $user_groups = UserGroup::where('id','!=','1')->get();
        if(!$request->has('agent_id')){
            return redirect(route('view_agent'));
        }

        $agent_id = $request->input('agent_id');
        $agent = User::where('id','=',$agent_id)->first();
        
        return view('sales_agents/edit' , [ 'user_groups' => $user_groups, 'agent' => $agent ]);
    }


    public function saveEditAgent(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'user_group_id' => 'required|numeric'
        ]);

        
        if ($validator->fails()) {
            return redirect(route('view_agent'));
        }


        $avatar = "default/avatar.jpg";

        if($request->has('images')){
           $images=$request->input('images');
           $avatar = $images[0]; 
        }


        $data = [
            'user_group_id' => $request->input('user_group_id'), 
            'name' => $request->input('name'), 
            'email' => $request->input('email'),
            'picture' => $avatar,
            'mobile' => $request->input('mobile'),
            'description' => $request->input('description')
        ];


        $user = User::where('id','=',$request->input('agent_id'))->update($data);


        return redirect(route('view_agent'))->with('status', 'Agent Updated Successfully!');        
    }

    public function deleteAgent(Request $request){

        $validator = Validator::make($request->all(), [
            'agent_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return redirect(route('view_agent'))
                        ->withErrors($validator)
                        ->withInput();
        }

        $agent_id = $request->input('agent_id');

        if(User::where( 'id' , '=' , $agent_id )->exists()){

            User::where( 'id' , '=' , $agent_id )->delete();

        }else{
            return redirect(route('view_agent'))
                        ->withErrors(['agent_err'=>'Agent do not exist.']);
        }

        return redirect(route('view_agent'))->with('status', 'Agent Deleted Successfully!');
    }

    public function getSalesAgentData(Request $request){

        $query = User::where('user_group_id','!=', Config::get('admin.user_group'));


        // query filter
        $q = "";
        if($request->has('name') &&  !empty($request->input('name'))){
            $q = $request->input('name');
        }

        $query->where('name','LIKE','%'.$q.'%');

        // phone filter
        if($request->has('phone') && !empty($request->input('phone'))){
            $phone = $request->input('phone');
            $query->where('mobile','=',$phone);
        }

        // email filter
        if($request->has('email') && !empty($request->input('email'))){
            $email = $request->input('email');
            $query->where('email','=',$email);
        }

        // user group filter
        if($request->has('user_group') && !empty($request->input('user_group'))){
            $user_group = $request->input('user_group');
            $query->where('user_group_id','=',$user_group);
        }

        $agents = $query->orderBy('id', 'DESC')->get();

        $all_agents = [];
        foreach ($agents as $agent) {
            $data = [
                'id' => $agent->id,
                'name' => $agent->name,
                'email' => $agent->email,
                
                'mobile' => $agent->mobile,
                'user_group' => UserGroup::where('id','=',$agent->user_group_id)->first()->name
            ];

            array_push($all_agents, $data);
        }

        return Datatables::of($all_agents)->make();
    }

}
