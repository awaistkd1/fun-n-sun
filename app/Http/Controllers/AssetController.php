<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use DataTables;

use App\Asset;

use Carbon\Carbon;

class AssetController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware([ 'auth', 'check_permission' ]);
    }


    public function form(){
    	return view('asset/add');
    }

    public function show(){
    	return view('asset/list');
    }

    public function save(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'min_capacity' => 'required|numeric',
            'max_capacity' => 'required|numeric',
            'per_agent_booking_limit' => 'required|numeric'
        ]);

        
        if ($validator->fails()) {
            return redirect(route('add_asset'))
                        ->withErrors($validator)
                        ->withInput();
        }


        $asset = Asset::create([
            'created_by' => Auth::id(), 
            'name' => $request->input('name'), 
            'min_capacity' => $request->input('min_capacity'), 
            'max_capacity' => $request->input('max_capacity'), 
            'per_agent_booking_limit' => $request->input('per_agent_booking_limit'), 
            'description' => $request->input('description'), 
            'asset_ref' => '' 
        ]);

        $ref = "ASS".Carbon::now()->year.str_pad($asset->id,4,"0",STR_PAD_LEFT);


        $asset->asset_ref = $ref;

        $asset->save();

        // redirect to listing page
        return redirect(route('view_asset'))->with('status', 'Asset Added Successfully!');
    }

    public function viewById(Request $request){

        if(!$request->has('id')){
            return redirect()->to(route('view_asset'));
        }

        $id = $request->input('id');

        $asset = Asset::where('id','=',$id)->first();

        return view('asset/asset',[ 'asset' => $asset ]);
    }

    public function deleteAsset(Request $request){

        $validator = Validator::make($request->all(), [
            'asset_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return redirect(route('view_asset'))
                        ->withErrors($validator)
                        ->withInput();
        }

        $asset_id = $request->input('asset_id');

        if(Asset::where( 'id' , '=' , $asset_id )->exists()){

            Asset::where( 'id' , '=' , $asset_id )->delete();

        }else{
            return redirect(route('view_asset'))
                        ->withErrors(['trip_err'=>'Asset do not exist.']);
        }

        return redirect(route('view_asset'))->with('status', 'Asset Deleted Successfully!');
    }

    public function editAsset(Request $request){
        if(!$request->has('asset_id')){
            return redirect(route('view_asset'));
        }

        $asset_id = $request->input('asset_id');
        $asset = Asset::where('id','=',$asset_id)->first();

        return view('asset/edit',['asset'=>$asset]);
    }

    public function saveEditAsset(Request $request){

        $validator = Validator::make($request->all(), [
            'asset_id' => 'required|numeric',
            'name' => 'required',
            'min_capacity' => 'required|numeric',
            'max_capacity' => 'required|numeric',
            'per_agent_booking_limit' => 'required|numeric'
        ]);

        
        if ($validator->fails()) {
            return redirect(route('add_asset'))
                        ->withErrors($validator)
                        ->withInput();
        }


        $data = [
            'name' => $request->input('name'), 
            'min_capacity' => $request->input('min_capacity'), 
            'max_capacity' => $request->input('max_capacity'), 
            'per_agent_booking_limit' => $request->input('per_agent_booking_limit'), 
            'description' => $request->input('description'),  
        ];

        Asset::where('id','=',$request->input('asset_id'))->update($data);
        
        // redirect to listing page
        return redirect(route('view_asset'))->with('status', 'Asset Updated Successfully!');
    }

    public function getAssetData(Request $request){
        
        $assets = Asset::select([
            'id',
            'name',
            'min_capacity',
            'max_capacity',
            'per_agent_booking_limit'
        ]);

        $q = "";
        
        if($request->has('name') &&  !empty($request->input('name'))){
            $q = $request->input('name');
        }

        $assets->where('name','LIKE','%'.$q.'%');
        $assets->orWhere('asset_ref','=',$q);
        $assets->orderBy('id', 'DESC');

        return Datatables::of($assets)->make();

    }

}
