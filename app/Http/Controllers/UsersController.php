<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserGroup;
use App\User;
use DataTables;
use Storage;
use Validator;
use Auth;
use Config;
use Hash;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware([ 'auth','check_permission' ]);
    }

    public function form(){
        $user_groups = UserGroup::where('id','!=','1')->get();
    	return view('users/add' , [ 'user_groups' => $user_groups ]);
    }

    public function show(){
        $user_groups = UserGroup::where('id','!=','1')->get();
    	return view('users/list',['user_groups' => $user_groups]);
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'user_group_id' => 'required|numeric'
        ]);

        
        if ($validator->fails()) {
            return redirect(route('add_users'))
                        ->withErrors($validator)
                        ->withInput();
        }

        if(User::where('email','=',$request->input('email'))->exists()){
            return redirect(route('add_users'))
                        ->withErrors([
                            'email' => 'This user already exists'
                        ])
                        ->withInput();
        }


        $avatar = "default/avatar.jpg";

        if($request->has('images')){
           $images=$request->input('images');
           $avatar = $images[0]; 
        }


        $data = [
            'user_group_id' => $request->input('user_group_id'), 
            'name' => $request->input('name'), 
            'email' => $request->input('email'), 
            'password' => bcrypt($request->input('password')),
            'picture' => $avatar,
            'mobile' => $request->input('mobile'),
            'description' => $request->input('description'),
            'created_by' => Auth::id()
        ];


        $user = User::create($data);


        return redirect(route('view_users'))->with('status', 'User Created Successfully!');
    }


    public function viewById(Request $request){
        if(!$request->has('id')){
            return redirect(route('view_users'));
        }

        $user_id = $request->input('id');
        $user = User::where('id','=',$user_id)->first();

        return view('users/user',[ 'user' => $user ]);        
    }


    public function getProfile(Request $request){
        $user = User::where('id','=',Auth::id())->first();
        
        return view('users/profile' , ['user' => $user ]);        
    }

    public function updateProfile(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email'
        ]);

        
        if ($validator->fails()) {
            return redirect(route('view_profile'));
        }


        $avatar = "default/avatar.jpg";

        if($request->has('images')){
           $images=$request->input('images');
           $avatar = $images[0]; 
        }


        $data = [
            'name' => $request->input('name'), 
            'email' => $request->input('email'),
            'picture' => $avatar,
            'mobile' => $request->input('mobile'),
            'description' => $request->input('description')
        ];

        $user = User::where('id','=',Auth::id())->update($data);

        return redirect(route('view_profile'))->with('status', 'Profile Updated Successfully!');        
    }

    public function updatePasswordForm(){
        return view('users/password');
    }

    public function updatePassword(Request $request){

        if (!(Hash::check($request->input('current'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->input('current'), $request->input('new')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'current' => 'required',
            'new' => 'required|string|min:5',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->input('new'));
        $user->save();

        return redirect()->back()->with("status","Password changed successfully !");

    }

    public function showEditForm(Request $request){
        $user_groups = UserGroup::where('id','!=','1')->get();
        if(!$request->has('user_id')){
            return redirect(route('view_users'));
        }

        $user_id = $request->input('user_id');
        $user = User::where('id','=',$user_id)->first();
        
        return view('users/edit' , [ 'user_groups' => $user_groups, 'user' => $user ]);
    }


    public function saveEditUser(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'user_group_id' => 'required|numeric'
        ]);

        
        if ($validator->fails()) {
            return redirect(route('view_users'));
        }


        $avatar = "default/avatar.jpg";

        if($request->has('images')){
           $images=$request->input('images');
           $avatar = $images[0]; 
        }


        $data = [
            'user_group_id' => $request->input('user_group_id'), 
            'name' => $request->input('name'), 
            'email' => $request->input('email'),
            'picture' => $avatar,
            'mobile' => $request->input('mobile'),
            'description' => $request->input('description')
        ];


        $user = User::where('id','=',$request->input('user_id'))->update($data);


        return redirect(route('view_users'))->with('status', 'User Updated Successfully!');        
    }

    public function deleteUser(Request $request){

        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return redirect(route('view_users'))
                        ->withErrors($validator)
                        ->withInput();
        }

        $user_id = $request->input('user_id');

        if(User::where( 'id' , '=' , $user_id )->exists()){

            User::where( 'id' , '=' , $user_id )->delete();

        }else{
            return redirect(route('view_users'))
                        ->withErrors(['user_err'=>'User do not exist.']);
        }

        return redirect(route('view_agent'))->with('status', 'User Deleted Successfully!');
    }

    public function getUsersData(Request $request){


        $query = User::where('user_group_id','!=', Config::get('admin.user_group'));


        // query filter
        $q = "";
        if($request->has('name') &&  !empty($request->input('name'))){
            $q = $request->input('name');
        }

        $query->where('name','LIKE','%'.$q.'%');

        // phone filter
        if($request->has('phone') && !empty($request->input('phone'))){
            $phone = $request->input('phone');
            $query->where('mobile','=',$phone);
        }

        // email filter
        if($request->has('email') && !empty($request->input('email'))){
            $email = $request->input('email');
            $query->where('email','=',$email);
        }

        // user group filter
        if($request->has('user_group') && !empty($request->input('user_group'))){
            $user_group = $request->input('user_group');
            $query->where('user_group_id','=',$user_group);
        }

        $users = $query->orderBy('id', 'DESC')->get();


        $all_users = [];
        foreach ($users as $user) {
            $data = [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                
                'mobile' => $user->mobile,
                'user_group' => UserGroup::where('id','=',$user->user_group_id)->first()->name
            ];

            array_push($all_users, $data);
        }

        return Datatables::of($all_users)->make();
    }


}
