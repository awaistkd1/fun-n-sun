<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Asset;
use App\Booking;
use App\Customer;
use App\Coupon;
use App\Trip;
use App\User;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware([ 'auth','check_permission' ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::id() == 1){
        
            $total_assets = Asset::count();
            $total_trips = Trip::count();
            $total_coupon = Coupon::count();
            $total_customer = Customer::count();
            $total_users = User::count();
            $total_agents = $total_users - 1;
            $total_booking = Booking::count();
        
        }else{
        
            $total_assets = Asset::where('created_by','=',Auth::id())->count();
            $total_trips = Trip::where('created_by','=',Auth::id())->count();
            $total_coupon = Coupon::where('created_by','=',Auth::id())->count();
            $total_customer = Customer::where('created_by','=',Auth::id())->count();
            $total_users = User::where('created_by','=',Auth::id())->count();
            $total_agents = $total_users;
            $total_booking = Booking::where('created_user_id','=',Auth::id())->count();
        
        }

        return view('dashboard',[
            'total_assets' => $total_assets,
            'total_users' => $total_users,
            'total_agents' => $total_agents,
            'total_customers' => $total_customer,
            'total_trips' => $total_trips,
            'total_coupons' => $total_coupon,
            'total_bookings' => $total_booking
        ]);

    }
}
