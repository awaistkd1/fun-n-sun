<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use Auth;
use DataTables;

use Carbon\Carbon;
use App\Customer;

class CustomerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware([ 'auth','check_permission' ]);
    }


    public function form(){
    	return view('customer/add');
    }

    public function show(){
    	return view('customer/list');
    }


    public function save(Request $request){

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'middle_name' => 'nullable',
            'last_name' => 'required',
            'dob' => 'nullable',
            'email' => 'required|email',
            'mobile' => 'required',
            'referral_email' => 'nullable|email',
            'referral_name' => 'nullable',
            'description' => 'nullable'
        ]);
        

        if ($validator->fails()) {
            return redirect(route('add_customer'))
                        ->withErrors($validator)
                        ->withInput();
        }


        $data = [
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'middle_name' => $request->input('middle_name'),
            'phone' => $request->input('mobile'),
            'email' => $request->input('email'),
            'dob' => Carbon::CreateFromFormat('m-d-Y',$request->input('dob'))->format('Y-m-d'),
            'description' => $request->input('description'),
            'referral_name' => $request->input('referral_name'),
            'referral_email' => $request->input('referral_email'),
            'created_by' => Auth::id()
        ];

        $customer = Customer::create($data);


        return redirect(route('view_customer'))->with('status', 'Customer Added Successfully!');
    }

    public function viewById(Request $request){
        if(!$request->has('id')){
            return redirect(route('view_customer'));
        }


        $customer_id = $request->input('id');

        $customer = Customer::where('id','=',$customer_id)->first();

        return view('customer/customer',['customer' => $customer]);
    }

    public function editCustomer(Request $request){
        if(!$request->has('customer_id')){
            return redirect(route('view_customer'));
        }

        $customer_id = $request->input('customer_id');
        $customer = Customer::where('id','=',$customer_id)->first();
        
        return view('customer/edit',['customer' => $customer]);        
    }

    public function saveEditCustomer(Request $request){
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required',
            'first_name' => 'required',
            'middle_name' => 'nullable',
            'last_name' => 'required',
            'dob' => 'nullable',
            'email' => 'required|email',
            'mobile' => 'required',
            'referral_email' => 'nullable|email',
            'referral_name' => 'nullable',
            'description' => 'nullable'
        ]);
        

        if ($validator->fails()) {
            return redirect(route('add_customer'))
                        ->withErrors($validator)
                        ->withInput();
        }


        $data = [
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'middle_name' => $request->input('middle_name'),
            'phone' => $request->input('mobile'),
            'email' => $request->input('email'),
            'dob' => Carbon::CreateFromFormat('m-d-Y',$request->input('dob'))->format('Y-m-d'),
            'description' => $request->input('description'),
            'referral_name' => $request->input('referral_name'),
            'referral_email' => $request->input('referral_email')
        ];

        Customer::where('id','=',$request->input('customer_id'))->update($data);

        return redirect(route('view_customer'))->with('status', 'Customer Updated Successfully!');        
    }

    public function deleteCustomer(Request $request){
        if(!$request->has('customer_id')){
            return redirect(route('view_customer'));
        }

        $customer_id = $request->input('customer_id');

        Customer::where('id','=',$customer_id)->delete();
        return redirect(route('view_customer'))->with('status', 'Customer Deleted Successfully!');   
    }


    public function searchByPhone(Request $request){

        $phone  = $request->input('term');

        $customers = Customer::where('phone', 'LIKE', '%'.$phone.'%')->get();

        return response()->json($customers);
    }


    public function getCustomerData(Request $request){

        $customers = Customer::select([
            'id',
            'first_name',
            'last_name',
            'middle_name',
            'phone', 
            'email', 
            'dob', 
            'referral_name', 
            'referral_email'
        ]);


        // query filter
        $q = "";
        if($request->has('name') &&  !empty($request->input('name'))){
            $q = $request->input('name');
        }

        $customers->orWhere(function ($query) use ($q) {
            $query->where('first_name','LIKE','%'.$q.'%');
            $query->orWhere('middle_name','LIKE','%'.$q.'%');
            $query->orWhere('last_name','LIKE','%'.$q.'%');
        });


        // phone filter
        if($request->has('phone') && !empty($request->input('phone'))){
            $phone = $request->input('phone');
            $customers->where('phone','=',$phone);
        }

        // email filter
        if($request->has('email') && !empty($request->input('email'))){
            $email = $request->input('email');
            $customers->where('email','=',$email);
        }               

        $customers->orderBy('id', 'DESC');
        
        return Datatables::of($customers)->make();
    }

}
