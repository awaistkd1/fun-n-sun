<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use Carbon\Carbon;
use DataTables;
use Config;
use App\Asset;
use App\AssetImage;
use App\UserGroup;
use App\Trip;
use App\TripDiscount;
use App\ExtraService;
use App\ExtraServiceDiscount;
use App\TripExtraServices;
use App\Booking;
use App\TripImage;
use App\Helper\TripHelper;

class TripController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware([ 'auth','check_permission' ]);
    }


    public function form(){
        $assets = Asset::all();
        $user_groups = UserGroup::where('id','!=','1')->get();
    	return view('trip/add',['assets'=>$assets, 'user_groups'=>$user_groups]);
    }

    public function show(){
        $assets = Asset::all();
    	return view('trip/list',['assets' => $assets]);
    }

    public function save(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'asset_id' => 'required',
            'date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'repeat' => 'required',
            'adult_price' => 'required',
            'child_price' => 'required',
            'infant_price' => 'required'
        ]);

        
        if ($validator->fails()) {
            return redirect(route('add_asset'))
                        ->withErrors($validator)
                        ->withInput();
        }


        // save trip
        $trip_data = [
            'asset_id' => $request->input('asset_id'),
            'name' => $request->input('name'),
            'date' => Carbon::CreateFromFormat('m-d-Y',$request->input('date'))->format('Y-m-d'),
            'start_date' => $request->input('start_time'),
            'end_date' => $request->input('end_time'),
            'repeat' => $request->input('repeat'),
            'description' => $request->input('description'),
            'adult_price' => $request->input('adult_price'),
            'child_price' => $request->input('child_price'),
            'infant_price' => $request->input('infant_price'),
            'trip_ref' => '',
            'created_by' => Auth::id(),
            'status' => Config::get('trip.open')
        ];

        $trip = Trip::create($trip_data);

        $trip->trip_ref = "TRIP".Carbon::now()->year.str_pad($trip->id,4,"0",STR_PAD_LEFT);

        $trip->save();

        // insert price discounts


        if($request->has('price_discounts')){

            $price_discounts = $request->input('price_discounts');
            foreach ($price_discounts as $discount) {
                TripDiscount::create([
                    'trip_id' => $trip->id,
                    'user_group_id' => $discount[0],
                    'adult' => $discount[1],
                    'child' => $discount[2],
                    'infant' => $discount[3]
                ]);
            }

        }

        // insert extra services
        
        if($request->has('extra_services')){
            $extra_services = $request->input('extra_services');
            foreach ($extra_services as $service) {
                
                $es = ExtraService::create([
                    'name' => $service[0],
                    'price' => $service[1]
                ]);

                // save extra service discount
                if(isset($service['discounts'])){

                    foreach ($service['discounts'] as $discount) {
                        ExtraServiceDiscount::create([
                            'extra_service_id' => $es->id,
                            'user_group_id' => $discount[0],
                            'discount' => $discount[1]
                        ]);
                    }

                }

                // save trip extra services
                TripExtraServices::create([
                    'trip_id' => $trip->id,
                    'extra_service_id' => $es->id
                ]);
            }
        }


        // save the images

        if($request->has('images')){

            foreach($request->input('images') as $image) {
            
                TripImage::create([
                    'trip_id' => $trip->id,
                    'path' => $image
                ]);
            
            }
        }

        return redirect(route('view_trip'))->with('status', 'Trip Added Successfully!');
    }

    public function viewById(Request $request){

        if(!$request->has('id')){
            return redirect()->to(route('bookings'));
        }

        $id = $request->input('id');
        $trip = Trip::where('id','=',$id)->first();

        $trips_discounts = $trip->tripDiscounts()->get();

        // calculate available booking
        $total_booking_on_trip = Booking::where('trip_id','=',$id)->sum('ticket_qty');
        $my_total_booking_on_trip = Booking::where('trip_id','=',$id)->where('created_user_id','=',Auth::id())->sum('ticket_qty');

        $ast = Asset::where('id','=',$trip->asset_id)->first();
        
        $asset_capacity = $ast->max_capacity;
        $asset_booking_limit = $ast->per_agent_booking_limit;

        $tickets_left = $asset_capacity - $total_booking_on_trip;
        $my_limit = $asset_booking_limit - $my_total_booking_on_trip;

        $discounts = [];
        foreach ($trips_discounts as $discount) {
            $user_group = $discount->userGroup()->first();
            array_push($discounts, [
                'user_group' => $user_group->name,
                'adult' => $discount->adult,
                'child' => $discount->child,
                'infant' => $discount->infant
            ]);
        }

        $revenue = TripHelper::calculateTripRevenue($trip);

        return view('trip/trip',[
            'trip' => $trip,
            'discounts' => $discounts,
            'tickets_left' => $tickets_left,
            'my_limit' => $my_limit,
            'total_revenue' => $revenue['total'],
            'total_paid' => $revenue['paid'],
            'total_unpaid' => $revenue['unpaid'],
            'total_tickets_booked' => $total_booking_on_trip
        ]);

    }

    public function changeTripStatus(Request $request){
        $validator = Validator::make($request->all(), [
            'trip_id' => 'required',
            'status' => 'required'
        ]);

        
        if ($validator->fails()) {
            return redirect(route('view_trip_by_id').'?id='.$request->input('trip_id'))
                        ->withErrors($validator)
                        ->withInput();
        }

        if(!in_array($request->input('status'), Config::get('trip'))){
            die("Invalid trip status");
        }

        $trip = Trip::where('id','=',$request->input('trip_id'))->first();
        $trip->status = $request->input('status');
        $trip->save();
        
        return redirect(route('view_trip_by_id').'?id='.$request->input('trip_id'))->with('status', 'Trip Status Changed!');
    }

    public function showEditForm(Request $request){
        $validator = Validator::make($request->all(), [
            'trip_id' => 'required|numeric'
        ]);

        
        if ($validator->fails()) {
            return redirect(route('view_trip'))
                        ->withErrors($validator)
                        ->withInput();
        }


        $trip_id = $request->input('trip_id');

        if(Trip::where( 'id' , '=' , $trip_id )->exists()){

            $trip = Trip::where( 'id' , '=' , $trip_id )->first();
            $assets = Asset::all();
            $user_groups = UserGroup::where('id','!=','1')->get();
            
            $extraServices = [];
            $ser = $trip->extraServices()->get();

            foreach ($ser as $k) {

                $service_discounts = [];
                
                foreach ($user_groups as $ug) {
                    $es  = ExtraServiceDiscount::where('extra_service_id' , '=' , $k->id)->where('user_group_id','=',$ug->id)->exists();

                    $discount = 0;
                    if($es){
                        $discount  = ExtraServiceDiscount::where('extra_service_id' , '=' , $k->id)->where('user_group_id','=',$ug->id)->first()->discount;
                    }

                    $darr = [
                        'user_group_id' => $ug->id,
                        'user_group_name' => $ug->name,
                        'discount' => $discount
                    ];

                    array_push($service_discounts, $darr);

                }

                $arr = [
                    'trip_id' => $trip->id,
                    'name' => $k->name,
                    'price' => $k->price,
                    'extra_service_id' => $k->id,
                    'discounts' => $service_discounts
                ];

                array_push($extraServices, $arr);

            }


            return view('trip/edit',[ 
                'trip' => $trip, 
                'assets' => $assets, 
                'user_groups' => $user_groups,
                'extraServices' => $extraServices
            ]);



        }else{
            return redirect(route('view_trip'))
                        ->withErrors(['trip_err'=>'Trip do not exist.']);
        }

        return redirect(route('view_trip'))->with('status', 'Trip Deleted Successfully!');
    }

    public function deleteTrip(Request $request){
        $validator = Validator::make($request->all(), [
            'trip_id' => 'required|numeric'
        ]);

        
        if ($validator->fails()) {
            return redirect(route('view_trip'))
                        ->withErrors($validator)
                        ->withInput();
        }

        $trip_id = $request->input('trip_id');

        if(Trip::where( 'id' , '=' , $trip_id )->exists()){

            Trip::where( 'id' , '=' , $trip_id )->delete();

        }else{
            return redirect(route('view_trip'))
                        ->withErrors(['trip_err'=>'Trip do not exist.']);
        }

        return redirect(route('view_trip'))->with('status', 'Trip Deleted Successfully!');
    }

    public function saveEditTrip(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'asset_id' => 'required',
            'date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'repeat' => 'required',
            'adult_price' => 'required',
            'child_price' => 'required',
            'infant_price' => 'required'
        ]);

        
        if ($validator->fails()) {
            return redirect(route('update_trip').'?trip_id='.$request->input('id'))
                        ->withErrors($validator)
                        ->withInput();
        }

        // save trip
        $trip_data = [
            'asset_id' => $request->input('asset_id'),
            'name' => $request->input('name'),
            'date' => Carbon::CreateFromFormat('m-d-Y',$request->input('date'))->format('Y-m-d'),
            'start_date' => $request->input('start_time'),
            'end_date' => $request->input('end_time'),
            'repeat' => $request->input('repeat'),
            'description' => $request->input('description'),
            'adult_price' => $request->input('adult_price'),
            'child_price' => $request->input('child_price'),
            'infant_price' => $request->input('infant_price'),
        ];

        $trip = Trip::where('id','=',$request->input('id'))->update($trip_data);

        // update price discounts

        $price_discounts = $request->input('price_discounts');

        // delete previous discounts
        TripDiscount::where('trip_id','=',$request->input('id'))->delete();
        if($request->has('price_discounts')){
            foreach ($price_discounts as $discount) {
                TripDiscount::create([
                    'trip_id' => $request->input('id'),
                    'user_group_id' => $discount[0],
                    'adult' => $discount[1],
                    'child' => $discount[2],
                    'infant' => $discount[3]
                ]);
            }
        }
        
        // insert extra services
        TripExtraServices::where('trip_id','=',$request->input('id'))->delete();

        if($request->has('extra_services')){
            $extra_services = $request->input('extra_services');
            foreach ($extra_services as $service) {
                
                $es = ExtraService::create([
                    'name' => $service[0],
                    'price' => $service[1]
                ]);

                // save extra service discount
                if(isset($service['discounts'])){
                    foreach ($service['discounts'] as $discount) {
                        ExtraServiceDiscount::create([
                            'extra_service_id' => $es->id,
                            'user_group_id' => $discount[0],
                            'discount' => $discount[1]
                        ]);
                    }                    
                }



                // save trip extra services
                TripExtraServices::create([
                    'trip_id' => $request->input('id'),
                    'extra_service_id' => $es->id
                ]);

            }            
        }



        // save the images

        TripImage::where('trip_id',$request->input('id'))->delete();

        if($request->has('images')){

            foreach($request->input('images') as $image) {
            
                TripImage::create([
                    'trip_id' => $request->input('id'),
                    'path' => $image
                ]);
            
            }
        }



        return redirect(route('view_trip'))->with('status', 'Trip Updated Successfully!');

    }

    public function getTripData(Request $request){
        
        $trips = Trip::select(['id', 'name', 'date', 'start_date', 'end_date', 'repeat', 'status']);


        // query filter
        $q = "";
        if($request->has('name') &&  !empty($request->input('name'))){
            $q = $request->input('name');
        }
        $trips->where('name','LIKE','%'.$q.'%');
        $trips->orWhere('trip_ref','=',$q);
        // assets filter

        if($request->has('asset_id') && !empty($request->input('asset_id'))){
            $asset_id = $request->input('asset_id');
            $trips->where('asset_id','=',$asset_id);
        }

        // repeat filter

        if($request->has('repeat') && !empty($request->input('repeat'))){
            $repeat = $request->input('repeat');
            $trips->where('repeat','=',$repeat);
        }

        // open filter
        
        if($request->has('status') && !empty($request->input('status'))){
            $status = $request->input('status');
            $trips->where('status','=',$status);
        }

        // date filter
        
        if($request->has('date') && !empty($request->input('date'))){

            $trips->whereDate('date', '=', 
                Carbon::CreateFromFormat('Y-m-d',$request->input('date'))->format('Y-m-d'));

        }
        
        $trips->orderBy('id', 'DESC');
        
        return Datatables::of($trips)->make();
    }

}
