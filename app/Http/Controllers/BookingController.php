<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asset;
use App\User;
use Config;
use Auth;
use Validator;
use DataTables;

use Carbon\Carbon;
use App\Trip;
use App\TripExtraServices;
use App\Customer;
use App\Booking;
use App\Coupon;
use App\BookingExtraServices;

use App\Helper\TripHelper;

class BookingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware([ 'auth','check_permission' ]);
    }

    private function isTripAvailable($date){
        $trip_date=Carbon::CreateFromFormat('Y-m-d',$date);
        $today = Carbon::today();

        $diff_in_days = $trip_date->diffInDays($today);
        
        if( ($diff_in_days % 7) == 0  ){
            return true;
        }

        return false;

    }

    public function form(){
    	return view('booking/add');
    }

    public function show(){
        $assets = Asset::all();
    	return view('booking/list',['assets'=>$assets]);
    }

    public function displayBookingForm($id){
        
        if(!Trip::where('id','=',$id)->exists()){
            return redirect(route('bookings'))->with('error', 'This trip do not exist.');
        }

        $trip = Trip::where('id','=',$id)->first();

        if($trip->repeat == 'weekly'){
            if(!$this->isTripAvailable($trip->date)){
                return redirect(route('bookings'))->with('error', 'This trip is not available for booking.');
            }
        }

        $agents = User::where('user_group_id' , '!=' , 1)->get();

        $trip_extra_services = TripExtraServices::where('trip_id','=',$trip->id)->get();


        $payment_types = [
            1 => 'Cash', 
            2 =>'Cash - Western Union', 
            3 => 'Cash - Bank Transfer', 
            4 => 'Cash - Bank Deposit', 
            5 =>'Visa'
        ];

        return view('booking/make',[
            'agents' => $agents,
            'trip' => $trip,
            'trip_extra_services' => $trip_extra_services,
            'payment_types'=>$payment_types
        ]);
    }

    public function saveBooking(Request $request){


        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'middle_name' => 'nullable',
            'last_name' => 'required',
            'dob' => 'nullable',
            'email' => 'nullable',
            'mobile' => 'required',
            'referral_email' => 'nullable|email',
            'referral_name' => 'nullable',
            'description' => 'nullable',
            'commissioned_sales_agent' => 'required',
            'adult_qty' => 'required',
            'child_qty' => 'required',
            'infant_qty' => 'required',
            'payment_type' => 'required',
            'payment_notes' => 'nullable',
            'trip_id' => 'required'
        ]);
        

        if ($validator->fails()) {
            return redirect(route('make_booking',$request->input('trip_id')))
                        ->withErrors($validator)
                        ->withInput();
        }


        // check if enough tickets are available or not
        
        if(Auth::id() != 1){
            $_trip = Trip::where('id','=',$request->input('trip_id'))->first();
            $tickets_qty = $request->input('tickets_qty');
            $total_booking_on_trip = Booking::where('trip_id','=',$_trip->id)->sum('ticket_qty');
            $ast = Asset::where('id','=',$_trip->asset_id)->first();

            $asset_capacity = $ast->max_capacity;

            $tickets_left = $asset_capacity - $total_booking_on_trip;

            if($tickets_qty > $tickets_left){

                return redirect(route('make_booking',$request->input('trip_id')))
                        ->withErrors([
                            'ticket_error' => 'There are only '.$tickets_left.' left. You cannot book '.$tickets_qty.' tickets'
                        ])
                        ->withInput();
            }


            // check if max agent limit is exceed or not
            $my_total_booking_on_trip = Booking::where('trip_id','=',$_trip->id)->where('created_user_id','=',Auth::id())->sum('ticket_qty');
            $asset_booking_limit = $ast->per_agent_booking_limit;

            $my_limit = $asset_booking_limit - $my_total_booking_on_trip;

            if($tickets_qty > $my_limit){
                return redirect(route('make_booking',$request->input('trip_id')))
                        ->withErrors([
                            'ticket_error' => 'You cannot book more than '.$my_limit.' tickets.'
                        ])
                        ->withInput();                
            }
        }


        $coupon = NULL;
        // check if coupon exists and not expired
        if($request->has('coupon_code') && !empty($request->input('coupon_code'))){
            if(Coupon::where('code','=',$request->input('coupon_code'))->exists()){

                // check if coupon is valid or not
                $coupon = Coupon::where('code','=',$request->input('coupon_code'))->first();

                $from = Carbon::createFromFormat('Y-m-d',$coupon->valid_from);
                $to = Carbon::createFromFormat('Y-m-d',$coupon->valid_to);

                $today = Carbon::now();

                if(!$today->between($from,$to)){
                    // Coupon has expired
                    return redirect(route('make_booking',$request->input('trip_id')))
                            ->withErrors([
                                'coupon_id' => 'This coupon has expired.'
                            ])
                            ->withInput();
                }

                // validate the number of use

                $number_of_used = Booking::where('coupon_id','=',$coupon->id)->count();

                if($coupon->number_of_use <= $number_of_used){
                    return redirect(route('make_booking',$request->input('trip_id')))
                            ->withErrors([
                                'coupon_id' => 'This coupon has reached his maximum use limit.'
                            ])
                            ->withInput();
                }
            }else{
                return redirect(route('make_booking',$request->input('trip_id')))
                        ->withErrors([
                            'coupon_id' => 'Coupon do not exist.'
                        ])
                        ->withInput();
            } 
        }


        if(Customer::where('phone','=',$request->input('mobile'))->exists()){
            
            $customer = Customer::where('phone','=',$request->input('mobile'))->first();

        }else{

            if($request->has('dob') && (!empty($request->input('dob')))){
                $date_of_birth = Carbon::CreateFromFormat('m-d-Y',$request->input('dob'))->format('Y-m-d');
            }else{
                $date_of_birth = NULL;
            }
            

            $customer_data = [
                'first_name' => $request->input('first_name'),
                'last_name' => $request->input('last_name'),
                'middle_name' => $request->input('middle_name'),
                'phone' => $request->input('mobile'),
                'email' => $request->input('email'),
                'dob' => $date_of_birth,
                'referral_name' => $request->input('referral_name'),
                'referral_email' => $request->input('referral_email'),
                'created_by' => Auth::id()
            ];

            $customer = Customer::create($customer_data);            
        }

        $booking_data = [
            'customer_id' => $customer->id,
            'created_user_id' => Auth::id(),
            'sales_agent_id' => ($request->has('sales_agent_id') && $request->input('sales_agent_id') != NULL) ? $request->input('sales_agent_id') : 1, // TODO : update this
            'coupon_id' => ($coupon != NULL) ? $coupon->id : NULL,
            'ticket_qty' => $request->input('tickets_qty'),
            'request_info' => '',
            'sales_agents_commission' => $request->input('commissioned_sales_agent'),
            'adult_qty' => $request->input('adult_qty'),
            'child_qty' => $request->input('child_qty'),
            'infant_qty' => $request->input('infant_qty'),
            'status' => 0,
            'booking_ref'  => '',
            'payment_type' => $request->input('payment_type'),
            'payment_notes' => $request->input('payment_notes'),
            'trip_id' => $request->input('trip_id')
        ];

        $booking = Booking::create($booking_data);

        $ref = "BOK".Carbon::now()->year.str_pad($booking->id,4,"0",STR_PAD_LEFT);

        $booking->booking_ref = $ref;
        $booking->save();

        // TODO: save the booking extra services
        //BookingExtraServices
        $es = $request->input('extra_service');
        $es_qty = $request->input('extra_service_qty');

        for ($i=0; $i < count($es); $i++) {

            $service_id = $es[$i];
            $service_qty = $es_qty[$i];
            
            BookingExtraServices::create([
                'booking_id' => $booking->id,
                'extra_service_id' => $service_id,
                'qty' => ($service_qty == NULL) ? 0 : $service_qty
            ]);

        }

        return redirect(route('view_all_bookings_of_trip', $request->input('trip_id')))->with('status', 'Booking Added Successfully!');
    }

    public function viewAllBookingsOfTrip($id){
        
        if(!Trip::where('id','=',$id)->exists()){
            die("This trip do not exist.");
        }

        return view('booking/bookings',[ 'trip_id' => $id ]);
    }

    public function collectAmount(Request $request){
        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric',
            'booking_id' => 'required'
        ]);
        

        if ($validator->fails()) {
            return response()->json(['error'=>'Invalid Amount or Trip']);
        } 

        $booking = Booking::where('id','=',$request->input('booking_id'))->first();

        $booking->amount_paid +=$request->input('amount');

        $booking->save();
        
        return response()->json(['success'=>1]);
    }


    public function showEditForm(Request $request){
        
        
        if(!$request->has('booking_id')){
            die("Invalid request");
        }

        $booking_id = $request->input('booking_id');
        
        if(!Booking::where('id','=',$booking_id)->exists()){
            die("Booking do not exist.");
        }

        $booking = Booking::where('id','=',$booking_id)->first();



        if(!Trip::where('id','=',$booking->trip_id)->exists()){
            return redirect(route('bookings'))->with('error', 'This trip do not exist.');
        }

        $trip = Trip::where('id','=',$booking->trip_id)->first();

        if($trip->repeat == 'weekly'){
            if(!$this->isTripAvailable($trip->date)){
                return redirect(route('bookings'))->with('error', 'This trip is not available for booking.');
            }
        }

        $agents = User::where('user_group_id' , '!=' , 1)->get();

        $trip_extra_services = TripExtraServices::where('trip_id','=',$trip->id)->get();


        $payment_types = [
            1 => 'Cash', 
            2 =>'Cash - Western Union', 
            3 => 'Cash - Bank Transfer',
            4 => 'Cash - Bank Deposit',
            5 =>'Visa'
        ];

        return view('booking/edit-booking',[
            'agents' => $agents,
            'trip' => $trip,
            'trip_extra_services' => $trip_extra_services,
            'payment_types'=>$payment_types,
            'booking' => $booking,
            'customer' => $booking->customer()->first(),
            'coupon' => $booking->coupon()->first(),
            'booking_extra_services' => BookingExtraServices::where('booking_id','=',$booking->id)->get()
        ]);


    }

    public function saveEditBooking(Request $request){
        
        $validator = Validator::make($request->all(), [
            'booking_id' => 'required',
            'commissioned_sales_agent' => 'required',
            'adult_qty' => 'required',
            'child_qty' => 'required',
            'infant_qty' => 'required',
            'payment_type' => 'required',
            'payment_notes' => 'nullable',
            'trip_id' => 'required'
        ]);
        

        if ($validator->fails()) {
            return redirect(route('update_booking').'?booking_id='.$request->input('booking_id'))
                        ->withErrors($validator)
                        ->withInput();
        }


        // check if enough tickets are available or not
        
        if(Auth::id() != 1){
            $_trip = Trip::where('id','=',$request->input('trip_id'))->first();
            $tickets_qty = $request->input('tickets_qty');
            $total_booking_on_trip = Booking::where('trip_id','=',$_trip->id)->sum('ticket_qty');
            $ast = Asset::where('id','=',$_trip->asset_id)->first();

            $asset_capacity = $ast->max_capacity;

            $tickets_left = $asset_capacity - $total_booking_on_trip;

            if($tickets_qty > $tickets_left){

                return redirect(route('update_booking').'?booking_id='.$request->input('booking_id'))
                        ->withErrors([
                            'ticket_error' => 'There are only '.$tickets_left.' left. You cannot book '.$tickets_qty.' tickets'
                        ])
                        ->withInput();
            }


            // check if max agent limit is exceed or not
            $my_total_booking_on_trip = Booking::where('trip_id','=',$_trip->id)->where('created_user_id','=',Auth::id())->sum('ticket_qty');
            $asset_booking_limit = $ast->per_agent_booking_limit;

            $my_limit = $asset_booking_limit - $my_total_booking_on_trip;

            if($tickets_qty > $my_limit){
                return redirect(route('update_booking').'?booking_id='.$request->input('booking_id'))
                        ->withErrors([
                            'ticket_error' => 'You cannot book more than '.$my_limit.' tickets.'
                        ])
                        ->withInput();                
            }
        }


        $coupon = NULL;
        // check if coupon exists and not expired
        if($request->has('coupon_code') && !empty($request->input('coupon_code'))){
            if(Coupon::where('code','=',$request->input('coupon_code'))->exists()){

                // check if coupon is valid or not
                $coupon = Coupon::where('code','=',$request->input('coupon_code'))->first();

                $from = Carbon::createFromFormat('Y-m-d',$coupon->valid_from);
                $to = Carbon::createFromFormat('Y-m-d',$coupon->valid_to);

                $today = Carbon::now();

                if(!$today->between($from,$to)){
                    // Coupon has expired
                    return redirect(route('update_booking').'?booking_id='.$request->input('booking_id'))
                            ->withErrors([
                                'coupon_id' => 'This coupon has expired.'
                            ])
                            ->withInput();
                }

                // validate the number of use

                $number_of_used = Booking::where('coupon_id','=',$coupon->id)->count();

                if($coupon->number_of_use <= $number_of_used){
                    return redirect(route('update_booking').'?booking_id='.$request->input('booking_id'))
                            ->withErrors([
                                'coupon_id' => 'This coupon has reached his maximum use limit.'
                            ])
                            ->withInput();
                }
            }else{
                return redirect(route('update_booking').'?booking_id='.$request->input('booking_id'))
                        ->withErrors([
                            'coupon_id' => 'Coupon do not exist.'
                        ])
                        ->withInput();
            } 
        }

        $customer_id = $request->input('customer_id');

        $booking_data = [
            'customer_id' => $customer_id,
            'sales_agent_id' => ($request->has('sales_agent_id') && $request->input('sales_agent_id') != NULL) ? $request->input('sales_agent_id') : 1, // TODO : update this
            'coupon_id' => ($coupon != NULL) ? $coupon->id : NULL,
            'ticket_qty' => $request->input('tickets_qty'),
            'request_info' => '',
            'sales_agents_commission' => $request->input('commissioned_sales_agent'),
            'adult_qty' => $request->input('adult_qty'),
            'child_qty' => $request->input('child_qty'),
            'infant_qty' => $request->input('infant_qty'),
            'status' => 0,
            'payment_type' => $request->input('payment_type'),
            'payment_notes' => $request->input('payment_notes'),
            'trip_id' => $request->input('trip_id')
        ];

        $booking_id = $request->input('booking_id');

        Booking::where( 'id' , '=' , $booking_id )->update($booking_data);


        //delete previous bookings
        BookingExtraServices::where('booking_id','=',$booking_id)->delete();
        
        //BookingExtraServices
        $es = $request->input('extra_service');
        $es_qty = $request->input('extra_service_qty');

        for ($i=0; $i < count($es); $i++) {

            $service_id = $es[$i];
            $service_qty = $es_qty[$i];
            
            BookingExtraServices::create([
                'booking_id' => $booking_id,
                'extra_service_id' => $service_id,
                'qty' => ($service_qty == NULL) ? 0 : $service_qty
            ]);

        }

        return redirect(route('view_all_bookings_of_trip', $request->input('trip_id')))->with('status', 'Booking Updated Successfully!');
    }

    public function deleteBookingById(Request $request)
    {
        if(!$request->has('booking_id')){
            die("Booking not found");
        }

        $booking_id = $request->input('booking_id');
        $trip_id = Booking::where('id','=',$booking_id)->first()->trip_id;
        Booking::where('id','=',$booking_id)->delete();

        return redirect(route('view_all_bookings_of_trip',$trip_id))->with('status', 'Booking Deleted Successfully!'); 
    }

    public function viewBookingById( Request $request ){
        if(!$request->has('id')){
            return redirect(route('home'));
        }

        $booking_id = $request->input('id');
        // Get booking
        $booking = Booking::where('id','=',$booking_id)->first();
        $user = $booking->user()->first();
        $customer = $booking->customer()->first();

        $total_cost = TripHelper::getTotalBookingRevenue($booking);

        $total_agent_commision = TripHelper::calculateTotalAgentCommission($booking,$user->user_group_id);


        $booking_extra_services = BookingExtraServices::where('booking_id','=',$booking->id)->get();

        $bes = [];
        foreach ($booking_extra_services as $es) {
            $service = $es->extraService()->first();
            $data = [
                'qty' => $es->qty,
                'name' => $service->name,
                'price' => $service->price
            ];

            array_push($bes, $data);
        }

        return view('booking/single',[
            'booking' => $booking,
            'user' => $user,
            'customer' => $customer,
            'total_agent_commision' => $total_agent_commision,
            'total_cost' => $total_cost,
            'extra_services' => $bes
        ]);
    }

    public function getAllBookingsAjax(Request $request){
        

        $query = Booking::where('trip_id','=',$request->input('trip_id'));
        
        //name
        if($request->has('name') &&  !empty($request->input('name'))){
            $q = $request->input('name');
            $query->where('booking_ref','=',$q);
        }


        if(Auth::id() != 1){
            $query->where('created_user_id','=',Auth::id());
        }
        
        $bookings = $query->orderBy('id', 'DESC')->get();

        $response = [];

        foreach ($bookings as $booking) {
            $customer = $booking->customer()->first();
            $agent = $booking->sales_agent()->first();
            $trip_extra_services = $booking->extraServices()->get();
            $trip = $booking->trip()->first();

            // customer name
            $customer_name = $customer->first_name;
            if($customer->middle_name != NULL){
                $customer_name .= ' '.$customer->middle_name;
            }

            $customer_name .= ' '.$customer->last_name;

            // total price
            $ticket_cost = ($booking->adult_qty * $trip->adult_price) + 
                    ($booking->child_qty * $trip->child_price) + 
                    ($booking->infant_qty* $trip->infant_price);

            $extra_service_cost = 0;

            foreach ($trip_extra_services as $service) {
                $p = $service->price;
                
                $qty =  BookingExtraServices::where('booking_id','=',$booking->id)->orWhere('extra_service_id','=',$service->id)->first()->qty;

                $t = $p*$qty;
                $extra_service_cost += $t;
            }

            $total_cost = $ticket_cost + $extra_service_cost;

            // check if coupon is available or not
            if($booking->coupon_id != NULL){
                $coupon = $booking->coupon()->first();
                $total_cost -= $coupon->discount_amount;
            }

            $data = [ 
                'id' => $booking->id,
                'customer' => ['name' => $customer_name, 'id' => $customer->id],
                'trip' => ['name' => $trip->name, 'id' => $trip->id],
                'sale_agent' => ($agent->id == 1)? 'Admin' : $agent->name,
                'tickets_qty' => $booking->ticket_qty,
                'amount_paid' => $booking->amount_paid,
                'balance' => $total_cost - $booking->amount_paid,
                'ref' => $booking->booking_ref
            ];
            array_push($response, $data);
        }


        //


        return Datatables::of($response)->make();
    }

    public function getAvailableTripData(Request $request){
        
        $trips = Trip::select(['id', 'name', 'date', 'start_date', 'end_date', 'repeat','status']);

        // query filter
        $q = "";
        if($request->has('name') &&  !empty($request->input('name'))){
            $q = $request->input('name');
        }
        $trips->where('name','LIKE','%'.$q.'%');

        // assets filter

        if($request->has('asset_id') && !empty($request->input('asset_id'))){
            $asset_id = $request->input('asset_id');
            $trips->where('asset_id','=',$asset_id);
        }

        // repeat filter

        if($request->has('repeat') && !empty($request->input('repeat'))){
            $repeat = $request->input('repeat');
            $trips->where('repeat','=',$repeat);
        }

        // open filter
        
        if($request->has('status') && !empty($request->input('status'))){
            $status = $request->input('status');
            $trips->where('status','=',$status);
        }

        // date filter
        
        if($request->has('date') && !empty($request->input('date'))){
            
            $trips->where('date', '=', 
                Carbon::CreateFromFormat('Y-m-d',$request->input('date'))->format('d-m-Y'));
        }

        
        $response = [];
        foreach ($trips->orderBy('id', 'DESC')->get() as $trip) {
            if($trip->repeat == 'weekly'){

                // check if this trip is available for booking
                $trip_date = $trip->date;

                if($this->isTripAvailable($trip_date)){
                    array_push($response, $trip);
                }

            }else{
                array_push($response, $trip);
            }
        }

        return Datatables::of($response)->make();
    }
}
