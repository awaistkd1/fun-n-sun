<?php

namespace App\Http\Controllers;

use Validator;
use Auth;

use DataTables;
use Config;

use App\UserGroup;

use Illuminate\Http\Request;

class UserGroupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware([ 'auth','check_permission' ]);
    }


    public function form(){
        $system = array('assets', 'trips', 'bookings', 'customers', 'sales_agents', 'coupons');
    	return view('user_group/add', [ 'system' => $system ]);
    }

    public function show(){
    	return view('user_group/list');
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'max:1000'
        ]);

        
        if ($validator->fails()) {
            return redirect(route('add_user_group'))
                        ->withErrors($validator)
                        ->withInput();
        }


        $data = [
            'name' => $request->input('name'), 
            'description' => $request->input('description'), 
            'assets_add' => ($request->has('assets_add') ? 1 : 0), 
            'assets_update' => ($request->has('assets_update') ? 1 : 0),
            'assets_delete' => ($request->has('assets_delete') ? 1 : 0),
            'trips_add' => ($request->has('trips_add') ? 1 : 0),
            'trips_update' => ($request->has('trips_update') ? 1 : 0),
            'trips_delete' => ($request->has('trips_delete') ? 1 : 0),
            'bookings_add' => ($request->has('bookings_add') ? 1 : 0),
            'bookings_update' => ($request->has('bookings_update') ? 1 : 0),
            'bookings_delete' => ($request->has('bookings_delete') ? 1 : 0),
            'customers_add' => ($request->has('customers_add') ? 1 : 0),
            'customers_update' => ($request->has('customers_update') ? 1 : 0),
            'customers_delete' => ($request->has('customers_delete') ? 1 : 0),
            'sales_agents_add' => ($request->has('sales_agents_add') ? 1 : 0),
            'sales_agents_update' => ($request->has('sales_agents_update') ? 1 : 0),
            'sales_agents_delete' => ($request->has('sales_agents_delete') ? 1 : 0),
            'coupons_add' => ($request->has('coupons_add') ? 1 : 0),
            'coupons_update' => ($request->has('coupons_update') ? 1 : 0),
            'coupons_delete' => ($request->has('coupons_delete') ? 1 : 0)
        ];

        $user_group = UserGroup::create($data);

        return redirect(route('view_user_group'))->with('status', 'User Group Added Successfully!');

    }

    public function viewById(Request $request){
        if(!$request->has('id')){
            return redirect(route('view_user_group'));
        }

        $system = array('assets', 'trips', 'bookings', 'customers', 'sales_agents', 'coupons');
        $group_id = $request->input('id');
        $group = UserGroup::where('id','=',$group_id)->first();
        
        return view('user_group/group' , [ 'group' => $group, 'system' => $system ]);
    }

    public function showEditForm(Request $request){

        if(!$request->has('group_id')){
            return redirect(route('view_user_group'));
        }

        $system = array('assets', 'trips', 'bookings', 'customers', 'sales_agents', 'coupons');
        $group_id = $request->input('group_id');
        $group = UserGroup::where('id','=',$group_id)->first();
        
        return view('user_group/edit' , [ 'group' => $group, 'system' => $system ]);
    }

    public function saveEditForm(Request $request){
        $validator = Validator::make($request->all(), [
            'group_id' => 'required',
            'name' => 'required',
            'description' => 'max:1000'
        ]);

        
        if ($validator->fails()) {
            return redirect(route('view_user_group'))
                        ->withErrors($validator)
                        ->withInput();
        }


        $data = [
            'name' => $request->input('name'), 
            'description' => $request->input('description'), 
            'assets_add' => ($request->has('assets_add') ? 1 : 0), 
            'assets_update' => ($request->has('assets_update') ? 1 : 0),
            'assets_delete' => ($request->has('assets_delete') ? 1 : 0),
            'trips_add' => ($request->has('trips_add') ? 1 : 0),
            'trips_update' => ($request->has('trips_update') ? 1 : 0),
            'trips_delete' => ($request->has('trips_delete') ? 1 : 0),
            'bookings_add' => ($request->has('bookings_add') ? 1 : 0),
            'bookings_update' => ($request->has('bookings_update') ? 1 : 0),
            'bookings_delete' => ($request->has('bookings_delete') ? 1 : 0),
            'customers_add' => ($request->has('customers_add') ? 1 : 0),
            'customers_update' => ($request->has('customers_update') ? 1 : 0),
            'customers_delete' => ($request->has('customers_delete') ? 1 : 0),
            'sales_agents_add' => ($request->has('sales_agents_add') ? 1 : 0),
            'sales_agents_update' => ($request->has('sales_agents_update') ? 1 : 0),
            'sales_agents_delete' => ($request->has('sales_agents_delete') ? 1 : 0),
            'coupons_add' => ($request->has('coupons_add') ? 1 : 0),
            'coupons_update' => ($request->has('coupons_update') ? 1 : 0),
            'coupons_delete' => ($request->has('coupons_delete') ? 1 : 0)
        ];

        $group_id = $request->input('group_id');

        $user_group = UserGroup::where('id','=',$group_id)->update($data);

        return redirect(route('view_user_group'))->with('status', 'User Group Updated Successfully!');

    }


    public function deleteUserGroup(Request $request){

        $validator = Validator::make($request->all(), [
            'group_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return redirect(route('view_user_group'))
                        ->withErrors($validator)
                        ->withInput();
        }

        $group_id = $request->input('group_id');

        if(UserGroup::where( 'id' , '=' , $group_id )->exists()){

            UserGroup::where( 'id' , '=' , $group_id )->delete();

        }else{
            return redirect(route('view_user_group'))
                        ->withErrors(['agent_err'=>'User Group do not exist.']);
        }

        return redirect(route('view_user_group'))->with('status', 'User Group Deleted Successfully!');
    }


    public function getUserGroupData(Request $request){

        $user_groups = UserGroup::select(['id','name'])->where('id','!=', Config::get('admin.user_group'));

        // query filter
        $q = "";
        if($request->has('name') &&  !empty($request->input('name'))){
            $q = $request->input('name');
        }

        $user_groups->where('name','LIKE','%'.$q.'%');
        $user_groups->orderBy('id', 'DESC');
        return Datatables::of($user_groups)->make();

    }

}
