<?php
namespace App\Http\Middleware;

use Closure;
use Auth;
use Log;
use Config;

use App\UserGroup;
use App\User;
use Illuminate\Support\Facades\View;

class CheckUserPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user() == null){
            return $next($request);
        }
        
        $user =  User::where( 'id' , '=' , Auth::id() )->first();

        $user_group = $user->userGroup()->first();
        $route_name = \Request::route()->getName();

        View::share('user_group', $user_group);
        View::share('currency', Config::get('preferences.currency'));

        if($user_group->id ==1){ return $next($request); }

        // assets route categories
        $prop = $this->getRouteCategory($route_name);

        if($prop){
            Log::info($prop);
            $is_allowed = $user_group->{$prop};

            if($is_allowed == 1){
                return $next($request);
            }else{
                return response()->view('unauthorized',[],403);
            }

        }else{
            return $next($request);    
        }
    }

    private function getRouteCategory($route_str){
        
        $permissions = Config::get('permissions');

        $prop_str = false;
        foreach ($permissions as $module_name => $module_permissions) {
            foreach ($module_permissions as $type => $routes) {
                if(in_array($route_str, $routes)){
                    if($type != 'view'){
                        $prop_str = $module_name."_".$type;
                    }
                    break;
                }
            }
        }

        return $prop_str;
    }
}