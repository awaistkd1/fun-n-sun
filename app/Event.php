<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

	/**
     * The table associated with the model.
     * 
     * @var string
    */
    protected $table = 'events';
    

    protected $fillable = ['name', 'description', 'date','created_by'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

}
