<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property float $price
 * @property Booking[] $bookings
 * @property ExtraServiceDiscount[] $extraServiceDiscounts
 * @property Trip[] $trips
 */
class ExtraService extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'ExtraService';
    /**
     * @var array
     */
    protected $fillable = ['name', 'price'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function bookings()
    {
        return $this->belongsToMany('App\Booking', 'BookingExtraServices', 'extra_service_id', 'booking_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function extraServiceDiscounts()
    {
        return $this->hasMany('App\ExtraServiceDiscount', 'extra_service_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function trips()
    {
        return $this->belongsToMany('App\Trip', 'TripExtraServices', 'extra_service_id', 'trip_id');
    }
}
